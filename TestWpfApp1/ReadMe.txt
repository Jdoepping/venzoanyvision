﻿This project contains a way to test the Admin part of the AnyVision plug-in.

It is a faster way to change and test the UI. Because it doesn't need to be loaded into the 
actual Admin client, with the super slow test cycles that entails.

See the App.xaml.cs method, to understand how it is loaded.



