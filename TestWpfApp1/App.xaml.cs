﻿using System;
using System.Threading.Tasks;
using System.Windows;
using Extensions.Messaging;
using SelectableRectangles.Admin.Item;
using SelectableRectangles.Background;

namespace TestAdminUi
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// To run this typically a Milestone system should be running on the local machine (See the Login method)
    /// </summary>
    public partial class App : Application
    {
        private AnyVisionSocketEventListenerPlugin plugin;
        private SimpleMessaging externalMessenger;
        private AdminUi.MainWindow mw;
        private event Action UiLoadAssumedCompleteEvent;

        protected override async void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            VideoOS.Platform.SDK.Environment.Initialize();
            Login("Negotiate");// either Basic or Negotiate
            // Start the background plug-in. In the normal case the plug-in is running
            // when the Admin client is started.
            try
            {
                // The constructor and Init methods are both called by the framework
                // and cannot contain parameters. 
                // The Init methods will try to start the connection to AnyVisionServer.
                plugin = new AnyVisionSocketEventListenerPlugin();
                plugin.Init();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            externalMessenger = new SimpleMessaging(MessagingScope.External);
            // The register process is slow, so give it some time to finish.
            await Task.Delay(TimeSpan.FromSeconds(4));
            var kind = new Guid("c64fa440-29dc-4ba1-a265-053a820a2fef");
            var itemManager = new SelectableRectangles.Admin.ItemManager(kind);
            mw = new AdminUi.MainWindow
            {
                Functionality = new Functionality(itemManager,()=>externalMessenger.Send(SelectableRectangles.PluginDefinition.MessageIds.ReconnectWebSocket))
            };
            itemManager.Init();
            itemManager.FillUserControl();
            mw.DataContext = itemManager.DataContext;
            mw.Show();
            UiLoadAssumedCompleteEvent += ItemManager_UiLoadAssumedCompleteEvent;
            UiLoadAssumedCompleteEvent?.Invoke();
        }

        private async void ItemManager_UiLoadAssumedCompleteEvent()
        {
            await Task.Delay(200);
            mw.RemoveHasUnsavedChangesFlag();
            UiLoadAssumedCompleteEvent -= ItemManager_UiLoadAssumedCompleteEvent;
        }

        public static void Login(string negotiateOrBasic)
        {
            var uri = new Uri("http://localhost");
            dynamic credentialCache;
            switch (negotiateOrBasic)
            {
                case "Basic":
                    credentialCache = VideoOS.Platform.Login.Util.BuildCredentialCache(uri, "Jakob", "Jakob", "Basic");
                    break;
                case "Negotiate":
                    credentialCache = VideoOS.Platform.Login.Util.BuildNetworkCredential(uri, "", "", "Negotiate");
                    break;
                default: throw new ArgumentException($"Invalid argument: {negotiateOrBasic}");
            }

            VideoOS.Platform.SDK.Environment.RemoveAllServers();
            VideoOS.Platform.SDK.Environment.AddServer(uri, credentialCache);
            VideoOS.Platform.SDK.Environment.Login(uri);
        }
    }
}
