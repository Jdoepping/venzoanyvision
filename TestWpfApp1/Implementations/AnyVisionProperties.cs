﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using AdminUi.Interfaces;

namespace TestAdminUi.Implementations
{
    // Not in use
    //public class AnyVisionProperties : IAnyVisionProperties, INotifyPropertyChanged
    //{
    //    private string domain;
    //    private string username;
    //    private string password;
    //    private string baseUrl;

    //    public AnyVisionProperties()
    //    {
    //        username = "jakob";
    //        password = "AnyVision1!";
    //        baseUrl = "http://172.20.48.78";
    //        WebSocketSettings = new WebSocketSettingsImpl();
    //    }

    //    public string UserName
    //    {
    //        get => username;
    //        set
    //        {
    //            if (username == value) return;
    //            username = value;
    //            OnPropertyChanged(nameof(UserName));
    //        }
    //    }
    //    public string Password
    //    {
    //        get => password;
    //        set
    //        {
    //            if (password == value) return;
    //            password = value;
    //            OnPropertyChanged(nameof(Password));
    //        }
    //    }
    //    public string Domain
    //    {
    //        get => domain;
    //        set
    //        {
    //            if (domain == value) return;
    //            domain = value;
    //            OnPropertyChanged(nameof(Domain));
    //        }
    //    }
    //    public string BaseUrl
    //    {
    //        get => baseUrl;
    //        set
    //        {
    //            if (baseUrl == value) return;
    //            baseUrl = value;
    //            OnPropertyChanged(nameof(BaseUrl));
    //        }
    //    }

    //    public IWebSockets WebSocketSettings { get ; set; }

    //    public event PropertyChangedEventHandler PropertyChanged;
    //    public event Action ConfigurationChangedByUser;

    //    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    //    {
    //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    //    }
    //}
}
