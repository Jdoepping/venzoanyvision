﻿using System;
using System.Collections;
using System.Collections.Generic;
using AdminUi.Interfaces;

namespace TestAdminUi.Implementations
{
    internal class WebSocketSettingsImpl: IWebSockets
    {
        public WebSocketSettingsImpl()
        {
            TrackCreated = true;
            RecognitionCreated = true;
            CameraChanged = true;
            CameraDeleted = true;
        }

        public IEnumerator<bool> GetEnumerator() => GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
        {
            yield return TrackCreated;
            yield return RecognitionCreated;
            yield return CameraChanged;
            yield return CameraDeleted;
        }

        public event Action<IWebSockets> OnStatusChange;
        public bool TrackCreated { get; set; }
        public bool RecognitionCreated { get; set; }
        public bool CameraChanged { get; set; }
        public bool CameraDeleted { get; set; }
    }
}
