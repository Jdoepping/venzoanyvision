﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnyVisionIntegration
{
    /* Create a web socket that listens to AnyVision events.
     * track:created        A detection of an unknown person took place
     * recognition:created  A recognition of a subject took place
     * camera:changed       One or more parameters of a camera were changed
     * camera:deleted       A camera was deleted from Better Tomorrow
     *
     * For example, if you subscribe to track:created, whenever an unknown person is
     * captured on one of your connected cameras, you will receive a push notification.
     *
     *
     */
   
    
    /// <summary>
    /// 
    /// 
    /// </summary>
    public class TrackData
    {
        //TrackData myDeserializedClass = JsonConvert.DeserializeObject<TrackData>(myJsonResponse); 
        public string Id { get; set; }
        public DateTime FrameTimeStamp { get; set; }
        public int ObjectType { get; set; }
        public int CameraType { get; set; }
        public Camera Camera { get; set; }
    }
}
