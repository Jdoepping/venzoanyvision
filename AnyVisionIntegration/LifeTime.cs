﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using AnyVisionIntegration.Exceptions;
using AnyVisionIntegration.Interfaces;
using MilestoneSdkExtensions.Miscellaneous;


namespace AnyVisionIntegration
{
    /// <summary>
    /// A class to manage the life of a service.
    /// The service in this case is the AnyVision WebSocket.
    ///
    /// Problems:
    /// Renewal of token:
    /// 
    /// 1. How to know when the token expires?
    /// 2. When the token expires the websocket must be recycled.
    ///
    /// Loss of connection
    /// 1. Try to reestablish the connection. Repeatedly try to reconnect.
    /// 2. Reconnects should use a circuit breaker pattern, so it doesn't spam the
    /// server with requests. If it is detected that credentials are invalid reconnects tries are pointless.
    ///
    /// Usage:
    /// Create an instance and close it when the process expires.
    /// In this implementation it assumed that the service, doesn't need to be recycled. 
    /// It can only Connect and disconnect. Time will tell whether this is correct.
    /// </summary>
    public class LifeTime
    {
        private readonly IService service;
        private readonly ICircuitBreaker circuitBreaker;
        private readonly Action<Exception> log;
        private readonly Action<Entry> report;
        private StateEnum state=StateEnum.Disconnected;
        public event Action ConnectingEvent;
        public event Action ConnectedEvent;
        public event Action DisconnectedEvent;
        public event Action<string> NoneTransientErrorEvent;

        public enum StateEnum
        {
            Disconnected,
            Connecting,
            Connected
        }

        public StateEnum State {
            get => state;
            set {
                
                state = value;
                switch (state)
                {
                    case StateEnum.Disconnected:
                        DisconnectedEvent?.Invoke();
                        break;
                    case StateEnum.Connecting:
                        ConnectingEvent?.Invoke();
                        break;
                    case StateEnum.Connected:
                        ConnectedEvent?.Invoke();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public TimeSpan TokenRenewal { get=>service.Delay; set=>service.Delay=value; }

        public LifeTime(IService service, ICircuitBreaker circuitBreaker, Action<Exception> log, Action<Entry> report)
        {
            this.service = service;
            this.circuitBreaker = circuitBreaker;
            this.log = log;
            this.report = report;
            service.TokenExpiredEvent += OnTokenExpiredEvent;
            service.ServerDisconnected += OnDisconnect;
        }
        public void Close()
        {
            try
            {
                service.TokenExpiredEvent -= OnTokenExpiredEvent;
                service.ServerDisconnected -= OnDisconnect;
                service.Dispose();
            }
            catch (Exception e)
            {
                log(e);
            }
        }
        /// <summary>
        /// On a token expired, credentials should be renewed, this is done by connecting
        /// with a new token.
        /// I assume the old connection should be disconnected.
        /// If the circuit breaker is running a connection attempt is in progress
        /// Otherwise reconnect.
        /// Todo: The IsRunning property is shared and could be accessed simultaneously 
        /// </summary>
        private async void OnTokenExpiredEvent()
        {
            await service.Disconnect();
            if (circuitBreaker.IsRunning) return;
            await Connect();
        }
        /// <summary>
        /// The service raises a disconnected event. This is assumed to be an involuntary loss of 
        /// the connection. 
        /// The policy is to wait a while and then try to reconnect.
        /// </summary>
        private async void OnDisconnect()
        {
            State = StateEnum.Disconnected;
            if (circuitBreaker.IsRunning) return;
            await Task.Delay(TimeSpan.FromMinutes(1));
            await Connect();
        }
        public async Task Connect()
        {
            try
            {
                State = StateEnum.Connecting;
                Report("Connect");
                // Should always throw an error on failed connect.
                await service.Connect();
            }
            catch (Exception ex)
            {
                Report("Exception", ex);
                if (!IsTransient(ex))
                {
                    State = StateEnum.Disconnected;
                    return;
                }
                // Don't see a reason to await this call.
                await circuitBreaker.StartRetry(service.Connect);
            }
            State = StateEnum.Connected;
        }

        /// <summary>
        /// Is the error transient or not. If not retrying a call is pointless. 
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private bool IsTransient(Exception ex)
        {
            switch (ex)
            {
                case AnyVisionLoginException e:
                    break;
                case HttpRequestException e:
                    switch (e.InnerException)
                    {
                        case WebException innerEx:
                            NoneTransientErrorEvent?.Invoke(innerEx.Message);
                            return false;
                        default: return true;
                    }
                default: return true;
            }
            NoneTransientErrorEvent?.Invoke(ex.Message);
            return false;
        }
    
        /// <summary>
        /// Well a little bit ugly code
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        private void Report(string message, Exception ex = null)
        {
            if (message.Equals("Connect"))
            {
                report(new Entry { TimeStamp = DateTime.Now, Reason = "Server disconnected", });
                // Todo: fix this strange message
                log(new Exception("Connecting (not a bug)"));
                return;
            }
            report(new Entry {
                TimeStamp = DateTime.Now,
                Reason = "Connect Failed",
                ErrorMessage = ex.Message,
            });
            log(ex);
        }
    }
}
