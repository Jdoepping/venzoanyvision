﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using AnyVisionIntegration.ApiClasses;
using Newtonsoft.Json;

namespace AnyVisionIntegration.WebClient
{
   public class AnyVisionHttpClient: IDisposable
   {
       private const string AnyVisionPath = "/bt/api/";
        private readonly string url;
       private HttpClient client;
       public AnyVisionHttpClient(string url) => this.url = url;

        private HttpClient Client
        {
            get
            {
                client =  client ?? new HttpClient
                {
                    BaseAddress = new Uri(url+ AnyVisionPath)
                };
                client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(
                //    new MediaTypeWithQualityHeaderValue("application/json"));
                return client;
        ; }
        }

        /// <summary>
        /// Adds the bearer token, used to authorize subsequent calls. 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task AddBearerToken(string username, string password)
        {
            var response = await Connect(username, password);
            var token = await ExtractResponse<ConnectResponse>(response);
            client.DefaultRequestHeaders.Add("Authorization", new[] { $"Bearer{token}" });
        }

        public async  Task<T> ExtractResponse<T>(HttpResponseMessage response)
        {
            var content = await response.Content.ReadAsStringAsync();
            return (T)JsonConvert.DeserializeObject(content,typeof(T));
        }

        public async Task<HttpResponseMessage> Connect(string username, string password)
        {
            var response = await Client.PostAsJsonAsync("login",new Credentials
            {
                username=username,
                password = password
            });
            return response;
        }

        private class Credentials
        {
            public string username { get; set; }
            public string password { get; set; }
        }

        public void Dispose() => client.Dispose();

        public async Task<HttpResponseMessage> Cameras(int offset= 0, string sortOrder="desc",  int limit = 10) => 
            await Client.GetAsync($"cameras?offset={offset}&sortOrder={sortOrder}&limit={limit}");
   }
}
