﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AnyVisionIntegration.WebClient
{
    public static class AnyVisionHttpClientExtensions
    {
        /// <summary>
        /// Created this extension because earlier versions of HttpClient apparently had this.
        /// But mostly because I hope to use it several times.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="client"></param>
        /// <param name="requestUri"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> PostAsJsonAsync<T>(this HttpClient client, string requestUri, T param)
        {
            var json = JsonConvert.SerializeObject(param);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            return await client.PostAsync(requestUri, content);
        }
    }
}