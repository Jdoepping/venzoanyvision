﻿using System.Collections;
using System.Collections.Generic;

namespace AnyVisionIntegration
{
    /// <summary>
    /// Analytical events associated with the AnyVision Server.
    /// 
    /// Analytical events that will be auto created and associated with events received from
    /// an AnyVision server.
    /// The policy is to create them when the background plug-in is started. 
    /// </summary>
    public  class SocketEvents : IEnumerable<string>
    {
        public const string TrackCreated = "AnyVision_TrackCreated";
        public const string RecognitionCreated = "AnyVision_RecognitionCreated";
        public const string CameraChanged = "AnyVision_CameraChanged";
        public const string CameraDeleted = "AnyVision_CameraDeleted";

        public IEnumerator<string> GetEnumerator()
        {
            yield return TrackCreated;
            yield return RecognitionCreated;
            yield return CameraChanged;
            yield return CameraDeleted;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
