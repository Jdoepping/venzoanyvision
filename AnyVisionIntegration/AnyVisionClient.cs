﻿using System;
using System.Threading.Tasks;
using AnyVisionIntegration.ApiClasses;
using AnyVisionIntegration.Exceptions;
using AnyVisionIntegration.WebClient;
using H.Socket.IO;
using H.Socket.IO.EventsArgs;
using H.WebSockets.Args;
using H.WebSockets.Utilities;
using Newtonsoft.Json;

namespace AnyVisionIntegration.Interfaces
{
    public class AnyVisionClient : IAnyVisionClient
    {
        private const string AnyVisionPath = "/bt/api/";
        private readonly H.Socket.IO.SocketIoClient client;
        private readonly IAnyVisionSettings settings;
        private readonly VideoOS.CustomDevelopment.PublicStuff.ILogging log;
        public event Action OnDisconnectEvent;

        public AnyVisionClient(IAnyVisionSettings settings, Action<string, string> onSocketEventReceived,
            Action<string, string> onServerEventReceived,
            VideoOS.CustomDevelopment.PublicStuff.ILogging logger)
        {
            client = new SocketIoClient();
            this.settings = settings;
            Url = settings.BaseUrl + AnyVisionPath;
            log = logger;
            SocketActions = new SocketActionsImpl(client, onSocketEventReceived);
            client.Connected += Client_Connected;
            client.Disconnected += Client_Disconnected; ;
        }

        private void Client_Disconnected(object sender, WebSocketCloseEventArgs e) => OnDisconnectEvent?.Invoke();
       

        private void Client_Connected(object sender, SocketIoEventEventArgs e)
        {
            // No idea what to do here.
            //throw new NotImplementedException();
        }

        private string Url { get; }

        /// <summary>
        /// Get a token used to when establishing the socket connection
        /// </summary>
        /// <returns>token</returns>
        /// <exception cref="AnyVisionLoginException"></exception>
        private async Task<string> GetToken()
        {
            using (var webClient = new AnyVisionHttpClient(settings.BaseUrl))
            {
                var response = await webClient.Connect(settings.UserName, settings.Password);
                var content = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode) return ((ConnectResponse)JsonConvert.DeserializeObject(content, typeof(ConnectResponse))).Token;
                throw new AnyVisionLoginException(response);
            }
        }
        /// <summary>
        /// Connects the socket and will start listening to AnyVisions Socket events.
        /// Caller must trap errors.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="AnyVisionLoginException">Thrown when the login fails</exception>
        /// <exception cref="InvalidOperationException">Thrown on connecting to the socket</exception>
        /// <exception cref="ArgumentNullException">Thrown on connecting to the socket</exception>
        /// <exception cref="ObjectDisposedException">Thrown on connecting to the socket</exception>
        /// <exception cref="OperationCanceledException">Thrown on connecting to the socket</exception>
        public async Task<bool> Connect()
        {
            var token = await GetToken();
            var uri = new Uri($"{Url}?token={token}");
            return await client.ConnectAsync(uri);
        }
        /// <summary>
        /// Disconnect from the socket
        /// </summary>
        /// <returns></returns>
        /// /// <exception cref="ObjectDisposedException">Thrown on connecting to the socket</exception>
        public async Task Disconnect()
        {
            OnDisconnectEvent?.Invoke();
            try
            {
                await client.DisconnectAsync();
            }
            // Don't know how to handle this
            catch (Exception e) { }

        }

        public SocketActionsImpl SocketActions { get;}
        public ISocketEventHandlers WebClientEventHandlers { get;}
        /// <summary>
        /// The SocketIoClient is disposable. So we continue the chain.
        /// </summary>
        public void Dispose()
        {
            client.Dispose();
        }
       
        /// <summary>
        /// The idea with this class is to keep things separate. The events here
        /// are about websocket, and not about the services provided by the websocket.
        /// So, to keep things separate
        /// </summary>
        private class WebClientEventHandlersImpl: ISocketEventHandlers
        {
            public WebClientEventHandlersImpl(SocketIoClient client, Action<string, string> onServerEventReceived)
            {
                //this.externalMessenger = externalMessenger;
                client.Connected += Connected;
                client.Disconnected += OnDisconnected; //(sender, args) => Console.WriteLine($"Disconnected. Reason: {args.Reason}, Status: {args.Status:G}");
                //client.EventReceived += EventReceived;//(sender, args) => Console.WriteLine($"EventReceived: Namespace: {args.Namespace}, Value: {args.Value}, IsHandled: {args.IsHandled}");
                //client.HandledEventReceived += HandledEventReceived;//(sender, args) => Console.WriteLine($"HandledEventReceived: Namespace: {args.Namespace}, Value: {args.Value}");
                //client.UnhandledEventReceived += UnhandledEventReceived;//(sender, args) => Console.WriteLine($"UnhandledEventReceived: Namespace: {args.Namespace}, Value: {args.Value}");
                //client.ErrorReceived += ErrorReceived;//(sender, args) => Console.WriteLine($"ErrorReceived: Namespace: {args.Namespace}, Value: {args.Value}");
                //client.ExceptionOccurred += ExceptionOccurred;//(sender, args) => Console.WriteLine($"ExceptionOccurred: {args.Value}");
            }
            /// <summary>
            /// Tell everyone, who cares to listen, that a connection with the anyvision server was made   
            /// </summary>
            /// <param name="obj"></param>
            /// <param name="e"></param>
            public void Connected(object obj, SocketIoEventArgs e)
            {
                //var success = bool.Parse(e.Value);
                //externalMessenger?.Send(AnyVisionIntegration.MessageIds.ConnectedEvent, success);
            }

            public void OnDisconnected(object obj, WebSocketCloseEventArgs e)
            {
                
            }
            //externalMessenger?.Send(AnyVisionIntegration.MessageIds.DisconnectedEvent, e.Reason);

            public void EventReceived(object obj, SocketIoEventEventArgs e)
            {
                throw new NotImplementedException();
            }

            public void HandledEventReceived(object obj, SocketIoEventEventArgs e)
            {
                throw new NotImplementedException();
            }

            public void UnhandledEventReceived(object obj, SocketIoEventEventArgs e)
            {
                throw new NotImplementedException();
            }

            public void ErrorReceived(object obj, SocketIoErrorEventArgs e)
            {
                throw new NotImplementedException();
            }

            public void ExceptionOccurred(object obj, DataEventArgs<Exception> e)
            {
                throw new NotImplementedException();
            }
        }
    }
}