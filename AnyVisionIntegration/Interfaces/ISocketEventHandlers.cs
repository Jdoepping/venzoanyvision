﻿using System;
using H.Socket.IO.EventsArgs;
using H.WebSockets.Args;
using H.WebSockets.Utilities;

namespace AnyVisionIntegration.Interfaces
{
    /// <summary>
    /// Events exposed by SocketIoClient
    /// </summary>
    public interface ISocketEventHandlers
    {
        void Connected(object obj, SocketIoEventArgs e);
        void  OnDisconnected(object obj, WebSocketCloseEventArgs e);
        void EventReceived(object obj, SocketIoEventEventArgs e);
        void HandledEventReceived(object obj, SocketIoEventEventArgs e);
        void UnhandledEventReceived(object obj, SocketIoEventEventArgs e);
        void ErrorReceived(object obj, SocketIoErrorEventArgs e);
        void ExceptionOccurred(object obj, DataEventArgs<Exception> e);
    }
}