﻿namespace AnyVisionIntegration.Interfaces
{
    public interface IAnyVisionSettings
    {
        string UserName { get; }
        string Password { get; }
        string BaseUrl { get;}
        bool TrackCreated { get; }
        bool RecognitionCreated { get; }
        bool CameraChanged { get; }
        bool CameraDeleted { get; }


    }
}
