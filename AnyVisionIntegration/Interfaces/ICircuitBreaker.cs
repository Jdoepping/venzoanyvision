﻿using System;
using System.Threading.Tasks;


namespace AnyVisionIntegration.Interfaces
{
    public interface ICircuitBreaker
    {
        Task StartRetry(Func<Task<bool>> func);
        bool IsRunning { get; }
        
    }
}
