﻿using System;
using System.Threading.Tasks;


namespace AnyVisionIntegration.Interfaces
{
    public interface IService: IDisposable
    {
        event Action TokenExpiredEvent;
        event Action ServerDisconnected;
        Task<bool> Connect();
        Task Disconnect();
        TimeSpan Delay { get; set; }
    }
}
