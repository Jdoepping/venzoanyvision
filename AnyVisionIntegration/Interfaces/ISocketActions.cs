﻿namespace AnyVisionIntegration.Interfaces
{
    /// <summary>
    /// The socket events exposed by the server
    /// </summary>
    public interface ISocketActions
    {
        void TrackCreated(object obj);
        void RecognitionCreated(object obj);
        void CameraChanged(object obj);
        void CameraDeleted(object obj);
    }
}