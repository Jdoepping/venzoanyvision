﻿using System;
using System.Threading.Tasks;

namespace AnyVisionIntegration.Interfaces
{
    /// <summary>
    /// The client reacts to the server event.
    /// An implementation will specify what is done on the server events.   
    /// </summary>
    public interface IAnyVisionClient: IDisposable
    {
        event Action OnDisconnectEvent;
        Task<bool> Connect();
        Task Disconnect();
        SocketActionsImpl SocketActions { get; }
        //ISocketEventHandlers WebClientEventHandlers { get; }
    }
}