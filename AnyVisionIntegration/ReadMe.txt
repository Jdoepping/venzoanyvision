﻿Uses https://github.com/HavenDV/H.Socket.IO






Please see: 
https://docs.microsoft.com/en-us/aspnet/core/signalr/dotnet-client?view=aspnetcore-3.1&tabs=visual-studio

Install the packet using this: https://www.nuget.org/packages/Microsoft.AspNetCore.SignalR.Client

This project is used to create a SignalR connection, that can attach to 
WebSockets
Server-Sent Events
Long Polling

For the Anyvision case it is here to mimic the Web-Socket
With trace:created, recognition:created etc.


Testing
17/11 2020
A single test (ConnectToAnyVisionHubTest()) that really just test the signalr functionality and cannot run unattended.
Run the SimAnyVision solution and SimAnyVsion project and try the "ReceiveMessage" method.