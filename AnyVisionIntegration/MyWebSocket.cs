﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AnyVisionIntegration.ApiClasses;
using AnyVisionIntegration.WebClient;
using Newtonsoft.Json;

namespace AnyVisionIntegration
{
    public class MyWebSocket
    {
        private const string Url = "http://172.20.48.78/bt/api/";

        public async Task<string> TryRequest()
        {
            var client = new AnyVisionHttpClient(Url);
            var response = await client.Connect("jakob", "Anyvision1!");
            if (!response.IsSuccessStatusCode) throw new Exception("Connect Failed");
            var resultJson = await response.Content.ReadAsStringAsync();
            var token = JsonConvert.DeserializeObject<ConnectResponse>(resultJson).Token;
            var request = WebRequest.Create(Url+"track:created");
            var httpRequest = (HttpWebRequest) request;
            httpRequest.PreAuthenticate = true;
            httpRequest.Headers.Add("Authorisation", "Bearer " + token);
            httpRequest.Accept = "application/json";
            var webRep = request.GetResponse();
            var rStream = webRep.GetResponseStream();
            var myStreamReader = new StreamReader(rStream, Encoding.Default);
            var json = myStreamReader.ReadToEnd();
            return json;

        }
    }
}
