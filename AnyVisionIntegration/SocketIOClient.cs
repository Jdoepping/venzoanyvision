﻿using System;
using System.Net;
using System.Threading.Tasks;
using H.Socket.IO;
using H.Socket.IO.EventsArgs;
using H.WebSockets.Args;


namespace AnyVisionIntegration
{
    

    public class SocketIOClient
    {
        private H.Socket.IO.SocketIoClient client;
        private const string token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjJjMjE2NGVhLThjMmQtNDYzMS1hNGYxLWZkM2QwM2Q5ZmFiOCIsInJvbGVJZCI6ImIxMjdlOTY3LTRlNWUtNDFlNC05M2JjLWZhYTczODdiYTQzOCIsImlzRGF0YVJlc3RyaWN0ZWQiOmZhbHNlLCJmdWxsTmFtZSI6Impha29iIERvcHBpbmciLCJpc0FjdGl2ZSI6dHJ1ZSwibG9jYWxlIjoiZW5fdXMiLCJ0aW1lem9uZSI6IkFtZXJpY2EvTmV3X1lvcmsiLCJ1c2VyR3JvdXBJZCI6IjAwMDAwMDAwLTAyMDAtNzkxMi01NWE3LTQ1OGQ0MGRhODc2NSIsImp0aSI6IjBkYjUzMzJhLWVlYzEtNGYxMi1hZGFjLTQ5ZGNjYjBkMjRjZCIsImlhdCI6MTYxMzEyMzY1MSwiZXhwIjoxNjEzMjEwMDUxfQ.c3c1Zt_bOiig2lJTLpSNXxNNvunE5xu4qZoFL0-QA8Lgu_dbwD_PeYAggUyIUh89_mcr2mFcbjHS52YuHKvW2Q";
        public async static Task<bool> Foo()
         {
            ServicePointManager.ServerCertificateValidationCallback += (s, v, t, f) => true;
            using (var client = new SocketIoClient())
            {
                //client.EngineIoClient.WebSocketClient.Socket.Options.
                client.Connected += OnConnected;
                client.Disconnected += OnDisConnected; //(sender, args) => Console.WriteLine($"Disconnected. Reason: {args.Reason}, Status: {args.Status:G}");
                client.EventReceived += (sender, args) => Console.WriteLine($"EventReceived: Namespace: {args.Namespace}, Value: {args.Value}, IsHandled: {args.IsHandled}");
                client.HandledEventReceived += (sender, args) => Console.WriteLine($"HandledEventReceived: Namespace: {args.Namespace}, Value: {args.Value}");
                client.UnhandledEventReceived += (sender, args) => Console.WriteLine($"UnhandledEventReceived: Namespace: {args.Namespace}, Value: {args.Value}");
                client.ErrorReceived += (sender, args) => Console.WriteLine($"ErrorReceived: Namespace: {args.Namespace}, Value: {args.Value}");
                client.ExceptionOccurred += (sender, args) => Console.WriteLine($"ExceptionOccurred: {args.Value}");

                client.On<object>("trace:created", message =>
                {
                    Console.WriteLine($"trace created");
                });
                client.On<object>("recognition:created", message =>
                {
                    Console.WriteLine($"recognition:created");
                });
                client.On<object>("camera:changed", message =>
                {
                    Console.WriteLine($"Camera changed");
                });
                client.On<object>("camera:deleted", message =>
                {
                    Console.WriteLine($"Camera Deleted");
                });

                try
                {
                    //client.DisconnectAsync()
                    var loggedOn = await client.ConnectAsync(
                        new Uri($"http://172.20.48.78/bt/api/socket.io/?token={token}")); //"wss://socketio-chat-h9jt.herokuapp.com/"

                   

                    //await client.DisconnectAsync();
                    while (true)
                    {
                        await Task.Delay(TimeSpan.FromSeconds(2));
                    }
                }
                catch (Exception e)
                {

                    return false;
                }
            }

            return true;

        }

        private static void OnConnected(object obj, SocketIoEventArgs e)
        {

        }
        private static void OnDisConnected(object obj, WebSocketCloseEventArgs e)
        {

        }
    }
}


