﻿using System.Collections.Generic;

namespace AnyVisionIntegration
{
    public class Camera
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public List<double> Location { get; set; }
        public string CameraGroupId { get; set; }
        public int FeaturesQuality { get; set; }
        public int Liveness { get; set; }
        public string CollateId { get; set; }
        public object RelatedTrackId { get; set; }
        public bool IsDeleted { get; set; }
        public object DeletedAt { get; set; }
        public Metadata Metadata { get; set; }
        public List<Image> Images { get; set; }
        public int SubjectScore { get; set; }
        public List<object> CloseMatches { get; set; }
        public object Subject { get; set; }
    }
}