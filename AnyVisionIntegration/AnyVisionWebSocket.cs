﻿//using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace AnyVisionIntegration
{
    /// <summary>
    /// An AnyVision server exposes 4 socket events (not sure what to call it)
    /// track:crated
    /// recognition:created
    /// camera:changed
    /// camera:deleted
    ///
    /// A signalr hub is used to listen to these events. 
    /// </summary>
    //public class AnyVisionWebSocket
    //{
    //    private const string Url = "https://localhost:44354/ChatHub";

    //    public HubConnection connection;
    //    private bool messageReceived;
    //    private string message;
    //    private const string _myAccessToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjJjMjE2NGVhLThjMmQtNDYzMS1hNGYxLWZkM2QwM2Q5ZmFiOCIsInJvbGVJZCI6ImIxMjdlOTY3LTRlNWUtNDFlNC05M2JjLWZhYTczODdiYTQzOCIsImlzRGF0YVJlc3RyaWN0ZWQiOmZhbHNlLCJmdWxsTmFtZSI6Impha29iIERvcHBpbmciLCJpc0FjdGl2ZSI6dHJ1ZSwibG9jYWxlIjoiZW5fdXMiLCJ0aW1lem9uZSI6IkFtZXJpY2EvTmV3X1lvcmsiLCJ1c2VyR3JvdXBJZCI6IjAwMDAwMDAwLTAyMDAtNzkxMi01NWE3LTQ1OGQ0MGRhODc2NSIsImp0aSI6ImFkNGQzOGJmLTVjM2UtNDYzYy05MDdjLTE1OTQzZjYzNWM4ZiIsImlhdCI6MTYxMjg3Mjk4NiwiZXhwIjoxNjEyOTU5Mzg2fQ.F9w3XFlNEtZxUKzN2sfX3AUym0MGDTG9eFiGLALslOyB7oSBXdzwkn7EFViRBg5kk-EMhCtxpFAlhj3XGJV0WA";
    //    public Action<string> TrackCreatedFunc { get; set; }
    //    public Action<string> RecognitionCreatedFunc { get; set; }
    //    public Action<string> CameraChangedFunc { get; set; }
    //    public Action<string> CameraDeletedFunc { get; set; }
    //    public async void LaunchSignalR(string url = null)
    //    {
    //        var tempUrl = url ?? Url;
    //        //tempUrl = tempUrl + $"/?token={_myAccessToken}";
    //        connection = new HubConnectionBuilder()
    //            .WithUrl(tempUrl, options =>
    //                {
    //                    options.AccessTokenProvider = () => Task.FromResult(_myAccessToken);
    //                    options.HttpMessageHandlerFactory = (message) =>
    //                    {
    //                        if (message is HttpClientHandler clientHandler)
    //                            // bypass SSL certificate
    //                            clientHandler.ServerCertificateCustomValidationCallback +=
    //                                (sender, certificate, chain, sslPolicyErrors) => true;
    //                        return message;
    //                    };
    //                })
    //            .Build();

    //        AttachMethods();
    //        try
    //        {
    //            await connection.StartAsync();
    //        }
    //        catch (Exception ex)
    //        {
    //            throw;
    //            //messagesList.Items.Add(ex.Message);
    //        }
    //    }

    //    public bool IsMessageReceived() => messageReceived;

    //    private void AttachMethods()
    //    {
    //        connection.Closed += async (error) =>
    //        {
    //            await Task.Delay(new Random().Next(0, 5) * 1000);
    //            await connection.StartAsync();
    //        };

    //        connection.On<string, string>("ReceiveMessage", (user, message) =>
    //        {
    //            var s = message;
    //            messageReceived = true;

    //            //this.Dispatcher.Invoke(() =>
    //            //{
    //            //    var newMessage = $"{user}: {message}";
    //            //    messagesList.Items.Add(newMessage);
    //            //});
    //        });

    //        connection.On<string, string>("track:created", (user, message) =>
    //        {
    //            var s = message;
    //            messageReceived = true;
    //            TrackCreatedFunc(message);
    //        });
    //        connection.On<string, string>("recognition:created", (user, message) =>
    //        {
    //            var s = message;
    //            messageReceived = true;
    //            RecognitionCreatedFunc(message);
    //        });
    //        connection.On<string, string>("camera:changed", (user, message) =>
    //        {
    //            var s = message;
    //            messageReceived = true;
    //            CameraChangedFunc(message);
    //        });
    //        connection.On<string, string>("camera:deleted", (user, message) =>
    //        {
    //            var s = message;
    //            messageReceived = true;
    //            CameraDeletedFunc(message);
    //        });
    //    }

       

    //}
}
