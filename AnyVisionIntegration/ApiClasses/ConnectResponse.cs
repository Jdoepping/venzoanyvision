﻿using Newtonsoft.Json;

namespace AnyVisionIntegration.ApiClasses
{
    /// <summary>
    /// The response from an AnyVision login request
    /// </summary>
    public  class ConnectResponse
    {
        [JsonProperty("token")] public string Token { get; set; }
        [JsonProperty("isEulaConfirmed")] public bool IsEulaConfirmed { get; set; }
    }
}
