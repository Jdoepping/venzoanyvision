﻿using System.Collections.Generic;
using System.Linq;
using AnyVisionIntegration.Models;

namespace AnyVisionIntegration.ApiClasses
{
    public static class ImageExtractResponseExtensions
    {
        public static IEnumerable<MyRectangle> ToMyRectangles(this ImageExtractResponse response)
        {
            return response.items.Select((t, index) =>
                new MyRectangle
                {
                    Id=index,
                    X = (int)t.position.left,
                    Y = (int)t.position.top,
                    Width = (int)(t.position.right-t.position.left),
                    Height = (int)(t.position.bottom - t.position.top),
                }
            );
        }
    }
}