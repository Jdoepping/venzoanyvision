﻿namespace AnyVisionIntegration.ApiClasses
{
    public class Subject
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}