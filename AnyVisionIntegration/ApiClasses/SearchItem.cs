﻿namespace AnyVisionIntegration.ApiClasses
{
    /// <summary>
    /// The result returned from a search-tracks/ and search-tracks/goto (perhaps more)
    /// 
    /// The return is an object array, the object type has the properties of a search item 
    /// </summary>
    public class SearchItem
    {
        public string id { get; set; }
        public string frameTimeStamp { get; set; }
        public int objectType { get; set; }  // Face=1, Body=2
        public Attributes[] attributes { get; set; }
        public Attributes[] images { get; set; }
    }
}
