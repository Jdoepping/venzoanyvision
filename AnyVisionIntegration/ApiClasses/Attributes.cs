﻿namespace AnyVisionIntegration.ApiClasses
{
    public class Attributes
    {
        public int gender { get; set; }
        public int topColor { get; set; }
        public int bottomColor { get; set; }
        public bool backpack { get; set; }
    }
}