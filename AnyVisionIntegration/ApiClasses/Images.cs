﻿namespace AnyVisionIntegration.ApiClasses
{
    public class Images
    {
        public string url { get; set; }
        public int featuresQuality { get; set; }
    }
}