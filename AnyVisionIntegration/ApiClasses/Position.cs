﻿namespace AnyVisionIntegration.ApiClasses
{
    public class Position
    {
        public double left { get; set; }
        public double top { get; set; }
        public double right { get; set; }
        public double bottom { get; set; }
    }
}
