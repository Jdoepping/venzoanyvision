﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnyVisionIntegration.Interfaces;

namespace AnyVisionIntegration.ApiClasses
{
    public class AnyVisionServer //: IAnyVisionServer
    {
        public AnyVisionServer(H.Socket.IO.SocketIoClient client)
        {

        }

        public class MySocketActions //: ISocketActions
        {
            public MySocketActions(H.Socket.IO.SocketIoClient client)
            {
                //client.Connected += Client_Connected;
            }

            //private void Client_Connected(object sender, H.Socket.IO.EventsArgs.SocketIoEventEventArgs e)=>TrackCreated?.Invoke()
           

            public event Action<object> TrackCreated;
            public event Action<object> RecognitionCreated;
            public event Action<object> CameraChanged;
            public event Action<object> CameraDeleted;
        }

        public ISocketActions SocketActions { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public ISocketEventHandlers WebClientEventHandlers { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public bool Connect()
        {
            throw new NotImplementedException();
        }

        public bool Disconnect()
        {
            throw new NotImplementedException();
        }
    }
}
