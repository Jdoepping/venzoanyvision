﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnyVisionIntegration.ApiClasses
{
    /// <summary>
    /// The body params for the "search-traces/goto call.
    /// </summary>
    public class GotoBody
    {
        public string[] cameras { get; set; }
        public DateTime to { get; set; }
        public DateTime from { get; set; }
        public DateTime frameTimeStamp { get; set; }
        public string tractId { get; set; }

    }
}
