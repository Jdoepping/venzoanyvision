﻿using System;
using System.Collections;
using System.Net.Http;

namespace AnyVisionIntegration.Exceptions
{
    public class AnyVisionLoginException : Exception
    {
        /// <summary>
        /// Will this force the recipient to know HttpResponseMessage?
        /// </summary>
        public  HttpResponseMessage Response { get; }
        public AnyVisionLoginException(HttpResponseMessage response) => Response = response;
        public override string Message => $"{Response.ReasonPhrase} status code: {Response.StatusCode}";
    }

}
