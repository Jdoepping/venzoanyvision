﻿namespace AnyVisionIntegration
{
    public class Metadata
    {
        public Attributes Attributes { get; set; }
        public int SubjectScore { get; set; }
    }
}