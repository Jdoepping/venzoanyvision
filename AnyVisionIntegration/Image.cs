﻿namespace AnyVisionIntegration
{
    public class Image
    {
        public string Url { get; set; }
        public int FeaturesQuality { get; set; }
        public int ImageType { get; set; }
    }
}