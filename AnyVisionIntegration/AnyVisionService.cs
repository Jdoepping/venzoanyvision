﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AnyVisionIntegration.Interfaces;

namespace AnyVisionIntegration
{
    /// <summary>
    /// The AnyVision socket connection dressed up as a service.
    /// The main contribution is support for renewal of the token;
    /// </summary>
    public class AnyVisionService : IService
    {
        private readonly IAnyVisionClient service;
        private CancellationTokenSource source;
        private Task raiseRenewTokenEventsTask;
        public AnyVisionService(IAnyVisionClient service) => this.service = service;

        /// <summary>
        /// Delay between token renewals
        /// </summary>
        public TimeSpan Delay { get; set; }=TimeSpan.FromMinutes(1);

        public void Dispose()
        {
            Disconnect();
            service.Dispose();
        }

        public event Action TokenExpiredEvent;
        public event Action ServerDisconnected;
        public async Task<bool> Connect()
        {
            try
            {
                if (!(bool)source?.Token.IsCancellationRequested)
                {
                    source.Cancel();
                }
            }
            catch(System.InvalidOperationException)
            {
            }
           
            if (!await service.Connect()) return false;
            source = new CancellationTokenSource();
            
            raiseRenewTokenEventsTask = Task.Run(async()=> await RaiseTokenExpiredEvent(),source.Token);
            //raiseRenewTokenEventsTask = RaiseTokenExpiredEvent();
            //Task.Factory.StartNew(() => RaiseTokenExpiredEvent(), source.Token);
            return true;
        }

        public async Task Disconnect()
        {
            source?.Cancel();
            source?.Dispose();
            // Todo: Isn't there a change it will hang forever here?
            await service.Disconnect();
        }
        /// <summary>
        /// Not sure what happens when a token expires in terms of what the service (websocket) will do.
        /// I assume the service disconnects, but I am not sure.
        /// If the service disconnects and that message is reliable then all this
        /// beautiful code is redundant. 
        /// </summary>
        /// <returns></returns>
        private async Task RaiseTokenExpiredEvent()
        {
            while (true)
            {
                try
                {
                    // Guess it works this way:
                    // The task will await the delay and run while loop 
                    // If source.Cancel is called and exception will be thrown.
                    await Task.Delay(Delay, source.Token);  // t.Wait();
                }
                catch (Exception e)
                {
                    // Looks like a cancellation of a a task must be trapped in try-catch
                    // When the wait is cancelled expired events should not be raised anymore.
                    // Say it fails for other reasons, what should happen then?
                    return;
                }
                TokenExpiredEvent?.Invoke();
            }
        }
    }
}
