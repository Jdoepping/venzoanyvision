﻿using System;

namespace AnyVisionIntegration.Interfaces
{
    /// <summary>
    /// Relays the events back to the background plug-in.
    /// </summary>
    public class SocketActionsImpl
    {
        private readonly Action<string, string> raiseAnalyticalEvent;

        public SocketActionsImpl(H.Socket.IO.SocketIoClient client,
            Action<string, string> raiseAnalyticalEvent)
        {
            this.raiseAnalyticalEvent = raiseAnalyticalEvent;
            client.On("track:created", TrackCreated);
            // Below doesn't work
            //client.On<string>("track:created", TrackCreated);
            client.On("recognition:created", RecognitionCreated);
            client.On("camera:changed", CameraChanged);
            client.On("camera:deleted", CameraDeleted);
        }

        private void TrackCreated(string json) =>
            raiseAnalyticalEvent(SocketEvents.TrackCreated, json);

        private void RecognitionCreated(string json) =>
            raiseAnalyticalEvent(SocketEvents.RecognitionCreated, json);

        private void CameraChanged(string json) =>
            raiseAnalyticalEvent(SocketEvents.CameraChanged, json);

        private void CameraDeleted(string json) =>
            raiseAnalyticalEvent(SocketEvents.CameraDeleted, json);
    }
}
