﻿using System;
using System.Collections.Generic;

namespace AnyVisionIntegration.Models.Cameras
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class FrameSkip
    {
        public int percent { get; set; }
        public bool autoSkipEnabled { get; set; }
    }

    public class CameraPadding
    {
        public string top { get; set; }
        public string left { get; set; }
        public string right { get; set; }
        public string bottom { get; set; }
    }

    public class Configuration
    {
        public bool webRTC { get; set; }
        public bool preview { get; set; }
        public FrameSkip frameSkip { get; set; }
        public List<int> cameraMode { get; set; }
        public CameraPadding cameraPadding { get; set; }
        public string ffmpegOptions { get; set; }
        public int frameRotation { get; set; }
        public int trackMaxLength { get; set; }
        public int trackMinLength { get; set; }
        public double livenessThreshold { get; set; }
        public bool enableFrameStorage { get; set; }
        public int trackerSeekTimeOut { get; set; }
        public int detectionMaxFaceSize { get; set; }
        public int detectionMinFaceSize { get; set; }
    }

    public class LastError
    {
        public string codeString { get; set; }
        public string description { get; set; }
        public int? code { get; set; }
    }

    public class Item
    {
        public string id { get; set; }
        public string title { get; set; }
        public string pipe { get; set; }
        public string description { get; set; }
        public string videoUrl { get; set; }
        public Configuration configuration { get; set; }
        public DateTime createdAt { get; set; }
        public bool isDeleted { get; set; }
        public object deletedAt { get; set; }
        public string status { get; set; }
        public double threshold { get; set; }
        public string timezone { get; set; }
        public bool restriction { get; set; }
        public string cameraGroupId { get; set; }
        public bool isEnabled { get; set; }
        public List<double> location { get; set; }
        public bool isLoadBalancingEnabled { get; set; }
        public bool isAlternativeThresholdEnabled { get; set; }
        public object alternativeThreshold { get; set; }
        public object timeProfileId { get; set; }
        public LastError lastError { get; set; }
        public object sdpFile { get; set; }
        public int unAckCount { get; set; }
    }

    public class Root
    {
        public List<Item> items { get; set; }
        public int total { get; set; }
    }


}
