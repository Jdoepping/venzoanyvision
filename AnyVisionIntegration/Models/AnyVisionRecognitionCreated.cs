﻿using System;
using System.Collections.Generic;

namespace AnyVisionIntegration.Models.RecognitionCreated
{
    // Generated by https://json2csharp.com/
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Camera
    {
        public string id { get; set; }
        public string title { get; set; }
        public List<double> location { get; set; }
        public string cameraGroupId { get; set; }
    }

    public class Attributes
    {
        public List<int> gender { get; set; }
        public List<int> age { get; set; }
    }

    public class Authorization
    {
        public bool restrictedCamera { get; set; }
        public string cameraGroupId { get; set; }
        public string subjectGroupId { get; set; }
        public string status { get; set; }
    }

    public class Metadata
    {
        public Attributes attributes { get; set; }
        public Authorization authorization { get; set; }
        public string subjectId { get; set; }
        public double subjectScore { get; set; }
    }

    public class Image
    {
        public string url { get; set; }
        public int featuresQuality { get; set; }
        public int imageType { get; set; }
    }

    public class CloseMatch
    {
        public string subjectId { get; set; }
        public string featuresId { get; set; }
        public double score { get; set; }
    }

    public class Image2
    {
        public string url { get; set; }
    }

    public class Group
    {
        public string id { get; set; }
        public int th { get; set; }
        public int type { get; set; }
        public string color { get; set; }
        public string title { get; set; }
        public bool isActive { get; set; }
        public DateTime createdAt { get; set; }
        public object deletedAt { get; set; }
        public bool isDefault { get; set; }
        public bool isDeleted { get; set; }
        public string alertLevel { get; set; }
        public string description { get; set; }
    }

    public class Subject
    {
        public string id { get; set; }
        public string name { get; set; }
        public Image image { get; set; }
        public List<Group> groups { get; set; }
        public bool isLocked { get; set; }
        public DateTime createdAt { get; set; }
        public object deletedAt { get; set; }
        public bool isDeleted { get; set; }
        public bool isIgnored { get; set; }
        public int unAckCount { get; set; }
        public string description { get; set; }
        public int subjectSource { get; set; }
        public int timesIdentified { get; set; }
        public object lastIdentifiedAt { get; set; }
    }

    public class Root
    {
        public string id { get; set; }
        public DateTime frameTimeStamp { get; set; }
        public int objectType { get; set; }
        public int cameraType { get; set; }
        public Camera camera { get; set; }
        public int featuresQuality { get; set; }
        public int liveness { get; set; }
        public string collateId { get; set; }
        public object relatedTrackId { get; set; }
        public bool isDeleted { get; set; }
        public object deletedAt { get; set; }
        public Metadata metadata { get; set; }
        public List<Image> images { get; set; }
        public string subjectFeaturesId { get; set; }
        public double subjectScore { get; set; }
        public List<CloseMatch> closeMatches { get; set; }
        public Subject subject { get; set; }
    }



}
