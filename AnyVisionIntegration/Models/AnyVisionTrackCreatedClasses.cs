﻿using System;
using System.Collections.Generic;

namespace AnyVisionIntegration.Models.TrackCreated
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Camera
    {
        public string id { get; set; }
        public string title { get; set; }
        public List<double> location { get; set; }
        public string cameraGroupId { get; set; }
    }

    public class Attributes
    {
        public List<int> gender { get; set; }
        public List<string> topColor { get; set; }
        public List<string> bottomColor { get; set; }
        public List<bool> backpack { get; set; }
        public List<int> age { get; set; }
    }

    public class Authorization
    {
        public bool restrictedCamera { get; set; }
        public string cameraGroupId { get; set; }
        public string status { get; set; }
    }

    public class Acknowledged
    {
        public string id { get; set; }
        public string title { get; set; }
        public bool autoAck { get; set; }
        public DateTime createdAt { get; set; }
        public string username { get; set; }
        public object userId { get; set; }
        public bool systemGenerated { get; set; }
    }

    public class Metadata
    {
        public Attributes attributes { get; set; }
        public Authorization authorization { get; set; }
        public double subjectScore { get; set; }
        public List<Acknowledged> acknowledged { get; set; }
    }

    public class Image
    {
        public string url { get; set; }
        public int featuresQuality { get; set; }
        public int imageType { get; set; }
    }

    public class Root
    {
        public string id { get; set; }
        public DateTime frameTimeStamp { get; set; }
        public int objectType { get; set; }
        public int cameraType { get; set; }
        public Camera camera { get; set; }
        public int featuresQuality { get; set; }
        public int liveness { get; set; }
        public string collateId { get; set; }
        public string relatedTrackId { get; set; }
        public bool isDeleted { get; set; }
        public object deletedAt { get; set; }
        public Metadata metadata { get; set; }
        public List<Image> images { get; set; }
        public double subjectScore { get; set; }
        public List<object> closeMatches { get; set; }
        public object subject { get; set; }
    }


}
