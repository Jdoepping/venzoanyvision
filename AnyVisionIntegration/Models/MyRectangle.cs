﻿namespace AnyVisionIntegration.Models
{
    public class MyRectangle
    {
        public int Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool IsSelected { get; set; }
    }
}
