﻿using Newtonsoft.Json;

namespace AnyVisionIntegration.Models
{
    public static class AnyVisionClassExtension
    {
        public static T GetRoot<T>(string json) => JsonConvert.DeserializeObject<T>(json);
    }
}
