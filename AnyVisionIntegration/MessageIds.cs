﻿namespace AnyVisionIntegration
{
    public class MessageIds
    {
        public const string ConnectedEvent = "com.anyvision.milestoneintegration.ConnectedEvent";
        public const string DisconnectedEvent = "com.anyvision.milestoneintegration.DisconnectedEvent";
        //public const string AddToSelectedCommand = "www.companyname.com.AddToSelectedCommand";
        //public const string AnyVisionWebSocketSettingsChangedIndication = "www.companyname.com.WebSocketSettingsChangedIndication";

    }
}
