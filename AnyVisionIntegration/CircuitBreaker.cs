﻿using System;
using System.Threading.Tasks;
using AnyVisionIntegration.Interfaces;

namespace AnyVisionIntegration
{
    /// <summary>
    /// A simple circuit breaker.
    /// At present it handles a single action.
    /// Has a few hardcoded values
    /// </summary>
    public class CircuitBreaker : ICircuitBreaker
    {
        private const int MaxRetry = 100;
        /// <summary>
        /// All operations are forced to expire after 10 minutes
        /// </summary>
        private const int ExpireTime = 1000 * 60 * 10;
        private const int DefaultDelay = 1000*60*5; // 5 minutes
        private int retryCount;
        private readonly Func<Exception, bool> isTransient;
        private readonly Action<Exception> report;
        private readonly TimeSpan delay;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="delay">The time delay between retrying an operation</param>
        /// <param name="isTransient">A method that answers whether the error can be transient. Is there a change a second try will succeed</param>
        /// <param name="report">could be logging could be something more elaborate</param>
        public CircuitBreaker(TimeSpan delay,Func<Exception, bool> isTransient = null, Action<Exception> report = null)
        {
            this.delay = delay;
            this.isTransient = isTransient == null ? ex => true : isTransient;
            this.report = report == null ? ex => { } : report;
        }

        public bool IsRunning { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="func">the function to try</param>
        /// <returns></returns>
        public async Task StartRetry(Func<Task<bool>> func)
        {
            if (IsRunning) return;
            IsRunning = true;
            while (retryCount<MaxRetry)
            {
                await Task.Delay(delay);
                try
                {
                    retryCount++;
                    var task = func();
                    if (task.Wait(ExpireTime)) break;
                }
                catch ( Exception e)
                {
                    report(e);
                    if (!isTransient(e))break;
                }
            }
            retryCount = 0;
            IsRunning = false;
        }
    }
}
