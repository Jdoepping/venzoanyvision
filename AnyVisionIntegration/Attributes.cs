﻿using System.Collections.Generic;

namespace AnyVisionIntegration
{
    public class Attributes
    {
        public List<int> Gender { get; set; }
        public List<int> Age { get; set; }
    }
}