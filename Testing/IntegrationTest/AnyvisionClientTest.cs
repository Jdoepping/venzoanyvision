﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AnyVisionIntegration.ApiClasses;
using AnyVisionIntegration.Interfaces;
using AnyVisionIntegration.WebClient;
using Newtonsoft.Json;
using VideoOS.CustomDevelopment.PublicStuff;
using VideoOS.Platform.Data;
using Xunit;
using Xunit.Abstractions;

namespace Testing.IntegrationTest
{
    /// <summary>
    /// These are integration test, that assumes access to a running AnyVision server.
    /// So, they cannot run on attended.
    /// The test will raise in complexity:
    /// 1. GetTokenTest: Essentially can will a login succeed
    /// 2. ConnectTest: Can a socket be established
    /// 3. ConnnectAndListen: Connect to socket and get a reply from the socket 
    /// </summary>
    public class AnyVisionClientTest 
    {
        private readonly ITestOutputHelper testOutputHelper;
        private readonly ILogging logNothing;
        private bool eventReceivedFromAnyVision;
        private readonly Settings settings;

        public AnyVisionClientTest(ITestOutputHelper testOutputHelper)
        {
            this.testOutputHelper = testOutputHelper;
            logNothing = new LogNothing();
            settings = new Settings();
        }
        /// <summary>
        /// This is also a login request. A successful login returns a token.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task GetTokenTest()
        {
            var httpClient = new AnyVisionHttpClient(settings.BaseUrl);
            var response = await httpClient.Connect(settings.UserName, settings.Password);
            Assert.True(response.IsSuccessStatusCode, response.StatusCode.ToString());
        }

        [Fact]
        public async Task GetCamerasTest()
        {
            var client = await GetClient();
            var response = await client.Cameras();
            Assert.True(response.IsSuccessStatusCode, response.StatusCode.ToString());
            var cams = await client.ExtractResponse<AnyVisionIntegration.Models.Cameras.Root>(response);
        }

        [Fact]
        public async Task ConnectTest()
        {
            var client = new AnyVisionClient(new Settings(), RaiseAnalyticalEvent, OnServerEventReceived, logNothing);
            var success = await client.Connect();
            Assert.True(success);
        }

        [Fact]
        public async Task ConnectAndListenTest()
        {
            var client = new AnyVisionClient(new Settings(), RaiseAnalyticalEvent, OnServerEventReceived, logNothing);
            var success = await client.Connect();
            Assert.True(success);
            while (!eventReceivedFromAnyVision)
            {
                await Task.Delay(TimeSpan.FromMilliseconds(200));
                //Console.WriteLine("Running");
            }

            var task = Task.Run(async () =>
            {
                while (!eventReceivedFromAnyVision)
                {
                    await Task.Delay(TimeSpan.FromMilliseconds(200));
                    //testOutputHelper.WriteLine("Running");
                }
            });
            Assert.True(task.Wait(TimeSpan.FromMinutes(10)), "No detections received. Might be OK");
        }

        private async Task<AnyVisionHttpClient> GetClient()
        {
            var c = new AnyVisionHttpClient(settings.BaseUrl);
            await c.AddBearerToken(settings.UserName, settings.Password);
            return c;
        }

        private void RaiseAnalyticalEvent(string eventId, string json) => eventReceivedFromAnyVision = true;

        private void OnServerEventReceived(string eventId, string json)
        {
            eventReceivedFromAnyVision = true;
        }

        public class Settings : IAnyVisionSettings
        {
            public string UserName => "jakob";

            public string Password => "Anyvision1!";

            public string BaseUrl => "http://172.20.48.78";

            public bool TrackCreated => true;

            public bool RecognitionCreated => true;

            public bool CameraChanged => true;

            public bool CameraDeleted => true;
        }
    }
}
