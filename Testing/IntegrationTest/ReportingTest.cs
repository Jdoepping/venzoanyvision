﻿using System;
using System.Linq;
using MilestoneSdkExtensions.Miscellaneous;
using SelectableRectangles.Background;
using VideoOS.CustomDevelopment.PublicStuff;
using Xunit;

namespace Testing.IntegrationTest
{
    /// <summary>
    /// Test the <see cref="AnyVisionSocketEventListenerPlugin.Reporting"/> class.
    /// The class stores information in Milestone items. The test can only run where a Milestone system
    /// is installed and requires username and password.
    /// </summary>
    public class ReportingTest: IClassFixture<LoginFixture>,IDisposable
    {
        public ReportingTest(LoginFixture loginFixture) => loginFixture.LoginCurrentWindowsUser();
        /// <summary>
        /// Creates the reporting item and writes an item to it.
        /// </summary>
        [Fact]
        public void WriteTest()
        {
            var rep = new AnyVisionSocketEventListenerPlugin.Reporting(log: new LogNothing());
            var entries = rep.GetEntries();
            var entryCount = entries.Count();
            rep.Report( new Entry
            {
                TimeStamp = DateTime.Now,
                Reason = "Created by Test",
                ErrorMessage = "A test entry"
            });
            Assert.Equal(expected: entryCount+1, actual: entries.Count());
        }
        public void Dispose()
        {
        }
    }
}

