﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Extensions.Messaging;
using SelectableRectangles.Background;
using VideoOS.CustomDevelopment.PublicStuff;
using Xunit;
using Xunit.Abstractions;

namespace Testing.IntegrationTest
{
    /// <summary>
    /// Testing the <see cref="AnyVisionSocketEventListenerPlugin"/> class.
    /// </summary>
    public class AnyVisionSocketEventListenerPluginTest:IClassFixture<LoginFixture>,IDisposable
    {
        private readonly ITestOutputHelper testOutputHelper;
        public AnyVisionSocketEventListenerPluginTest(LoginFixture loginFixture, ITestOutputHelper testOutputHelper)
        {
            loginFixture.LoginCurrentWindowsUser();
            this.testOutputHelper = testOutputHelper;
        }

        /// <summary>
        /// This test will just run the plugin in as many minutes as specified.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task RunTheAnyVisionSocketEventListenerPluginTest()
        {
            var log = new Logging(testOutputHelper);
            var plugin = new AnyVisionSocketEventListenerPlugin(log);
            plugin.Init();
            await Task.Delay(TimeSpan.FromMinutes(10));
            plugin.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ServiceDisconnectedMessageRaisedTest()
        {
            var externalMessenger = new SimpleMessaging(MessagingScope.External);
            var log = new Logging(testOutputHelper);
            var plugin = new AnyVisionSocketEventListenerPlugin(log);
            plugin.Init();
            await Task.Delay(TimeSpan.FromMinutes(10));
            plugin.Close();
        }
        public void Dispose()
        {
        }

        #region Helper logging class
        private class Logging : ILogging
        {
            private readonly ITestOutputHelper testOutputHelper;
            public Logging(ITestOutputHelper testOutputHelper ) => this.testOutputHelper = testOutputHelper;
            public void Log(string className, bool isError, string methodName, string msg)=>
               testOutputHelper.WriteLine($"{className} IsError:{isError}, Method:{methodName}, Msg{msg}");
            public void LogDebug(LogLevelEnum logLevel, string className, string methodName) =>
                testOutputHelper.WriteLine($"{className}, Method:{methodName}");
            public void LogDebug(LogLevelEnum logLevel, string className, string methodName, string message) => testOutputHelper.WriteLine($"{className}, Method:{methodName}");
            public void LogError(string className, string methodName, string msg) => testOutputHelper.WriteLine($"{className}, Method:{methodName}, Msg{msg}");
            public void LogException(string className, string methodName, Exception ex) => testOutputHelper.WriteLine($"{className}, Method:{methodName},Exception:{ex.Message}");
            public void LogException(string className, string methodName, string msg, Exception ex) => testOutputHelper.WriteLine($"{className}, Method:{methodName}, Msg{msg}, Exception:{ex.Message}");
            public void LogException(Exception ex) => testOutputHelper.WriteLine($"Exception:{ex.Message}");
            public void WriteLogEntry(string className, bool isError, string methodName, string message) => throw new NotImplementedException();
        }
        #endregion Helper logging class
    }
}
