﻿using System;
using VideoOS.Platform;
using Xunit;
using Env = VideoOS.Platform.SDK.Environment;

namespace Testing
{
    public class LoginFixture : IDisposable
    {
        public void LoginCurrentWindowsUser()
        {
            Env.Initialize();
            var uri = EnvironmentManager.Instance.MasterSite.ServerId.Uri;
            var credentialCache = VideoOS.Platform.Login.Util.BuildNetworkCredential(uri,
                "",
                "",
                "Negotiate");
            Common(credentialCache, uri);
        }
        /// <summary>
        /// The parameter credentialCache is either:
        /// 1. System.Net.CredentialCache
        /// 2. System.Net.NetworkCredential
        /// </summary>
        /// <param name="credentialCache"></param>
        /// <param name="uri"></param>
        private void Common(dynamic credentialCache, Uri uri)
        {
            Env.RemoveAllServers();
            // Observe the usage of uri!
            Env.AddServer(uri, credentialCache);
            try
            {
                Env.Login(uri);
            }
            catch (Exception ex)
            {
                Assert.True(false, $"Login failed: {ex.Message}");
            }
            Assert.True(true);
        }
        public void Dispose()
        {
        }
    }
}
