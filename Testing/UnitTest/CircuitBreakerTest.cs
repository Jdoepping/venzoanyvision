﻿using System;
using System.Threading.Tasks;
using AnyVisionIntegration;
using Xunit;

namespace Testing.UnitTest
{
    /// <summary>
    /// Testing <see cref="AnyVisionIntegration.CircuitBreaker"/>
    /// NB This tests runs for a long time.
    /// </summary>
    public class CircuitBreakerTest
    {
        private static int retries;
        /// <summary>
        /// Will retry until max 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task RetryUntilMaxTest()
        {
            retries = 0;
            var cb = new CircuitBreaker(TimeSpan.FromMilliseconds(5), exception => true, exception => { });
            await cb.StartRetry(IncrementRetries);
            Assert.True(retries==100);
        }

        /// <summary>
        /// New calls to <see cref="CircuitBreaker.StartRetry"/> should be discarded
        /// when a call is in progress.
        /// NB This CircuitBreaker supports a single call type. It blocks all calls
        /// regardless of whether they are different in type from the currently blocking call. 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task CallDiscardedWhenCircuitIsBlockedTest()
        {
            retries = 0;
            var cb = new CircuitBreaker(TimeSpan.FromMilliseconds(5), exception => true, exception => { });
            cb.StartRetry(Block);
            for (var i = 0; i < 10; i++)
            {
                // should terminate immediately 
                await cb.StartRetry(IncrementRetries);
            }
            while (cb.IsRunning)
            {
                await Task.Delay(10);
            }
            // Is blocked until Block method returns.
            Assert.True(retries == 1, retries.ToString());
        }

        /// <summary>
        /// The circuit breaker should timeout operations that run for too long time.
        /// The time is now set to 2 minutes.
        /// If "retries" are greater than 1 it means the <see cref="ReduceBlockTimeOnEachRun"/> has
        /// been called more than once. And this means circuit breaker timed out the operation.
        /// For the test to eventually stop the delay is reduced on each run, so that in the
        /// end the <see cref="ReduceBlockTimeOnEachRun"/> method will complete and return true. 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task MustNotBlockForeverTest()
        {
            retries = 0;
            var cb = new CircuitBreaker(TimeSpan.FromMilliseconds(5), exception => true, exception => { });
            await cb.StartRetry(ReduceBlockTimeOnEachRun);
            Assert.True(retries>1, retries.ToString());
        }

        private async Task<bool> IncrementRetries()
        {
            retries++;
            return false;
        }
        private async Task<bool> Block()
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            retries++;
            return true;
        }
       
        private async Task<bool> ReduceBlockTimeOnEachRun()
        {
            retries++;
            await Task.Delay(TimeSpan.FromSeconds(240 / retries));
            return true;
        }
    }
}
