﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AnyVisionIntegration;
using Xunit;
using Xunit.Abstractions;

namespace Testing.UnitTest
{
    public class AnyVisionServiceTest
    {
        private readonly ITestOutputHelper output;
        private int called;

        public AnyVisionServiceTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public async Task ConnectTest()
        {
            var dummy = new Dummy();
            var service = new AnyVisionService(dummy);
            await service.Connect();
            await service.Disconnect();
            //await Task.Delay(TimeSpan.FromMinutes(2));
        }

        [Fact]
        public async Task ConnectRenewToken()
        {
            var dummy = new Dummy();
            var service = new AnyVisionService(dummy)
            {
                Delay = TimeSpan.FromMinutes(2)
            };
            service.TokenExpiredEvent += Service_TokenExpiredEvent;
            await service.Connect();
            while (called<3) await Task.Delay(100);
            await service.Disconnect();
            Assert.True(called==3);
        }

        private void Service_TokenExpiredEvent() => called++;

        /// <summary>
        /// Trying to understand async. I will never get it :( 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestMe()
        {
            var source = new CancellationTokenSource();
            // Starts the task. Without additional code this method will finish.
            // The method owns the task "t", which is disposed when the method finishes, 
            // the method doesn't wait for the task to complete. Adding an await would make 
            // method wait and a avoid the method terminating before the task has finished.
            // 
            // Task.Wait is used synchronize the method, perhaps it is an implicit await. Remember 
            // await allows other processes to continue.
            // 
            var t = Task.Run(async ()=>
            {
                await Task.Delay(TimeSpan.FromSeconds(1.5), source.Token);
                output.WriteLine("I am done now");
                return 42;
            });
            source.Cancel();
            try
            {
                t.Wait();
            }
            catch (AggregateException ae)
            {
                foreach (var e in ae.InnerExceptions)
                    output.WriteLine("{0}: {1}", e.GetType().Name, e.Message);
            }
            output.WriteLine("Task t Status: {0}", t.Status);
            if (t.Status == TaskStatus.RanToCompletion)
                output.WriteLine(", Result: {0}", t.Result);
            source.Dispose();
        }

    }
}
