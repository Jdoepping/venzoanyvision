﻿using System;
using System.Threading.Tasks;
using AnyVisionIntegration.Interfaces;

namespace Testing.UnitTest
{
    internal class Dummy : IAnyVisionClient
    {
        public void Dispose()
        {
        }

        public event Action OnDisconnectEvent;
        public async Task<bool> Connect() => true;

        public async Task Disconnect()
        {
            //await Task.Delay(10);
        }

        public SocketActionsImpl SocketActions { get; }
        public ISocketEventHandlers WebClientEventHandlers { get; }
    }
}