﻿Installed XUnit and XUnit.runner.VisualStudio
When XUnit.runner.VisualStudio was not installed the test would not run in Test Explorer


My ambition was to use a Core 3.2 project.
But VideoOS.Platform.SDK depends on System.Windows.Forms which cannot (or I don't know how) be loaded 
in a Core 3.2 project. 
I tried this: https://stackoverflow.com/questions/59890543/unable-to-resolve-system-windows-forms-dll-in-net-core-3-1
but alas it did not do me any good.