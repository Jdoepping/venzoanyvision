﻿using System;
using System.Linq;
using System.Threading.Tasks;
using VideoOS.Platform;
using VideoOS.Platform.ConfigurationItems;
using MilestoneSdkExtensions;
using Xunit;


namespace Testing
{
    public class AnalyticalEventsTest : IClassFixture<LoginFixture>,IDisposable
    {
        private readonly LoginFixture  login;
        private readonly ManagementServer managementServer;
        /// <summary>
        /// The constructor is run for each test. The LoginFixture is only created once.
        /// </summary>
        /// <param name="login"></param>
        public AnalyticalEventsTest(LoginFixture login)
        {
            this.login = login;
            login.LoginCurrentWindowsUser();
            managementServer = new ManagementServer(EnvironmentManager.Instance.MasterSite.ServerId); 
           
        }

        [Fact]
        public async Task AddAnalyticalEvents()
        {
            var toAdd = new[] { "DeleteMe01", "DeleteMe02"};
            var f= managementServer.AnalyticsEventFolder;
            Assert.All(toAdd,s=>Assert.True(f.NotExists(s), $"The anal event {s} exists"));
            var tasks = await f.Create(toAdd);
            Assert.True(tasks.All(t=>t.State== StateEnum.Success));
        }

        [Fact]
        public async Task RemoveAnalyticalEvents()
        {
            var toAdd = new[] { "DeleteMe01", "DeleteMe02" };
            var f = managementServer.AnalyticsEventFolder;
            Assert.All(toAdd, s => Assert.True(f.Exists(s),$"the anal event {s} doesn't exists"));
            var tasks= await f.Remove(toAdd);
            Assert.True(tasks.All(t => t.State == StateEnum.Success));
        }

        public void Dispose()
        {
        }


    }
}
