﻿using System;
using VideoOS.Platform;
using VideoOS.Platform.ConfigurationItems;
using Env = VideoOS.Platform.SDK.Environment;

namespace Testing
{
    /// <summary>
    /// Get a fresh ManagementServer.
    /// For this to work I think, a login must have been done.
    /// </summary>
    public class ManagementServerFixture : IDisposable
    {
        public ManagementServerFixture()
        {
            Env.Initialize();
            // Below is only necessary in a stand alone environment. Not needed in plug-ins
            GetManagementServer = new ManagementServer(EnvironmentManager.Instance.MasterSite.ServerId);
        }

        public ManagementServer GetManagementServer { get; }

        public void Dispose()
        {
        }
    }
}
