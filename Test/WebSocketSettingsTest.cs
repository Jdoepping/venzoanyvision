﻿using Xunit;

namespace Test
{
    public class WebSocketSettingsTest
    {
        [Fact]
        public void InitializedTest()
        {
            var w = new BitmapProperties(0);
            Assert.False(w.TrackCreated);
            Assert.False(w.CameraChanged);
            Assert.False(w.CameraDeleted);
            Assert.False(w.RecognitionCreated);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(5)]
        [InlineData(6)]
        [InlineData(7)]
        [InlineData(8)]
        [InlineData(9)]
        [InlineData(10)]
        [InlineData(11)]
        [InlineData(12)]
        [InlineData(13)]
        [InlineData(14)]
        [InlineData(15)]
        public void DifferentStartValuesTest(ushort storage)
        {
            var w = new BitmapProperties(storage);
            Assert.True(w.TrackCreated==((storage & 1)==1));
            Assert.True(w.RecognitionCreated == ((storage & 2) == 2));
            Assert.True(w.CameraChanged == ((storage & 4) == 4));
            Assert.True(w.CameraDeleted == ((storage & 8) == 8));
            
        }
        [Theory]
        [InlineData(0)]
        [InlineData(2)]
        [InlineData(4)]
        [InlineData(8)]
        public void SetFirstValueTest(ushort storage)
        {
            var w = new BitmapProperties(storage);
            w.TrackCreated = true;
            Assert.True(w.TrackCreated);
            Assert.True(w.RecognitionCreated == ((storage & 2) == 2));
            Assert.True(w.CameraChanged == ((storage & 4) == 4));
            Assert.True(w.CameraDeleted == ((storage & 8) == 8));
        }
        [Theory]
        [InlineData(13)]
        public void SetSecondValueTest(ushort storage)
        {
            var w = new BitmapProperties(storage);
            foreach (var b in new[]{true, false})
            {
                w.RecognitionCreated = b;
                Assert.True(w.TrackCreated == ((storage & 1) == 1));
                Assert.True(w.RecognitionCreated==b, $"changed not correct value {b}");
                Assert.True(w.CameraChanged == ((storage & 4) == 4));
                Assert.True(w.CameraDeleted == ((storage & 8) == 8));
            }
        }
       [Fact]
        public void SetAllValuesTest()
        {
            var w = new BitmapProperties(0);
            foreach (var b in new[] { true, false })
            {
                w.CameraChanged = b;
                w.TrackCreated = b;
                w.CameraDeleted = b;
                w.RecognitionCreated = b;
                Assert.True(w.TrackCreated == b);
                Assert.True(w.RecognitionCreated == b, $"changed not correct value {b}");
                Assert.True(w.CameraChanged == b);
                Assert.True(w.CameraDeleted == b);
            }
        }

    }
    public class BitmapProperties 
    {
        private uint storage;
        public BitmapProperties(ushort storage) => this.storage = storage;

        public bool TrackCreated { get => G(1); set => S(1, value); }
        public bool RecognitionCreated { get => G(2); set => S(2, value); }
        public bool CameraChanged { get => G(4); set => S(4, value); }
        public bool CameraDeleted { get => G(8); set => S(8, value); }

        private bool G(uint index) => (storage & index) > 0;

        private void S(uint index, bool value)
        {
            if (G(index) == value) return;
            storage =value ? storage | index : storage &~index;
        }
    }
}
