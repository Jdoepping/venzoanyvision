﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using SelectableRectangles.Background;
using SelectableRectangles.Background.ApiClasses;
using SelectableRectangles.Shared;
using WpfTest.Annotations;

namespace WpfTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private SidebarData sideBar;
        public MainWindow()
        {
            InitializeComponent();
            //var api = new AnyVisionApi();
            //Task.Run(async () =>
            //{
            //    Sidebar = await api.GetSidebarData();

            //}).GetAwaiter().GetResult();
            ////SubjectGroup.DataContext = Sidebar;
            ////SubjectGroup.Name = Sidebar.SubjectName;
            //Sidebar = null;
            DataContext = this;
        }
        /*
         * The SubjectGroup (the instance of PropertiesImageFragment) has many properties.
         * The SidebarData will map to these data, but perhaps not 1 to 1.
         * The PropertiesImageFragement (usercontrol) is not likely to be reused. 
         */
        public SidebarData Sidebar
        {
            get => sideBar;
            set
            {
                sideBar = value;
                AddToSearchButton.IsEnabled = value != null;
                OnPropertyChanged(nameof(Sidebar));
            }
        }

        public string MyText
        {
            get => textBlock.Text;
            set
            {
                textBlock.Text = value;
                OnPropertyChanged(nameof(MyText));
            }
        }

        private async void FetchData(object sender, RoutedEventArgs e)
        {
            var api = new AnyVisionApi();
            var res = await api.GetSidebarData();
            var image = new BitmapImage(new Uri(@"C:\Users\jdoep\source\repos\Milestone\Venzo\SelectableRectangles\SelectableRectangles\Images\jakob.jpg", UriKind.Relative));
            //Utilities.SavePhoto(image);
            res.PictureCutOut = Utilities.GetBitmapImageAsByteArray(image);
            Sidebar = res;
            MyText = DateTime.Now.ToLongDateString();
            //PropertiesImageFragment.SetImageSource();

            //LocalMainImage.Source = new BitmapImage(new Uri("https://upload.wikimedia.org/wikipedia/commons/3/30/Googlelogo.png"));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
