﻿using System;
using AnyVisionIntegration;
using Newtonsoft.Json;
using VideoOS.Platform;
using VideoOS.Platform.Data;
using VideoOS.Platform.Messaging;

namespace SelectableRectangles.Background.Events
{
    /// <summary>
    /// The event provider will raise events when they are received from
    /// AnyVision. 
    /// </summary>
    public class AnyVisionEventProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="trackDataJson"></param>
        public void TrackCreatedEventReceived(string trackDataJson)
        {
            if (string.IsNullOrEmpty(trackDataJson)) return; 
            var trackData = JsonConvert.DeserializeObject<TrackData>(trackDataJson);
            var cameraItem = GetCameraId(trackData);
            var eventSource = new EventSource()
            {
                // Send empty - it is possible to send without an eventsource, but the intended design is that there should be a source
                // the FQID is primarily used to match up the ObjectId with a camera.
                FQID = cameraItem.FQID,
                // If FQID is null, then the Name can be an IP address, and the event server will make a lookup to find the camera
                Name = cameraItem.Name
            };
        }

        public void RaiseAnalyticalEventTest(EventSource eventSource)
        {
            var eventHeader = new EventHeader
            {
                ID = Guid.NewGuid(),
                Type = "AnyVisionWebSocket",
                Timestamp = DateTime.Now,
                Message = "AnyVisionTrackCreatedEvent", // identifies the event
                Source = eventSource,
                CustomTag = "AnyVision"
            };

            var analyticEvent = new AnalyticsEvent
            {
                EventHeader = eventHeader,
                Location = "Event location 1",
                Description = "Analytics event description.",
                Vendor = new Vendor { Name = "AnyVision" },
            };
            EnvironmentManager.Instance.SendMessage(new Message(MessageId.Server.NewEventCommand) { Data = analyticEvent });
            //MessageId.Server.NewEventIndication
            // Can I listen to my own event? 
        }

        private Item GetCameraId(TrackData trackData)
        {
            throw new NotImplementedException();
        }
    }
}
