﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelectableRectangles.Background.AnyVisionEvents
{
    /// <summary>
    /// The events provided by AnyVisions Web-Socket.
    /// </summary>
    public enum AnyvisionEventTypes
    {
        TractCreated,
        RecognitionCreated,
        CameraChanged,
        CameraDeleted,
    }
}
