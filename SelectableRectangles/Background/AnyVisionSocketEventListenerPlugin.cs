﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminUi.Interfaces;
using AnyVisionIntegration;
using AnyVisionIntegration.Interfaces;
using Extensions.Messaging;
using SelectableRectangles.Admin.Item;
using MilestoneSdkExtensions;
using SelectableRectangles.Admin;
using SelectableRectangles.Background.Exceptions;
using VideoOS.CustomDevelopment.Implementations.Logging;
using VideoOS.Platform;
using VideoOS.Platform.ConfigurationItems;
using VideoOS.Platform.Messaging;
using VideoOS.CustomDevelopment.PublicStuff;
using VideoOS.Platform.Data;

namespace SelectableRectangles.Background
{
    /// <summary>
    /// Listens to the events raised by an AnyVision Server.
    /// 1. The connected state should be monitored.
    /// 2. In case a connection is closed unexpectedly an alarm/warning should be raised.
    /// 3. It must be possible to open and close a connection to the server.
    /// 4. Analytical events should be raised on events from the server.
    /// 5. If the analytical events does not exists, they will be created. 
    /// </summary>
    public partial class AnyVisionSocketEventListenerPlugin: VideoOS.Platform.Background.BackgroundPlugin
    {
        internal const string ReportItemGuid = "{5462AAD2-1A41-4F8E-96AD-FAEDEB721DF9}";
        private const int TokenRenewDelayInMinutes =  5;
        private SimpleMessaging externalMessenger;
        private SimpleMessaging internalMessenger;
        // This client implements what happens on events from the AnyVisionServer
        private IAnyVisionClient anyVisionClient;
        private LifeTime lifeTime;
        private ILogging log;
        private IAnyVisionProperties currentSettings;

        #region constructors
        public AnyVisionSocketEventListenerPlugin()
        {
        }
        /// <param name="log"></param>
        public AnyVisionSocketEventListenerPlugin(ILogging log) => this.log = log;
       
        #endregion

        public override Guid Id => PluginDefinition.AnyVisionSocketEventListenerPluginId;
        public override string Name => "Venzo AnyVision Server Events";
        public override void Init()
        {
            log = log ?? new MipLogger(nameof(AnyVisionSocketEventListenerPlugin), LogLevelEnum.Debug);
            internalMessenger = new SimpleMessaging(MessagingScope.Internal);
            externalMessenger = new SimpleMessaging(MessagingScope.External);
            //externalMessenger.Register(WebSocketSettingsChanged,
            //    PluginDefinition.MessageIds.AnyVisionWebSocketSettingsChangedIndication);
            externalMessenger.Register(WebSocketSettingsChanged, PluginDefinition.MessageIds.ReconnectWebSocket);
            externalMessenger.Register(AnswerStateRequest, PluginDefinition.MessageIds.StateRequest);
            externalMessenger.Register<int>(TokenRenewalChanged, PluginDefinition.MessageIds.TokenRenewalChanged);
            StartListeningToAnyVisionEvents();
            Task.Run(async () => await lifeTime.Connect());
        }

        private void AnswerStateRequest() => externalMessenger.Send(PluginDefinition.MessageIds.StateIndication, 
            lifeTime?.State.ToString() ?? "Unknown");
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenRenewal">in minutes</param>
        private void TokenRenewalChanged(int tokenRenewal) => lifeTime.TokenRenewal = TimeSpan.FromMinutes(tokenRenewal);

        /// <summary>
        /// Reply with the current state of the service.
        /// </summary>
        private void ServerStateIndication() => 
            externalMessenger.Send(PluginDefinition.MessageIds.StateIndication,lifeTime.State.ToString());

        public override void Close()
        {
            anyVisionClient?.Dispose();
            internalMessenger.Close();
            externalMessenger.Close();
            RecycleServices();
        }
        /// <summary>
        /// On changed settings (base url or subscription) everything is recycled.
        /// It is assumed to be a very rare event..
        /// </summary>
        private async void WebSocketSettingsChanged()
        {
            StartListeningToAnyVisionEvents();
            await lifeTime.Connect();
        }

        #region Listening to anyvision web socket
        private  void StartListeningToAnyVisionEvents(IAnyVisionProperties item =null)
        {
            currentSettings = item ?? Constants.GetAnyVisionSettings();
            if (!HasSettingsEnablingConnection(currentSettings)) return;
            // Policy: Create the Analytical events if they are not already created.
            CreateAnalyticalEventsTypes(log);
            if (!HasSubscriptions(currentSettings)) return;
            if (IsARestart())
            {
                internalMessenger.Send(PluginDefinition.MessageIds.Disconnected);
                RecycleServices();
            }
            try
            {
                anyVisionClient = new AnyVisionClient(new AnyVisionSettingsImpl(currentSettings), RaiseAnalyticalEvent, OnServerEventReceived, log);
                var service = new AnyVisionService(anyVisionClient)
                {
                    Delay = TimeSpan.FromMinutes(int.Parse(currentSettings.TokenRenewalTime))
                };
                lifeTime = new LifeTime(service,new CircuitBreaker(TimeSpan.FromMinutes(TokenRenewDelayInMinutes)),
                    (e)=>log.LogException(e),
                    new Reporting(log).Report);
                lifeTime.ConnectedEvent += LifeTime_ConnectedEvent;
                lifeTime.ConnectingEvent += LifeTime_ConnectingEvent;
                lifeTime.DisconnectedEvent += LifeTime_DisconnectedEvent;
                lifeTime.NoneTransientErrorEvent += LifeTime_NoneTransientErrorEvent;
            }
            // Pretty sure the background plug-in swallows bugs it is weird though
            catch (Exception e)
            {
                // write this to the item and log it and raise and event that can be used to raise an alarm.
                log.LogException("AnyVisionSocketPlugin", "StartListeningToAnyVisionEvents",e);
                WriteExceptionToItem(e);
                externalMessenger.Send(PluginDefinition.MessageIds.AnyVisionWebSocketConnectFailed);
            }
        }

        private bool HasSettingsEnablingConnection(IAnyVisionProperties s) => !string.IsNullOrEmpty(s.BaseUrl);
        private void LifeTime_NoneTransientErrorEvent(string obj) => 
            internalMessenger.Send(PluginDefinition.MessageIds.NoneTransientError, obj);
        private void LifeTime_DisconnectedEvent()
        {
            RaiseEvents(Constants.Disconnected);
            internalMessenger.Send(PluginDefinition.MessageIds.Disconnected);
        }
        private void LifeTime_ConnectingEvent()
        {
            RaiseEvents(Constants.Connecting);
            internalMessenger.Send(PluginDefinition.MessageIds.Connecting);
        }

        private void LifeTime_ConnectedEvent()
        {
            RaiseEvents(Constants.Connected);
            internalMessenger.Send(PluginDefinition.MessageIds.Connected);
        }

        private bool IsARestart() => lifeTime != null || anyVisionClient != null;
        private void RecycleServices()
        {
            lifeTime.ConnectedEvent -= LifeTime_ConnectedEvent;
            lifeTime.ConnectingEvent -= LifeTime_ConnectingEvent;
            lifeTime.DisconnectedEvent -= LifeTime_DisconnectedEvent;
            lifeTime.NoneTransientErrorEvent -= LifeTime_NoneTransientErrorEvent;
            lifeTime.Close();
        }

        private void WriteExceptionToItem(Exception e)
        {
            var p = ((AnyVisionProperties) Constants.GetAnyVisionSettings());
            p.LastErrorMessage = e.Message;
            p.LastErrorMessageTime  = DateTime.Now;
            Configuration.Instance.SaveItemConfiguration(PluginDefinition.PluginId,p.MyItem);
        }

        private void RaiseEvents(EventType eventType)
        {
            var eventData = new EventData
            {
                EventHeader =
                    new EventHeader
                    {
                        ID = Guid.NewGuid(),
                        Type = "Server State",
                        Timestamp = DateTime.Now,
                        Message = eventType.Message,
                        Source = new EventSource()
                        {
                            FQID = Constants.ItemFqid(),
                            Name = "AnyVision Event Source"
                        },
                        CustomTag = "AnyVision Server Event",
                        MessageId = eventType.ID
                    }
            };
            internalMessenger.Send(MessageId.Server.NewEventCommand,eventData);
        }

        private void OnServerEventReceived(string eventId, string eventArgs)
        {
            throw new NotImplementedException();
        }
        #endregion Listening to anyvision web socket
        private void RaiseAnalyticalEvent(string eventId, string json)
        {
            // Some bugs here: Not sure how to handle more than one "camera" the Analytical event can only have 1 source (so it seems)
            Item camera = null;
            switch (eventId)
            {
                case AnyVisionIntegration.SocketEvents.TrackCreated:
                    if (!currentSettings?.WebSocketSettings.TrackCreated ?? true) return;
                    camera = ExtractCamera(eventId, ()=> 
                        AnyVisionIntegration.Models.AnyVisionClassExtension.GetRoot<AnyVisionIntegration.Models.TrackCreated.Root[]>(json).First().camera.id);
                    break;
                case AnyVisionIntegration.SocketEvents.RecognitionCreated:
                    if (!currentSettings?.WebSocketSettings.RecognitionCreated ?? true) return;
                    camera = ExtractCamera(eventId, () => 
                        AnyVisionIntegration.Models.AnyVisionClassExtension.GetRoot<AnyVisionIntegration.Models.RecognitionCreated.Root>(json).camera.id);
                    break;
                case AnyVisionIntegration.SocketEvents.CameraChanged:
                    if (!currentSettings?.WebSocketSettings.CameraChanged ?? true) return;
                    camera = ExtractCamera(eventId, () => 
                        AnyVisionIntegration.Models.AnyVisionClassExtension.GetRoot<AnyVisionIntegration.Models.CameraChanged.Root>(json).sourceId);
                    break;
                case AnyVisionIntegration.SocketEvents.CameraDeleted:
                    if (!currentSettings?.WebSocketSettings.CameraDeleted ?? true) return;
                    camera = ExtractCamera(eventId, () =>
                        AnyVisionIntegration.Models.AnyVisionClassExtension.GetRoot<AnyVisionIntegration.Models.CameraDeleted.Root>(json).cameraIds.First());
                    break;
                default: throw new UnSupportedAnyVisionSocketEventException(eventId);
            }
            if (camera == null) return; // A bug, but logged in ExtractCamera
            var aEvent = MilestoneSdkExtensions.AnalyticsEventFolderExtensions.CreateAnalyticalEvent(camera, eventId, json);
            internalMessenger.Send(MessageId.Server.NewEventCommand, aEvent);
            log.LogDebug(LogLevelEnum.Debug, nameof(AnyVisionSocketEventListenerPlugin), $"{nameof(RaiseAnalyticalEvent)}:{eventId}");
            externalMessenger.Send(PluginDefinition.MessageIds.SocketEventReceived);
        }
        /// <summary>
        /// Find the camera associated with the AnyVision socket event.
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="getCameraId"></param>
        /// <returns></returns>
        private Item ExtractCamera(string eventId, Func<string> getCameraId)
        {
            try
            {
                var cameraId = getCameraId();
                foreach (var item in CameraExtensions.GetFlatCameraList())
                {
                    var associatedProperties = Configuration.Instance.GetAssociatedProperties(item, PluginDefinition.TabPluginId);
                    if (!associatedProperties.Properties.ContainsKey(TabPlugin.GuidKey)) continue;
                    var anyVisionId = associatedProperties.Properties[TabPlugin.GuidKey];
                    if (anyVisionId == null) continue;
                    if (anyVisionId.Equals(cameraId)) return item;
                }
            }
            catch (Exception ex)
            {
                log.LogException(nameof (AnyVisionSocketEventListenerPlugin),nameof(ExtractCamera), ex);
            }
            return null;
        }
        /// <summary>
        /// Creates the types not instances of analytical events. If they do not already exits.
        /// </summary>
        private static async void CreateAnalyticalEventsTypes(ILogging log)
        {
            var server = new ManagementServer(EnvironmentManager.Instance.MasterSite.ServerId);
            var srs = await server.AnalyticsEventFolder.Create(new AnyVisionIntegration.SocketEvents());
            if (srs.All(t => t.State == StateEnum.Success || t.State == StateEnum.Completed)) return;
            log.LogError(nameof(AnyVisionSocketEventListenerPlugin),nameof(CreateAnalyticalEventsTypes),"Creation of Analytical Event Types failed" );
        }
       
        public override List<EnvironmentType> TargetEnvironments => new List<EnvironmentType> { EnvironmentType.Service};
        private static bool HasSubscriptions(IAnyVisionProperties s) => s.WebSocketSettings.Any(t => t);
    }
}
