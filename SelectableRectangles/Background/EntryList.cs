﻿using System.Collections;
using System.Collections.Generic;
using MilestoneSdkExtensions.Miscellaneous;
using Newtonsoft.Json;

namespace SelectableRectangles.Background
{
    public  class EntryList : IEnumerable<Entry>
    {
        private readonly Queue<Entry> queue = new Queue<Entry>();
        public int MaxCapacity { get; set; } = 20;

        public EntryList(string jsonQueue)
        {
            if (string.IsNullOrEmpty(jsonQueue)) return;
            queue = JsonConvert.DeserializeObject<Queue<Entry>>(jsonQueue);
        }

        public void Add(Entry entry)
        {
            if (queue.Count >= MaxCapacity)
            {
                queue.Dequeue();
            }
            queue.Enqueue(entry);
        }
        public IEnumerator<Entry> GetEnumerator() => queue.GetEnumerator(); 
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public string ToJson() => JsonConvert.SerializeObject(queue);
    }
}
