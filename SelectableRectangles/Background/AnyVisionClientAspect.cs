﻿using System;
using System.Threading.Tasks;
using AnyVisionIntegration.Exceptions;
using AnyVisionIntegration.Interfaces;
using VideoOS.CustomDevelopment.PublicStuff;

namespace SelectableRectangles.Background
{
    /// <summary>
    /// This class adds error handling and logging around a <see cref="AnyVisionIntegration.Interfaces.IAnyVisionClient"/>  
    /// </summary>
    internal class AnyVisionClientAspect: IAnyVisionClient
    {
        private readonly IAnyVisionClient client;
        private readonly ILogging log;
        public AnyVisionClientAspect(IAnyVisionClient client, ILogging logging)
        {
            this.client = client;
            log = logging;
        }

        public void Dispose() => client.Dispose();

        public event Action OnDisconnectEvent;

        /// <exception cref="AnyVisionLoginException">Thrown when the login fails</exception>
        /// <exception cref="InvalidOperationException">Thrown on connecting to the socket</exception>
        /// <exception cref="ArgumentNullException">Thrown on connecting to the socket</exception>
        /// <exception cref="ObjectDisposedException">Thrown on connecting to the socket</exception>
        /// <exception cref="OperationCanceledException">Thrown on connecting to the socket</exception>
        public async Task<bool> Connect()
        {
            try
            {
                return await client.Connect();
            }
            catch (AnyVisionLoginException e)
            {
                // A number of retries might be prudent. Reporting to the Management Client
                // Also the Management Client client should be able to restart.
                // Connection state should be shown in the MC

                // fails on login. Could be wrong credentials, could be server currently unavailable
                // if the status code is 503 the service is temporarily down and retries makes sense 
                // Other 500 and might be fatal
                // The request object must be queried to know what to do 
                // A first time failure should throw an alarm
                // 
                Console.WriteLine(e);
                throw;
            }
            
        }

        public async Task Disconnect()
        {
        }

        public SocketActionsImpl SocketActions => client.SocketActions;
        //public ISocketEventHandlers WebClientEventHandlers => client.WebClientEventHandlers;
    }
}
