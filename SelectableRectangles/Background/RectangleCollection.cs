﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Controls;
using System.Windows.Shapes;
using VideoOS.Platform;
using VideoOS.Platform.Client;

namespace SelectableRectangles.Background
{
    public class RectangleCollection: IEnumerable<List<MyRectangle>>
    {
        /// <summary>
        /// There is a 1 to 1 correspondence between addon and the viewitem. 
        /// </summary>
        private readonly Dictionary<FQID, List<MyRectangle>> rectanglesDictionary = new Dictionary<FQID, List<MyRectangle>>();
        private readonly ImageViewerAddOnCollection imageViewerAddOnCollection = new ImageViewerAddOnCollection();
        public bool ContainsKey(FQID id) => rectanglesDictionary.ContainsKey(id);
        public FQID GetViewItemIdByAddOn(ImageViewerAddOn iv)=> imageViewerAddOnCollection.GetViewItemIdByAddOn(iv);
        public List<MyRectangle> GetRectangle(FQID id) => rectanglesDictionary[id];
        /// <summary>
        /// What should be searched on...
        /// </summary>
        /// <param name="addOn"></param>
        public Bitmap CurrentImage(FQID viewItemFqid)
        {
            var addOn = imageViewerAddOnCollection.GetByIdWithCheck(viewItemFqid);
            return addOn.GetCurrentDisplayedImageAsBitmap();
        }

        public bool Add(FQID viewItemFqid, List<MyRectangle> rectangles)
        {
            if (rectanglesDictionary.ContainsKey(viewItemFqid))
            {
                // Remove the ones that are already there
                rectanglesDictionary[viewItemFqid] = rectangles;
                return false;
            }
            rectanglesDictionary.Add(viewItemFqid, rectangles);
            return true;

        }

        public List<MyRectangle> this[FQID id] => rectanglesDictionary[id];

        public void AddAddOn(ImageViewerAddOn imageViewerAddOn) => imageViewerAddOnCollection.Add(imageViewerAddOn);
        public void RenderOverlay(FQID id, List<Shape> shapes)
        {
            var item = imageViewerAddOnCollection.Get(id);
            var guid = item.AddOn.ShapesOverlayAdd(shapes, new ShapesOverlayRenderParameters { ZOrder = 0 });
            item.DrawGuid = guid;
        }

        public void UpdateOverlay(FQID id, List<Shape> shapes)
        {
            var item = imageViewerAddOnCollection.Get(id);
            item.AddOn.ShapesOverlayUpdate(item.DrawGuid, shapes, new ShapesOverlayRenderParameters { ZOrder = 0 });
        }


        public IEnumerator<List<MyRectangle>> GetEnumerator() => rectanglesDictionary.Values.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public double GetScale(FQID id)
        {
            var addOn = imageViewerAddOnCollection.GetById(id);
            return (double)addOn.PaintSize.Width / addOn.ImageSize.Width;
        }

        public void ClearAll()
        {
            foreach (var entry in rectanglesDictionary)
            {
                var item = imageViewerAddOnCollection.Get(entry.Key);
                ClientControl.Instance.CallOnUiThread(
                    ()=> item.AddOn?.ShapesOverlayRemove(item.DrawGuid)
                    );
                item.DrawGuid = Guid.Empty;
            }
            rectanglesDictionary.Clear();
        }
    }
}
