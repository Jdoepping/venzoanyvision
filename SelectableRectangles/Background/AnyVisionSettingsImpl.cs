﻿using AdminUi.Interfaces;
using AnyVisionIntegration.Interfaces;

namespace SelectableRectangles.Background
{
    public class AnyVisionSettingsImpl : IAnyVisionSettings
    {
        private readonly IAnyVisionProperties item;
        public AnyVisionSettingsImpl(IAnyVisionProperties item) => this.item = item;

        public string UserName => item.UserName;
        public string Password => item.Password;
        /// <summary>
        /// the slash might or might not be present. Now it will never be
        /// </summary>
        public string BaseUrl => item.BaseUrl.TrimEnd('/');
        public bool TrackCreated => item.WebSocketSettings.TrackCreated;
        public bool RecognitionCreated => item.WebSocketSettings.TrackCreated;
        public bool CameraChanged => item.WebSocketSettings.TrackCreated;
        public bool CameraDeleted => item.WebSocketSettings.TrackCreated;
    }
}
