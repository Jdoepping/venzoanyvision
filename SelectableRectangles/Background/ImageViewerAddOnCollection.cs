﻿using System;
using System.Collections.Generic;
using System.Linq;
using VideoOS.Platform;
using VideoOS.Platform.Client;

namespace SelectableRectangles.Background
{
    /// <summary>
    /// Keeping track of ImageViewerAddOns.
    /// It is necessary to know the ImageViewerAddOn associated with a ViewItem (if any). 
    /// I cannot find an identifier for an imageViewerAddOn. The Camera FQID is not suitable as the
    /// same camera can be used on multiple ViewItems.
    /// The ImageViewerAddOn is added on the ClientControl.Instance.NewImageViewerControlEvent where
    /// I cannot find an association to a ViewItem. If I could the ViewItem id could be used.
    /// </summary>
    public class ImageViewerAddOnCollection
    {
        private readonly HashSet<ImageViewerAddOnExtension> collection;

        public ImageViewerAddOnCollection() => collection = new HashSet<ImageViewerAddOnExtension>();

        public void Add(ImageViewerAddOn addOn)
        {
            if (collection.Any(t => t.AddOn == addOn)) return;
            collection.Add(new ImageViewerAddOnExtension(addOn));
        }

        /// <summary>
        /// An AddOn has a notion of selected. This is probably the viewitem containing the ImageViewer that is
        /// selected. 
        /// </summary>
        public ImageViewerAddOn Selected => collection.FirstOrDefault(t => t.AddOn.IsSelected)?.AddOn;
        /// <summary>
        /// Use the viewItem FQID to get the addon. Will only work when the two (viewitem and addon) has
        /// previously been associated (see GetAddOnAssociateWithViewItem)
        /// </summary>
        /// <param name="fqid"></param>
        /// <returns></returns>
        public ImageViewerAddOn GetById(FQID fqid) => collection.FirstOrDefault(t => fqid.Equals(t.ViewItemFqid))?.AddOn;
        /// <summary>
        /// Associating viewitems and addons are done in an indirect fashion.
        /// It is assumed that the AddOn which "Selected" property is true, is the
        /// associated addon 
        /// </summary>
        /// <param name="viewItemFqid"></param>
        /// <returns></returns>
        public ImageViewerAddOn GetByIdWithCheck(FQID viewItemFqid)
        {
            var addOn = GetById(viewItemFqid);
            var selectedAddOn = Selected;
            if (addOn != null && addOn == selectedAddOn) return addOn;

            switch (addOn)
            {
                case null when selectedAddOn != null:
                    AssociateViewItemAndAddOn(selectedAddOn, viewItemFqid);
                    return selectedAddOn;
                case null:
                    throw new Exception("Cannot find addon");
                default:
                    throw new Exception("Mismatch between addons");
            }
        }

        public ImageViewerAddOnExtension Get(FQID fqid) => collection.First(t => fqid.Equals(t.ViewItemFqid));
        public FQID GetViewItemIdByAddOn(ImageViewerAddOn iv) => collection.First(t => t.AddOn == iv).ViewItemFqid;

        private void AssociateViewItemAndAddOn(ImageViewerAddOn addOn, FQID fqid) =>
            collection.First(t => t.AddOn == addOn).ViewItemFqid = fqid;
        public class ImageViewerAddOnExtension
        {
            public ImageViewerAddOnExtension(ImageViewerAddOn addOn)
            {
                AddOn = addOn;
                ViewItemFqid = null;
            }

            public ImageViewerAddOn AddOn { get; }
            public FQID ViewItemFqid { get; set; }
            /// <summary>
            /// The guid identifying the current drawings.
            /// </summary>
            public Guid DrawGuid { get; set; }
        }
    }
}
