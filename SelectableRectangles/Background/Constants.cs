﻿using System;
using System.Collections.Generic;
using AdminUi.Interfaces;
using SelectableRectangles.Admin.Item;
using VideoOS.Platform;
using VideoOS.Platform.Data;

namespace SelectableRectangles.Background
{
    /// <summary>
    /// Contains constants or delivers constants.
    /// </summary>
    internal static class Constants
    {
        /// <summary>
        /// The AnyVision plugin has two items.The item used to contain configuration
        /// and the item used to hold a kind of reporting
        /// </summary>
        /// <returns></returns>
        public static FQID ItemFqid()
        {
            return new FQID(
                EnvironmentManager.Instance.CurrentSite.ServerId,
                Guid.Empty,
                "anyvisionproperties",
                FolderType.No,
                PluginDefinition.Kind
            )
            {
                ObjectId = new Guid(Admin.ItemManager.ItemGuid),
            };
        }
        /// <summary>
        ///  
        /// </summary>
        /// <returns>The same anyvision configuration item</returns>
        public static IAnyVisionProperties GetAnyVisionSettings()
        {
            var item = AnyVisionItem();
            return item != null ? new AnyVisionProperties(item) : null;
        }
        private static Item AnyVisionItem() =>
            Configuration.Instance.GetItemConfiguration(PluginDefinition.PluginId,
                PluginDefinition.Kind,
                new Guid(Admin.ItemManager.ItemGuid));

        internal const string EventGroupGuid = "{72FA1204-F9EF-43B2-8F25-C5BC97C4026D}";
        public static EventGroup AnyVisionEventGroup = new EventGroup
        {
            ID = new Guid(EventGroupGuid),
            Name = "AnyVision Server Status"
        };

    public static EventType Disconnected => new EventType
        {
            GroupID = new Guid(EventGroupGuid),
            ID = new Guid("{D78B71E5-DEA2-40C0-A1BB-E1F3589D965E}"),
            DefaultSourceKind = Guid.Empty,
            Message = "Server Disconnected",
            SourceKinds = new List<Guid>
            {
                PluginDefinition.Kind
            }
        };

    public static EventType Connected => new EventType
    {
        GroupID = new Guid(EventGroupGuid),
        ID = new Guid("{E2747D05-4333-40A1-9C27-61D6E900B913}"),
        DefaultSourceKind = Guid.Empty,
        Message = "Server Connected",
        SourceKinds = new List<Guid>
        {
            PluginDefinition.Kind
        }
    };

    public static EventType Connecting => new EventType
    {
        GroupID = new Guid(EventGroupGuid),
        ID = new Guid("{45E24D1A-244A-40EE-ABD5-9379273C80F1}"),
        DefaultSourceKind = Guid.Empty,
        Message = "Server Connecting",
        SourceKinds = new List<Guid>
        {
            PluginDefinition.Kind
        }
    };
    }
}
