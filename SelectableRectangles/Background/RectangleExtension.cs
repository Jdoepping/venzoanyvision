﻿using System.Drawing;

namespace SelectableRectangles.Background
{
    public static class RectangleExtension
    {
        /// <summary>
        /// https://math.stackexchange.com/questions/190111/how-to-check-if-a-point-is-inside-a-rectangle
        /// Search for "Affine map".
        /// The above link is the general case. But there is no rotation involved, that is the axis are parallel.
        /// The rotation matrix is the unit matrix which inverse is identical, (1 0) over (0 1).
        /// The width and height will scale the axis though.
        /// </summary>
        /// <param name="r">the rectangle</param>
        /// <param name="p">is this point inside the rectangle</param>
        /// <returns></returns>
        public static bool IsPointInRectangle(this MyRectangle r, Point p)
        {
            var x = p.X - r.X;
            if (!(x >= 0 && x <= r.Width)) return false;
            var y = p.Y - r.Y;
            return y >= 0 && y <= r.Height;
        }
    }
}
