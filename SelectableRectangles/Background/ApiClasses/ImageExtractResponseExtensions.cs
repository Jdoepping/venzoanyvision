﻿using System.Collections.Generic;
using System.Linq;

namespace SelectableRectangles.Background.ApiClasses
{
    public static class ImageExtractResponseExtensions
    {
        /// <summary>
        /// Translate between an ImageExtractResponse object and a collection of items
        /// 
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public static IEnumerable<MyRectangle> ToMyRectangles(this ImageExtractResponse response)
        {
            return response.items.Select((t, index) =>
                new MyRectangle
                {
                    Id=index+1,
                    X = (int)t.position.left,
                    Y = (int)t.position.top,
                    Width = (int)(t.position.right-t.position.left),
                    Height = (int)(t.position.bottom - t.position.top),
                }
            );
        }
    }
}