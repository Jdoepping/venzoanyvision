﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelectableRectangles.Background.ApiClasses
{
    public class SidebarNull : SidebarData
    {
        public SidebarNull():base(Guid.Empty)
        {
            SubjectName = "";
            FaceImages = new Uri[0];
            BodyImages = new Uri[0];
        }
    }
}
