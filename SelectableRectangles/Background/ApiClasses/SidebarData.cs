﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace SelectableRectangles.Background.ApiClasses
{
    /// <summary>
    /// A class that holds the information used in the viewitem "sidebar" i.e. the right 
    /// part of the "viewitem".
    /// There still is notions of "Actions", "Rankings" and Attributes.
    ///
    /// 
    /// </summary>
    public class SidebarData
    {
        public SidebarData(Guid id) => Id = id;

        public string SubjectName { get; set; }
        public string SubjectGroup { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public byte[] PictureCutOut { get; set; }
        //public Uri FirstFaceImage => FaceImages.First();
        public IEnumerable<Uri> FaceImages { get; set; }
        public IEnumerable<Uri> BodyImages { get; set; }
        public IEnumerable<string> Actions { get; set; }
        public IEnumerable<string> Rankings { get; set; }
        public IEnumerable<string> Attributes { get; set; }
        public Guid Id { get; }


    }
}
