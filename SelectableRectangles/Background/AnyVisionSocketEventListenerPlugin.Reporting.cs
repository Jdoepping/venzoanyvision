﻿using System;
using MilestoneSdkExtensions.Miscellaneous;
using SelectableRectangles.Admin.Item;
using VideoOS.CustomDevelopment.PublicStuff;
using VideoOS.Platform;

namespace SelectableRectangles.Background
{
    public partial class AnyVisionSocketEventListenerPlugin
    {
        /// <summary>
        /// More or less dead code. 
        /// 
        /// The intention is to enable showing a history of how the AnyVision WebSocket is running.
        /// This class stores the 20 latest entries. The entries are written in an Item and
        /// stored in Milestone configuration. 
        /// </summary>
        public class Reporting
        {
            private EntryList entryList;
            private AnyVisionProperties item;
            private readonly ILogging log;

            public Reporting(ILogging log) => this.log = log;
            /// <summary>
            /// The idea is to save some logging in an Item for AnyVision
            /// </summary>
            public void Report(Entry entry)
            {
                try
                {
                    return;
                    entryList = entryList ?? CreateEntryList();
                    entryList.Add(entry);
                    item.Reporting = entryList.ToJson();
                    //Configuration.Instance.SaveItemConfiguration(PluginDefinition.PluginId, item.MyItem);
                }
                catch (Exception ex)
                {
                    log.LogException(ex);
                }
            }

            public EntryList GetEntries()
            {
                return entryList = entryList ?? CreateEntryList();
            }
            

            private EntryList CreateEntryList()
            {
                item = GetAnyVisionReportItem();
                var json = item.Reporting;
                return entryList = new EntryList(json);
            }

            public static AnyVisionProperties GetAnyVisionReportItem() => new AnyVisionProperties(AnyVisionReportItem() ?? Item);

            /// <summary>
            /// Get the specific item used to store AnyVision properties in.
            /// </summary>
            /// <returns>null if the item doesn't exist otherwise the item</returns>
            private static Item AnyVisionReportItem() =>
                Configuration.Instance.GetItemConfiguration(PluginDefinition.PluginId,
                    PluginDefinition.Kind,
                    new Guid(ReportItemGuid));

            /// <summary>
            /// A constant FQID
            /// </summary>
            /// <returns>The FQID for an AnyVisionReport item</returns>
            private static FQID ItemFqid()
            {
                return new FQID(
                    EnvironmentManager.Instance.CurrentSite.ServerId,
                    Guid.Empty,
                    "anyvisionReportProperties",
                    FolderType.No,
                    PluginDefinition.Kind
                )
                {
                    ObjectId = new Guid(ReportItemGuid),
                };
            }
            private static Item Item => new Item(ItemFqid(), "Reporting");
        }
    }
}