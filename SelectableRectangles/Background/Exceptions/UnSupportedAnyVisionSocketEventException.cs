﻿using System;

namespace SelectableRectangles.Background.Exceptions
{
    /// <summary>
    /// Thrown when an event id is received that is not supported.
    /// This should only happen if the AnyVisionIntegration is extended without the plug-in following along.
    /// </summary>
    internal class UnSupportedAnyVisionSocketEventException : Exception
    {
        private readonly string eventId;
        public UnSupportedAnyVisionSocketEventException(string eventId) => this.eventId = eventId;
        public override string Message => $"The event id: {eventId} is not supported";
    }
}