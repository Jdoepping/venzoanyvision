﻿AnyVision Search

Story: Observing playback, a person is identified and selected. The properties of the
person, gender, topColor, bottomColor and backpack. Should then be made available for a search.
Such a search can include cameras also as a search parameter.

Perhaps the reason for this request is searching in live based on observations in playback.

Having identified an image and extracted it, features from that image can be extracted. using (Java code)
Request request = new Request.Builder()
  .url("https://kong.tls.ai/bt/api/features/extract-from-image")
  .post(body)
  .addHeader("Accept", "application/json")
  .addHeader("Content-Type", "multipart/form-data; boundary=---011000010111000001101001")

The response is reported as being: [Number] items (doesn't make sense)






