using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using SelectableRectangles.Admin;
using VideoOS.Platform;
using VideoOS.Platform.Admin;

namespace SelectableRectangles
{
    /// <summary>
    /// The PluginDefinition is the ‘entry’ point to any plugin.  
    /// This is the starting point for any plugin development and the class MUST be available for a plugin to be loaded.  
    /// Several PluginDefinitions are allowed to be available within one DLL.
    /// Here the references to all other plugin known objects and classes are defined.
    /// The class is an abstract class where all implemented methods and properties need to be declared with override.
    /// The class is constructed when the environment is loading the DLL.
    /// </summary>
    public class PluginDefinition : VideoOS.Platform.PluginDefinition
    {
        public class MessageIds
        {
            private const string CompanyName = "Venzosecure.com";
            private const string App = "AnyVisionServer";
            private static readonly string Prefix = $"{CompanyName}.{App}";

            public static readonly string InitiateFaceSearch = $"{Prefix}.InitiateFaceSearch";
            public static readonly string ImageSelected = $"{Prefix}.ImageSelected";
            public static readonly string AddToSelectedCommand = $"{Prefix}.AddToSelectedCommand";
            public static readonly string AnyVisionWebSocketSettingsChangedIndication = $"{Prefix}.WebSocketSettingsChangedIndication";
            public static readonly string AnyVisionWebSocketConnectFailed = $"{Prefix}.AnyVisionWebSocketConnectFailed";
            public static readonly string ReconnectWebSocket = $"{Prefix}.ReconnectWebSocket";
            public static readonly string Connecting = $"{Prefix}.Connecting";
            public static readonly string Connected = $"{Prefix}.Connected";
            public static readonly string Disconnected = $"{Prefix}.Disconnecting";
            public static readonly string NoneTransientError = $"{Prefix}.NoneTransientError";
            public static readonly string TokenRenewalChanged= $"{Prefix}.TokenRenewalChanged";
            /// <summary>
            /// Reply on a state request.
            /// </summary>
            public static readonly string StateIndication = $"{Prefix}.StateIndication";
            /// <summary>
            /// Asking for the current state of the service
            /// </summary>
            public static readonly string StateRequest = $"{Prefix}.StateStatus";
            public static readonly string SocketEventReceived = $"{Prefix}.SocketEventReceived";
        }

        private static System.Drawing.Image _treeNodeImage;
        private static System.Drawing.Image _topTreeNodeImage;

        internal static Guid PluginId = new Guid("d471311b-f224-4fdd-9ff7-bc1ea88576bf");
        internal static Guid Kind = new Guid("c64fa440-29dc-4ba1-a265-053a820a2fef");
        internal static Guid SidePanelId = new Guid("47a23cd0-e36d-4603-85c3-321366f535a1");
        internal static Guid ViewItemPluginId = new Guid("ffd55ed2-2e99-4056-ac36-c8bff226cc32");
        internal static Guid SettingsPanelId = new Guid("f3f76b65-b1ce-4faa-af85-75989dde2ded");
        internal static Guid BackgroundPluginId = new Guid("14caff96-1ba7-41a9-b03d-91a6be3dcd5d");
        internal static Guid AnyVisionSocketEventListenerPluginId = new Guid("C2A3F40E-2835-40EF-815B-7AD38E3F6481");
        internal static Guid WorkSpacePluginId = new Guid("ddd7ad3b-016d-47de-991c-b3c322b799dd");
        internal static Guid WorkSpaceViewItemPluginId = new Guid("5202ab89-ec31-4312-af92-ba4487c97676");
        internal static Guid TabPluginId = new Guid("2e5c840e-3111-4ac4-b761-d6f4af4e420a");
        internal static Guid ViewLayoutId = new Guid("1bc53181-b14f-4a5f-bdab-405a8af35b41");
        internal static Guid WorkSpaceToolbarPluginId = new Guid("{24A27A8D-C868-4951-B037-5178FA1468EE}");
        internal static Guid ViewItemToolbarPluginId = new Guid("{F53A8226-AA9C-4FCE-B827-1297DCED3E68}");
        internal static Guid ToolsOptionDialogPluginId = new Guid("{E3EB515B-05FE-45EF-82ED-90E01AD1932A}");

        #region Private fields

        private System.Windows.Forms.UserControl _treeNodeInofUserControl;

        //
        // Note that all the plugin are constructed during application start, and the constructors
        // should only contain code that references their own dll, e.g. resource load.

        private readonly List<VideoOS.Platform.Background.BackgroundPlugin> _backgroundPlugins = new List<VideoOS.Platform.Background.BackgroundPlugin>();
        private readonly Collection<VideoOS.Platform.Client.SettingsPanelPlugin> _settingsPanelPlugins = new Collection<VideoOS.Platform.Client.SettingsPanelPlugin>();
        private readonly List<VideoOS.Platform.Client.ViewItemPlugin> _viewItemPlugins = new List<VideoOS.Platform.Client.ViewItemPlugin>();
        private readonly List<ItemNode> _itemNodes = new List<ItemNode>();
        private readonly List<VideoOS.Platform.Client.SidePanelPlugin> _sidePanelPlugins = new List<VideoOS.Platform.Client.SidePanelPlugin>();
        private readonly List<string> _messageIdStrings = new List<string>();
        private List<SecurityAction> _securityActions = new List<SecurityAction>();
        private readonly List<VideoOS.Platform.Client.WorkSpacePlugin> _workSpacePlugins = new List<VideoOS.Platform.Client.WorkSpacePlugin>();
        private readonly List<VideoOS.Platform.Admin.TabPlugin> _tabPlugins = new List<VideoOS.Platform.Admin.TabPlugin>();
        private readonly List<VideoOS.Platform.Client.ViewItemToolbarPlugin> _viewItemToolbarPlugins = new List<VideoOS.Platform.Client.ViewItemToolbarPlugin>();
        private readonly List<VideoOS.Platform.Client.WorkSpaceToolbarPlugin> _workSpaceToolbarPlugins = new List<VideoOS.Platform.Client.WorkSpaceToolbarPlugin>();
        private readonly List<VideoOS.Platform.Admin.ToolsOptionsDialogPlugin> _toolsOptionsDialogPlugins = new List<VideoOS.Platform.Admin.ToolsOptionsDialogPlugin>();

        #endregion

        #region Initialization

        /// <summary>
        /// Load resources 
        /// </summary>
        static PluginDefinition()
        {
            _treeNodeImage = Properties.Resources.DummyItem;
            _topTreeNodeImage = Properties.Resources.Server;
        }


        /// <summary>
        /// Get the icon for the plugin
        /// </summary>
        internal static Image TreeNodeImage => _treeNodeImage;

        #endregion

        /// <summary>
        /// This method is called when the environment is up and running.
        /// Registration of Messages via RegisterReceiver can be done at this point.
        /// </summary>
        public override void Init()
        {
            // Populate all relevant lists with your plugins etc.
            _itemNodes.Add(new ItemNode(Kind, Guid.Empty,
                                         "AnyVision Server", _treeNodeImage,
                                         "AnyVision Server", _treeNodeImage,
                                         Category.Text, true,
                                         ItemsAllowed.One,
                                         new SelectableRectangles.Admin.ItemManager(Kind),
                                         null
                                         )
            {
                PlacementHint = PlacementHint.Servers,
            });
            if (EnvironmentManager.Instance.EnvironmentType == EnvironmentType.SmartClient)
            {
                _workSpacePlugins.Add(new SelectableRectangles.Client.WorkSpacePlugin());
                _sidePanelPlugins.Add(new SelectableRectangles.Client.SidePanel.Plugin());
                _viewItemPlugins.Add(new SelectableRectangles.Client.ViewItemPlugin());
                _viewItemPlugins.Add(new SelectableRectangles.Client.WorkSpaceViewItemPlugin());
                _viewItemToolbarPlugins.Add(new SelectableRectangles.Client.ViewItemToolbarPlugin());
                //_workSpaceToolbarPlugins.Add(new SelectableRectangles.Client.WorkSpaceToolbarPlugin());
                //_settingsPanelPlugins.Add(new SelectableRectangles.Client.SettingsPanel.Plugin());
            }
            if (EnvironmentManager.Instance.EnvironmentType == EnvironmentType.Administration)
            {
                _tabPlugins.Add(new SelectableRectangles.Admin.TabPlugin());
                //_toolsOptionsDialogPlugins.Add(new SelectableRectangles.Admin.ToolsOptionDialogPlugin());
            }

            _backgroundPlugins.Add(new SelectableRectangles.Background.BackgroundPlugin());
            // Hey stuff can be injected! Well not really
            _backgroundPlugins.Add(new SelectableRectangles.Background.AnyVisionSocketEventListenerPlugin());
        }

        /// <summary>
        /// The main application is about to be in an undetermined state, either logging off or exiting.
        /// You can release resources at this point, it should match what you acquired during Init, so additional call to Init() will work.
        /// </summary>
        public override void Close()
        {
            _itemNodes.Clear();
            _sidePanelPlugins.Clear();
            _viewItemPlugins.Clear();
            _settingsPanelPlugins.Clear();
            _backgroundPlugins.Clear();
            _workSpacePlugins.Clear();
            _tabPlugins.Clear();
            _viewItemToolbarPlugins.Clear();
            _workSpaceToolbarPlugins.Clear();
            _toolsOptionsDialogPlugins.Clear();
        }

        /// <summary>
        /// Return any new messages that this plugin can use in SendMessage or PostMessage,
        /// or has a Receiver set up to listen for.
        /// The suggested format is: "YourCompany.Area.MessageId"
        /// </summary>
        public override List<string> PluginDefinedMessageIds => _messageIdStrings;

        /// <summary>
		/// If authorization is to be used, add the SecurityActions the entire plugin 
		/// would like to be available.  E.g. Application level authorization.
		/// </summary>
		public override List<SecurityAction> SecurityActions
        {
            get => _securityActions;
            set
            {
            }
        }

        #region Identification Properties

        /// <summary>
        /// Gets the unique id identifying this plugin component
        /// </summary>
        public override Guid Id => PluginId;

        /// <summary>
		/// This Guid can be defined on several different IPluginDefinitions with the same value,
		/// and will result in a combination of this top level ProductNode for several plugins.
		/// Set to Guid.Empty if no sharing is enabled.
		/// </summary>
		public override Guid SharedNodeId => Guid.Empty;

        /// <summary>
		/// Define name of top level Tree node - e.g. A product name
		/// </summary>
		public override string Name => "AnyVision Server Plug-In";
        public override string Manufacturer => "Venzo Secure";
        public override string VersionString => "1.0.0.0";
        /// <summary>
		/// Icon to be used on top level - e.g. a product or company logo
		/// </summary>
		public override System.Drawing.Image Icon => _topTreeNodeImage;
        #endregion Identification Properties

        #region Administration properties

        /// <summary>
        /// A list of server side configuration items in the administrator
        /// </summary>
        public override List<ItemNode> ItemNodes => _itemNodes;

        /// <summary>
        /// An extension plug-in running in the Administrator to add a tab for built-in devices and hardware.
        /// </summary>
        public override ICollection<VideoOS.Platform.Admin.TabPlugin> TabPlugins => _tabPlugins;

        /// <summary>
        /// An extension plug-in running in the Administrator to add more tabs to the Tools-Options dialog.
        /// </summary>
        public override List<ToolsOptionsDialogPlugin> ToolsOptionsDialogPlugins => _toolsOptionsDialogPlugins;

        /// <summary>
        /// A user control to display when the administrator clicks on the top TreeNode
        /// </summary>
        public override System.Windows.Forms.UserControl GenerateUserControl() =>
            _treeNodeInofUserControl = new HelpPage();

        /// <summary>
        /// This property can be set to true, to be able to display your own help UserControl on the entire panel.
        /// When this is false - a standard top and left side is added by the system.
        /// </summary>
        public override bool UserControlFillEntirePanel => false;

        #endregion

        #region Client related methods and properties
        /// <summary>
        /// A list of Client side definitions for Smart Client
        /// </summary>
        public override List<VideoOS.Platform.Client.ViewItemPlugin> ViewItemPlugins => _viewItemPlugins;
        public override Collection<VideoOS.Platform.Client.SettingsPanelPlugin> SettingsPanelPlugins => _settingsPanelPlugins;
        public override List<VideoOS.Platform.Client.SidePanelPlugin> SidePanelPlugins => _sidePanelPlugins;
        public override List<VideoOS.Platform.Client.WorkSpacePlugin> WorkSpacePlugins => _workSpacePlugins;
        public override List<VideoOS.Platform.Client.ViewItemToolbarPlugin> ViewItemToolbarPlugins => _viewItemToolbarPlugins;
        public override List<VideoOS.Platform.Client.WorkSpaceToolbarPlugin> WorkSpaceToolbarPlugins => _workSpaceToolbarPlugins;
        public override List<VideoOS.Platform.Client.ViewLayout> ViewLayouts { get; } = new List<VideoOS.Platform.Client.ViewLayout> { new SelectableRectangles.Client.ViewLayout() };

        #endregion
        /// <summary>
        /// Create and returns the background task.
        /// </summary>
        public override List<VideoOS.Platform.Background.BackgroundPlugin> BackgroundPlugins => _backgroundPlugins;
    }
}
