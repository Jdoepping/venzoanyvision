﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace SelectableRectangles.Custom
{
    /// <summary>
    /// Interaction logic for PropertiesImageFragment.xaml
    /// </summary>
    public partial class PropertiesImageFragment //: UserControl
    {
        public PropertiesImageFragment()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;
        }
        public static readonly DependencyProperty SubjectNameProperty =
            DependencyProperty.Register("SubjectName", typeof(string),
                typeof(PropertiesImageFragment), new PropertyMetadata(string.Empty));
        public string SubjectName
        {
            set => SetValue(SubjectNameProperty, value);
            get => (string)GetValue(SubjectNameProperty);
        }

        public static readonly DependencyProperty SubjectGroupProperty =
            DependencyProperty.Register("SubjectGroup", typeof(string),
                typeof(PropertiesImageFragment), new PropertyMetadata(string.Empty));
        public string SubjectGroup
        {
            set => SetValue(SubjectGroupProperty, value);
            get => (string)GetValue(SubjectGroupProperty);
        }

        public static readonly DependencyProperty SubjectDescriptionProperty =
            DependencyProperty.Register("Description", typeof(string),
                typeof(PropertiesImageFragment), new PropertyMetadata(string.Empty));
        public string Description
        {
            set => SetValue(SubjectDescriptionProperty, value);
            get => (string)GetValue(SubjectDescriptionProperty);
        }

        public static readonly DependencyProperty CreatedDateProperty =
            DependencyProperty.Register("CreatedDate", typeof(DateTime),
                typeof(PropertiesImageFragment), new PropertyMetadata(null));
        public DateTime CreatedDate
        {
            set => SetValue(CreatedDateProperty, value);
            get => (DateTime)GetValue(CreatedDateProperty);
        }

        public static readonly DependencyProperty FaceImageProperty =
            DependencyProperty.Register("FaceImages", typeof(IEnumerable<Uri>),
                typeof(PropertiesImageFragment), new PropertyMetadata(new Uri[0]));
        public IEnumerable<Uri> FaceImages
        {
            set => SetValue(FaceImageProperty, value);
            get => (IEnumerable<Uri>)GetValue(FaceImageProperty);
        }

        public static readonly DependencyProperty BodyImageProperty =
            DependencyProperty.Register("BodyImages", typeof(IEnumerable<Uri>),
                typeof(PropertiesImageFragment), new PropertyMetadata(new Uri[0]));
        public IEnumerable<Uri> BodyImages
        {
            set => SetValue(BodyImageProperty, value);
            get => (IEnumerable<Uri>)GetValue(BodyImageProperty);
        }
        public static readonly DependencyProperty ActionsProperty =
            DependencyProperty.Register("Actions", typeof(IEnumerable<string>),
                typeof(PropertiesImageFragment), new PropertyMetadata(new string[0]));
        public IEnumerable<Uri> Actions
        {
            set => SetValue(ActionsProperty, value);
            get => (IEnumerable<Uri>)GetValue(ActionsProperty);
        }
        
        public static readonly DependencyProperty RankingsProperty =
            DependencyProperty.Register("Rankings", typeof(IEnumerable<string>),
                typeof(PropertiesImageFragment), new PropertyMetadata(new string[0]));
        public IEnumerable<Uri> Rankings
        {
            set => SetValue(RankingsProperty, value);
            get => (IEnumerable<Uri>)GetValue(RankingsProperty);
        }
        public static readonly DependencyProperty AttributesProperty =
            DependencyProperty.Register("Attributes", typeof(IEnumerable<string>),
                typeof(PropertiesImageFragment), new PropertyMetadata(new string[0]));
        public IEnumerable<Uri> Attributes
        {
            set => SetValue(AttributesProperty, value);
            get => (IEnumerable<Uri>)GetValue(AttributesProperty);
        }
        public static readonly DependencyProperty PictureCutOutProperty =
            DependencyProperty.Register("PictureCutOut", typeof(byte[]),
                typeof(PropertiesImageFragment), new PropertyMetadata(null));
        public byte[] PictureCutOut
        {
            set => SetValue(PictureCutOutProperty, value);
            get => (byte[])GetValue(PictureCutOutProperty);
        }

       
    }

   

    
}
