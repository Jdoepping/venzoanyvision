﻿using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Media.Imaging;

namespace SelectableRectangles.Custom
{
    public class SubjectProperties:INotifyPropertyChanged
    {
        private string subjectName = "Unknown";
        private string subjectGroup = "N/A";
        private BitmapImage mainImage;
        private string description = "N/A";

        public string SubjectName
        {
            get => subjectName;
            set
            {
                subjectName = value;
                OnPropertyChanged(nameof(SubjectName));
            }
        }

        public string SubjectGroup
        {
            get => subjectGroup;
            set
            {
                subjectGroup = value;
                OnPropertyChanged(nameof(SubjectGroup));
            }
        }

        public BitmapImage MainImage
        {
            get => mainImage;
            set
            {
                mainImage = value;
                OnPropertyChanged(nameof(MainImage));
            }
        }

        public string Description
        {
            get => description;
            set
            {
                description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)=>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        
    }
}
