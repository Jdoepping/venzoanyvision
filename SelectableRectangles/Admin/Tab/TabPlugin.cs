using System;
using System.Drawing;
using SelectableRectangles.Admin.Item;
using VideoOS.Platform;
using VideoOS.Platform.Admin;

namespace SelectableRectangles.Admin
{
    public class TabPlugin : VideoOS.Platform.Admin.TabPlugin
    {
        public const string GuidKey = "GuidKey_AnyVision";
        /// <summary>
        /// This plugin tab is visible when a camera is selected. Change to any other device type guid.
        /// </summary>
        public override Guid AssociatedKind => Kind.Camera;
        public override Guid Id => PluginDefinition.TabPluginId;
        public override Image Icon => PluginDefinition.TreeNodeImage;
        public override string Name => "AnyVision Server";
        /// <summary>
        /// This method is called when the user has logged in and configuration is accessible.<br/>
        /// </summary>
        public override void Init()
        {
        }
        public override void Close()
        {
        }
        public override VideoOS.Platform.Admin.TabUserControl GenerateUserControl(VideoOS.Platform.Item item) => new TabHostingUserControl(item);

        /// <summary>
        /// Check to see if this plugin tab should be visible.
        /// </summary>
        /// <param name="associatedItem">The currently selected device</param>
        /// <returns></returns>
        public override bool IsVisible(VideoOS.Platform.Item associatedItem) => true;
       
    }
}
