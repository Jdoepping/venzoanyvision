﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using AdminUi.Interfaces;
using VideoOS.Platform.Admin;

namespace SelectableRectangles.Admin.Tab
{
    internal class Settings : ITabSettings, INotifyPropertyChanged
    {
        private IEnumerable<ITabItem> cameras =new ITabItem[0];
        public Settings(AssociatedProperties ap, Action valuesChanged) => Camera =new TabItem(ap,OnPropertyChanged, valuesChanged);
        public ITabItem Camera { get; set; }
        public IEnumerable<ITabItem> Cameras
        {
            get => cameras;
            set
            {
                cameras = value;
                OnPropertyChanged(nameof(Cameras));
            }
        }
        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion INotifyPropertyChanged implementation
    }
}
