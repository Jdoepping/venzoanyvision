﻿using AdminUi.Interfaces;

namespace SelectableRectangles.Admin.Tab
{
    internal class CameraRootWrapper:ITabItem
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Guid { get; set; }
    }
}
