﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdminUi.Interfaces;
using AnyVisionIntegration.WebClient;
using VideoOS.Platform.ConfigurationItems;

namespace SelectableRectangles.Admin.Tab
{
    public class TabFunctionality: AdminUi.Interfaces.ITabFunctionality
    {
        public async Task<IEnumerable<ITabItem>> GetAnyVisionCameras()
        {
            var s=SelectableRectangles.Background.Constants.GetAnyVisionSettings();
            if (string.IsNullOrEmpty(s.BaseUrl)) return new ITabItem[0];
            var client = new AnyVisionHttpClient(s.BaseUrl);
            await client.AddBearerToken(s.UserName, s.Password);
            var response = await client.Cameras();
            if (response.IsSuccessStatusCode)
            {
               var cams= await client.ExtractResponse<AnyVisionIntegration.Models.Cameras.Root>(response);
               return cams.items.Select(t => new CameraRootWrapper
               {
                   Name = t.title,
                   Description = t.description,
                   Guid = t.id
               });
            }
            return new ITabItem[0];
        }
    }
}


