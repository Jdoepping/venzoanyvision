﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using SelectableRectangles.Admin.Tab;
using VideoOS.Platform;
using VideoOS.Platform.Admin;

namespace SelectableRectangles.Admin.Item
{
    /// <summary>
    /// Observe that the UserControl is of type Forms.UserControl
    /// </summary>
    public partial class TabHostingUserControl : VideoOS.Platform.Admin.TabUserControl
    {
        private readonly VideoOS.Platform.Item associatedItem;
        private bool ignoreChanged = false;
        private AssociatedProperties associatedProperties;
        /// <summary>
        /// The control that is displayed
        /// </summary>
        private readonly AdminUi.TabUserControl userControl;
        public TabHostingUserControl(VideoOS.Platform.Item item)
        {
            InitializeComponent();
            var host = new ElementHost
            {
                //BackColor = Color.BlueViolet,
                Dock = DockStyle.Fill,
            };
            // Create the WPF UserControl.
            userControl = new AdminUi.TabUserControl();
            host.Child = userControl;
            Controls.Add(host);
            associatedItem = item;
        }

        public override void Init()
        {
            base.Init();
            ignoreChanged = true;
            associatedProperties = Configuration.Instance.GetAssociatedProperties(associatedItem, PluginDefinition.TabPluginId);
            userControl.DataContext = new Settings(associatedProperties, FireConfigurationChanged);
            userControl.TabFunctionality = new TabFunctionality();
            ignoreChanged = false;
        }

        public override void Close()
        {
            //textBox1.Text = "";
            base.Close();
        }

        private void OnTextChanged(object sender, EventArgs e)
        {
            if (ignoreChanged) return;

            //associatedProperties.Properties[TabPlugin.GuidKeyProperty] = textBox1.Text;
            FireConfigurationChanged();

        }
        public override bool ValidateAndSave()
        {
            Configuration.Instance.SaveAssociatedProperties(associatedProperties);
            return true;
        }


    }
}
