﻿using System;
using AdminUi.Interfaces;
using VideoOS.Platform.Admin;

namespace SelectableRectangles.Admin.Tab
{
    public class TabItem : ITabItem
    {
        private const string NameKey = "NameKey_AnyVision";
        private const string DescriptionKey = "DescriptionKey_AnyVision";
        private const string GuidKey = "GuidKey_AnyVision";
        private readonly AssociatedProperties ap;
        private readonly Action<string> onPropertyChanged;
        private readonly Action valuesChanged;
        public TabItem(AssociatedProperties ap, Action<string> onPropertyChanged, Action valuesChanged)
        {
            this.ap = ap;
            this.onPropertyChanged = onPropertyChanged;
            this.valuesChanged = valuesChanged;
        }

        public ITabItem Camera { get; set; }

        public string Name
        {
            get => Get(NameKey, "Not Associated");
            set => Set(NameKey, value);
        }
        public string Description
        {
            get => Get(DescriptionKey, "NA");
            set => Set(DescriptionKey, value);
        }
        public string Guid
        {
            get => Get(GuidKey, "NA");
            set => Set(GuidKey, value);
        }

        private string Get(string key, string defaultValue)
        {
            if (!ap.Properties.ContainsKey(key))
            {
                ap.Properties.Add(key, defaultValue);
            }
            return ap.Properties[key];
        }

        private void Set(string key, string value)
        {
            if (!ap.Properties.ContainsKey(key))
            {
                ap.Properties.Add(key, "");
            }
            if (ap.Properties[key] == value) return;
            ap.Properties[key] = value;
            onPropertyChanged("Camera");
            valuesChanged();
        }
    }
}
