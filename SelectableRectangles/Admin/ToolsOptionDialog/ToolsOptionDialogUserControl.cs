using VideoOS.Platform.Admin;

namespace SelectableRectangles.Admin
{
    public partial class ToolsOptionDialogUserControl : VideoOS.Platform.Admin.ToolsOptionsDialogUserControl
    {
        public ToolsOptionDialogUserControl()
        {
            InitializeComponent();
        }

        public override void Init()
        {
        }

        public override void Close()
        {
        }

        public string MyPropValue
        {
            set { textBoxPropValue.Text = value ?? ""; }
            get { return textBoxPropValue.Text; }
        }
    }
}
