﻿using System;
using System.Windows.Navigation;
using AdminUi.Interfaces;

namespace SelectableRectangles.Admin
{
    public interface IUserControlItem
    {
        event EventHandler ConfigurationChangedByUser;
        void ClearContent();
        void FillContent(VideoOS.Platform.Item item);
        void UpdateItem(VideoOS.Platform.Item item);
        string DisplayName { get; set; }
        object DataContext { get; set; }
        IFunctionality Functionality{ get; set; }
        /// <summary>
        /// ConfigurationChangedByUser events tells the Milestone framework that a change exists.
        /// These calls should be suspended until values have loaded.
        /// </summary>
        bool SuspendConfigurationChanged { get; set; }
        bool HasUnsavedChanges { get; set; }
    }
        
        
}
