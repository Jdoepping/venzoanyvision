﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using AdminUi;
using AdminUi.Interfaces;

namespace SelectableRectangles.Admin.Item
{
    /// <summary>
    /// Observe that the UserControl is of type Forms.UserControl
    /// </summary>
    public partial class HostingUserControl : UserControl, IUserControlItem
    {
        /// <summary>
        /// The control that is displayed
        /// </summary>
        private readonly PropertiesAnyVision userControl;
        public HostingUserControl()
        {
            InitializeComponent();
            var host = new ElementHost
            {
                //BackColor = Color.BlueViolet,
                Dock = DockStyle.Fill,
            };
            // Create the WPF UserControl.
            // Binding is set in FillContent (Binds to Properties)
            userControl = new PropertiesAnyVision();
            // Child property.
            host.Child = userControl;
            userControl.TextChangedEvent += UserControl_TextChangedEvent;
            //Add the ElementHost control to the form's collection of child controls.
            Controls.Add(host);
        }
        private void UserControl_TextChangedEvent() => RaiseConfigurationChangedByUser(null, true);

        #region IUserControlItem
        public event EventHandler ConfigurationChangedByUser;
        public void ClearContent()
        {
        }
        /// <summary>
        /// Perhaps obsolete. Data binding does the filling.
        /// </summary>
        /// <param name="item"></param>
        public void FillContent(VideoOS.Platform.Item item)
        {
        }

        /// <summary>
        /// ConfigurationChangedByUser events tells the Milestone framework that a change exists.
        /// These calls should be suspended until values have loaded.
        /// </summary>
        public bool SuspendConfigurationChanged { get; set; } = true;

        public object DataContext
        {
            get => userControl.DataContext;
            set => userControl.DataContext = value;
        }

        public IFunctionality Functionality
        {
            get => userControl.Functionality;
            set => userControl.Functionality = value;
        }

        public bool HasUnsavedChanges
        {
            get => Functionality?.HasUnsavedChanges ?? false;
            set
            {
                if (Functionality == null) return;
                Functionality.HasUnsavedChanges = value;
            }
        }

        /// <summary>
        /// Called after a save.
        /// </summary>
        /// <param name="item"></param>
        public void UpdateItem(VideoOS.Platform.Item item)
        {
        }

        public string DisplayName { get; set; }
        #endregion IUserControlItem

        private void RaiseConfigurationChangedByUser(object source, bool e)
        {
            if (SuspendConfigurationChanged) return;
            Functionality.HasUnsavedChanges = true;
            ConfigurationChangedByUser?.Invoke(this, new EventArgs());
        }
    }
}
