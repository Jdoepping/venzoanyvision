﻿using System;
using System.Collections;
using System.Collections.Generic;
using AdminUi.Interfaces;

namespace SelectableRectangles.Admin.Item
{
    public class WebsocketSettings: IWebSockets
    {
        private uint storage;
        private readonly Action<string> onPropertyChanged;
        private readonly Action<uint> updateSetting;
        public event Action<IWebSockets> OnStatusChange; 
        public WebsocketSettings(uint storage, Action<uint> updateSetting, Action<string> onPropertyChanged)
        {
            this.storage = storage;
            this.updateSetting = updateSetting;
            this.onPropertyChanged = onPropertyChanged;
        }

        public bool TrackCreated { get=>G(1); set=>S(1,value,nameof(TrackCreated)); }
        public bool RecognitionCreated { get => G(2); set => S(2, value,nameof(RecognitionCreated)); }
        public bool CameraChanged { get => G(4); set => S(4, value, nameof(CameraChanged)); }
        public bool CameraDeleted { get => G(8); set => S(8, value, nameof(CameraDeleted)); }

        private bool G(uint index)=> (storage & index) > 0;

        private void S(uint index, bool value, string name)
        {
            if (G(index) == value) return;
            storage = value ? storage | index : storage & ~index;
            onPropertyChanged(name);
            updateSetting(storage);
            OnStatusChange?.Invoke(this);
        }

        public IEnumerator<bool> GetEnumerator()
        {
            yield return TrackCreated;
            yield return RecognitionCreated;
            yield return CameraChanged;
            yield return CameraDeleted;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
