﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AdminUi.Interfaces;
using AnyVisionIntegration.WebClient;

namespace SelectableRectangles.Admin.Item
{
    public class Functionality: IFunctionality
    {
        private readonly Action connectAction;
        private readonly ItemManager itemManager;
        private bool IsTestConnectionInProgress;

        /// <summary>
        /// The properties 
        /// </summary>
        /// <param name="connectAction"></param>
        public Functionality(ItemManager itemManager, Action connectAction)
        {
            this.itemManager = itemManager;
            this.connectAction = connectAction;
        }


        public IAnyVisionProperties Properties => itemManager.DataContext;
        public bool HasUnsavedChanges { get; set; }
        /// <summary>
        /// Enables saving in the test environment.
        /// Probably better to inject the ItemManager.
        /// </summary>
        public void Save() => itemManager.ValidateAndSaveUserControl();

        public async Task<bool> TestConnection(Action<string> showMessage)
        {
            // The source of the properties should be the current values as they are displayed 
            // in the UI.
            if (!IsValid(Properties)) return ShowErrorInProperties(showMessage, Properties);
            var client = new AnyVisionHttpClient(Properties.BaseUrl);
            HttpResponseMessage result;
            try
            {
                if (IsTestConnectionInProgress) return false;
                IsTestConnectionInProgress = true;
                result = await client.Connect(Properties.UserName, Properties.Password);
                if (result.IsSuccessStatusCode)
                {
                    showMessage("Success");
                    return true;
                }

                return await ShowHttpError(showMessage, result);
            }
            catch (Exception e)
            {
                showMessage(e.Message);
            }
            finally
            {
                IsTestConnectionInProgress = false;
            }
            return false;
        }

        public async Task<bool> ConnectToAnyVisionSocketServer(Action<string> showMessage)
        {
            // Todo: Disable the button, to avoid repeated clicks
            if (HasUnsavedChanges)
            {
                showMessage("Settings have been changed. Please save before connecting");
                return true;
            }

            Properties.Status = "Unknown";
            Properties.NoneTransientError = string.Empty;
            //if (!IsValid(properties)) return ShowErrorInProperties(showMessage, properties);
            connectAction();
            return false;
        }

        private bool IsValid(IAnyVisionProperties properties) =>
            (new[] {properties.UserName, properties.Password, properties.BaseUrl}).All(t => !string.IsNullOrEmpty(t));

        private static bool ShowErrorInProperties(Action<string> showMessage, IAnyVisionProperties p)
        {
            Func<string,bool> err = string.IsNullOrWhiteSpace;
            if (err(p.UserName)) showMessage("Username is missing");
            else if (err(p.Password)) showMessage("Password is missing");
            else if  (err(p.BaseUrl))showMessage("Baser Url is missing");
            return false;
        }
        private async Task<bool> ShowHttpError(Action<string> showMessage, HttpResponseMessage response)
        {
            var content = await response.Content.ReadAsStringAsync();
            showMessage(content);
            return false;
        }
    }
}
