namespace SelectableRectangles.Admin
{
    /// <summary>
    /// This control should contain the fields required for generating a valid Item.
    /// The template only has a Name field, but other fields should be added as required.
    /// 
    /// This dialog will be opened in a new form when user is selecting "New... " on the context menu in the administrator.
    /// When user presses "OK" the ItemManager.ValidateAddUserControl is called for validation, and if correct,
    /// the ItemManager.CreateItem is called with this class as the 3rd parameter.
    /// </summary>
    public partial class AddUserControl : System.Windows.Forms.UserControl
    {
        public AddUserControl() => InitializeComponent();

        /// <summary>
        /// The name entered by the user
        /// </summary>
        public string ItemName
        {
            set => textBoxName.Text = value;
            get => textBoxName.Text;
        }
    }
}
