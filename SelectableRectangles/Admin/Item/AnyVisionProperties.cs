﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using AdminUi.Interfaces;

namespace SelectableRectangles.Admin.Item
{
    /// <summary>
    /// Wraps the Item instance that contains the settings for an AnyVision. 
    /// </summary>
    public class AnyVisionProperties : IAnyVisionProperties, INotifyPropertyChanged
    {
        private const string UserNameKey = "UserName_AnyVision";
        private const string PasswordKey = "Password_AnyVision";
        private const string DomainKey = "Domain_AnyVision";
        private const string BaseUrlKey = "BaseUrl_AnyVision";
        private const string UserNameDefault = "Username";
        private const string PasswordDefault = "Password";
        private const string DomainDefault = "Domain";
        private const string BaseUrlDefault = "kong.tls.ai";
        private string LastConnectDefault = DateTime.MaxValue.ToString(CultureInfo.InvariantCulture);
        private const string WebSocketKey = "WebSocket_AnyVision";
        private const string LastConnectTimeKey = "LastConnectTime_AnyVision";
        private const string LastDisconnectTimeKey = "LastDisconnectTime_AnyVision";
        private const string TokenAgeKey = "TokenAge_AnyVision";
        private const string LastErrorMessageKey = "LastErrorMessage_AnyVision";
        private const string LastErrorMessageTimeKey = "LastErrorMessageTime_AnyVision";
        private const string TokenRenewalTimeKey = "TokenRenewasTimeKey_AnyVision";
        private const string TokenRenewalTimeDefault = "600"; // 600 minutes, 10 hours 
        private string status="Unknown";

        /// <summary>
        /// Used to dump log messages into item. 
        /// </summary>
        private const string ReportKey = "ReportKey_Anyvision";

        private  readonly VideoOS.Platform.Item item;
        private int eventCount;
        private string noneTransientError;
        private DateTime eventCountStartedTime=DateTime.MinValue;
        public event Action ConfigurationChangedByUser;
        public AnyVisionProperties(VideoOS.Platform.Item item)
        {
            this.item = item;
            var webSocket = Get(WebSocketKey, "0");
            WebSocketSettings = new WebsocketSettings(uint.Parse(webSocket), SetSocketSettings,OnPropertyChanged);
            //WebSocketSettings.OnStatusChange += WebSocketSettings_OnStatusChange;
        }

        public SolidColorBrush ConnectedStateForeground =>
            Status=="Connected" ? new SolidColorBrush(System.Windows.Media.Colors.Green) :
                new SolidColorBrush(System.Windows.Media.Colors.Red);

        public VideoOS.Platform.Item MyItem => item;

        public string TokenRenewalTime
        {
            get => Get(TokenRenewalTimeKey, TokenRenewalTimeDefault);
            set
            {
                if (int.TryParse(value, out int val))
                {
                    val = Math.Max(2, val);
                    IsTokenRenewalTimeChanged = true;
                    Set(TokenRenewalTimeKey, val.ToString());
                    
                }
            }
        }

        public bool IsTokenRenewalTimeChanged { get; set; }

        public string UserName
        {
            get => Get(UserNameKey, UserNameDefault);
            set => Set(UserNameKey, value);
        }
        public string Password
        {
            get => Get(PasswordKey, PasswordDefault);
            set => Set(PasswordKey, value); 
        }
        public string Domain
        {
            get => Get(DomainKey, DomainDefault);
            set => Set(DomainKey, value); 
        }
        public string BaseUrl
        {
            get => Get(BaseUrlKey, BaseUrlDefault);
            set => Set(BaseUrlKey, value);
        }

        public string Reporting
        {
            get => Get(ReportKey, "");
            set => Set(ReportKey, value);
        }

        public string LastErrorMessage
        {
            get => Get(LastErrorMessageKey, "");
            set => Set(LastErrorMessageKey, value);
        }
        public DateTime LastErrorMessageTime
        {
            get => DateTime.Parse(Get(LastErrorMessageTimeKey, DateTime.MaxValue.ToString(CultureInfo.InvariantCulture)));
            set => Set(LastErrorMessageTimeKey, value.ToString(CultureInfo.InvariantCulture));
        }
        public string Status
        {
            get => status;
            set
            {
                status = value;
                OnPropertyChanged(nameof(Status));
                OnPropertyChanged(nameof(ConnectedStateForeground));
            }
        }

        public DateTime LastConnectTime
        {
            get { 
                 var value = Get(LastConnectTimeKey, LastConnectDefault);
                 return DateTime.Parse(value);
            }
            set => Set(LastConnectTimeKey, value.ToString(CultureInfo.InvariantCulture));
        }

        public int EventCount
        {
            get
            {
                if ((DateTime.Now - eventCountStartedTime) <= TimeSpan.FromHours(1)) return eventCount;
                eventCount = 0;
                eventCountStartedTime = DateTime.Now;
                OnPropertyChanged(nameof(EventCountStarted));
                return eventCount;
            }
            set
            {
                eventCount=value;
                OnPropertyChanged(nameof(EventCount));
                OnPropertyChanged(nameof(LastEventReceived));
            }
        }

        public string EventCountStarted => eventCountStartedTime.ToLongTimeString();
        public string LastEventReceived => DateTime.Now.ToLongTimeString();

        public IWebSockets WebSocketSettings { get; set; }

        public string NoneTransientError
        {
            get => noneTransientError;
            set
            {
                noneTransientError = value; 
                OnPropertyChanged(nameof(NoneTransientError));
            }
        }

        private string Get(string key, string defaultValue)
        {
            if (!item.Properties.ContainsKey(key))
            {
                item.Properties.Add(key, defaultValue);
            }
            return item.Properties[key];
        }

        private void Set(string key, string value)
        {
            if (item.Properties[key] == value) return;
            item.Properties[key] = value;
            OnPropertyChanged("Domain"); 
            ConfigurationChangedByUser?.Invoke();
        }

        private void WebSocketSettings_OnStatusChange(IWebSockets obj)
        {
            throw new NotImplementedException();
        }

       

        private void SetSocketSettings(uint value)
        {
            item.Properties[WebSocketKey] = value.ToString();
            ConfigurationChangedByUser?.Invoke();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

