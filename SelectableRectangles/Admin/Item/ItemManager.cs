using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Threading.Tasks;
using AdminUi.Interfaces;
using Extensions.Messaging;
using VideoOS.Platform;
using VideoOS.Platform.Admin;
using VideoOS.Platform.Data;
using SelectableRectangles.Admin.Item;
using SelectableRectangles.Background;

namespace SelectableRectangles.Admin
{
    /// <summary>
    /// A single Item is allowed.  
    /// </summary>
    public class ItemManager : VideoOS.Platform.Admin.ItemManager
    {
        /// <summary>
        /// The guid for the single AnyVision item
        /// </summary>
        internal const string ItemGuid = "{16282B85-5568-4E64-978A-B4B7F6141C39}";

        private event Action UiLoadAssumedCompleteEvent; 

        private SimpleMessaging externalMessenger;
        private readonly Guid pluginId = PluginDefinition.PluginId;
        private IUserControlItem userControl;
        private readonly Guid kind;
        private bool HasRegistered;

        #region Constructors
        public ItemManager(Guid kind)  => this.kind = kind;
        public override void Init()
        {
            if (EnvironmentManager.Instance.EnvironmentType != EnvironmentType.Administration) return;
            if (HasRegistered) return;
            HasRegistered = true;
            userControl = new NullUserControlItem();
            // If the DataContext is not set, the Register calls will fail.
            DataContext = new AnyVisionProperties(new VideoOS.Platform.Item());
            externalMessenger = new SimpleMessaging(MessagingScope.External);
            externalMessenger.Register(()=>DataContext.Status="Disconnected", PluginDefinition.MessageIds.Disconnected);
            externalMessenger.Register(() => DataContext.Status = "Connecting...", PluginDefinition.MessageIds.Connecting);
            externalMessenger.Register(() => DataContext.Status = "Connected", PluginDefinition.MessageIds.Connected);
            externalMessenger.Register<string>(StateIndicationMessage, PluginDefinition.MessageIds.StateIndication);
            externalMessenger.Register(()=> DataContext.EventCount++, PluginDefinition.MessageIds.SocketEventReceived);
            externalMessenger.Register<string>((m) => DataContext.NoneTransientError = m, PluginDefinition.MessageIds.NoneTransientError);
        }

        private void StateIndicationMessage(string status) => DataContext.Status = status;

        public override void Close() => externalMessenger.Close();
        
        #endregion
        #region UserControl Methods
        public override System.Windows.Forms.UserControl GenerateDetailUserControl()
        {
            if (!(userControl is NullUserControlItem)) return (HostingUserControl)userControl;
            userControl = new HostingUserControl();
            userControl.ConfigurationChangedByUser += ConfigurationChangedByUserHandler;
            return (HostingUserControl)userControl;
        }

        /// <summary>
        /// The UserControl is no longer being used, and related resources can be released.
        /// </summary>
        public override void ReleaseUserControl()
        {
            if (userControl == null) return;
            userControl.ConfigurationChangedByUser -= ConfigurationChangedByUserHandler;
            userControl = null;
        }

        /// <summary>
        /// Clear all user entries on the UserControl, all visible fields should be blank or default values.
        /// </summary>
        public override void ClearUserControl()
        {
            CurrentItem = null;
            userControl?.ClearContent();
        }

        /// <summary>
        /// Fill the UserControl with the content of the Item or the data it represent.
        /// </summary>
        /// <param name="item">The Item to work with</param>
        public override void FillUserControl(VideoOS.Platform.Item item)
        {
            CurrentItem = FirstItem(item);
            // FillContent is getting obsolete because bindings are used instead. 
            DataContext = new AnyVisionProperties(CurrentItem);
            userControl.DataContext = DataContext;
            userControl.Functionality = new Functionality(this,()=> 
                externalMessenger.Send(PluginDefinition.MessageIds.ReconnectWebSocket));
            UiLoadAssumedCompleteEvent += ItemManager_UiLoadAssumedCompleteEvent;
            UiLoadAssumedCompleteEvent?.Invoke();
        }

        /// <summary>
        /// On binding the values in the UI, changed events are send.
        /// This happens after the method <see cref="FillUserControl"/> has is finished.
        /// Therefore this event used to postpone Configuration changed messages until
        /// loading is complete.
        /// </summary>
        private async void ItemManager_UiLoadAssumedCompleteEvent()
        {
            await Task.Delay(200);
            if (userControl != null) // Had a weird exception here on closing the
            {
                userControl.SuspendConfigurationChanged = false;
            }
            UiLoadAssumedCompleteEvent -= ItemManager_UiLoadAssumedCompleteEvent;
            externalMessenger.Send(PluginDefinition.MessageIds.StateRequest);
        }

        public IAnyVisionProperties DataContext { get; set; }
        /// <summary>
        /// There is only a single item (ItemsAllowed.One). If none exists it will be created here
        /// but not saved. The single FQID for the item is created.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private VideoOS.Platform.Item FirstItem(VideoOS.Platform.Item item)
        {
            if (item != null) return item;
            var fqid = Constants.ItemFqid();
            item = GetItem(fqid);
            return item ?? new VideoOS.Platform.Item(fqid, "AnyVision Configuration");
        }
        #endregion

        #region Working with currentItem

        /// <summary>
        /// Get the name of the current Item.
        /// </summary>
        /// <returns></returns>
        public override string GetItemName() => userControl?.DisplayName ?? "";

        /// <summary>
        /// Update the name for current Item.  the user edited the Name via F2 in the TreeView
        /// </summary>
        /// <param name="name"></param>
        public override void SetItemName(string name)
        {
            if (userControl == null) return;
            userControl.DisplayName = name;
        }

        /// <summary>
        /// Validate the user entry, and return true for OK.<br/>
        /// External configuration should be saved during this call.<br/>
        /// Any errors should be displayed to the user, and the field in 
        /// error should get focus.
        /// </summary>
        /// <returns>Indicates error in user entry.  True is a valid entry</returns>
        public override bool ValidateAndSaveUserControl()
        {
            userControl.HasUnsavedChanges = false;
            if (CurrentItem == null) return true;
            //Get user entered fields
            userControl?.UpdateItem(CurrentItem);
            if (DataContext.IsTokenRenewalTimeChanged)
                externalMessenger.Send(PluginDefinition.MessageIds.TokenRenewalChanged, int.Parse(DataContext.TokenRenewalTime));
            //In this template we save configuration on the VMS system
            Configuration.Instance.SaveItemConfiguration(PluginDefinition.PluginId, CurrentItem);
            return true;
        }

        public override VideoOS.Platform.Item CreateItem(VideoOS.Platform.Item parentItem, FQID suggestedFQID)
        {
            CurrentItem = new VideoOS.Platform.Item(suggestedFQID, "Any Vision Configuration");
            userControl?.FillContent(CurrentItem);
            Configuration.Instance.SaveItemConfiguration(pluginId, CurrentItem);
            return CurrentItem;
        }

        /// <summary>
        /// When an administrator selects the context menu Delete item, or press the DEL key, 
        /// a confirmation dialog is displayed and upon administrator confirms this method is called.
        /// <code>
        /// For configurations saved on the video server, the following code can be used:
        /// if (item != null)
        /// {
        ///     Configuration.Instance.DeleteItemConfiguration(MyPluginId, item);
        ///	}
        /// </code>
        /// </summary>
        /// <param name="item">The Item to delete</param>
        public override void DeleteItem(VideoOS.Platform.Item item)
        {
            if (item == null) return;
            Configuration.Instance.DeleteItemConfiguration(pluginId, item);
        }
        #endregion

        #region Configuration Access Methods

        /// <summary>
        /// Returns a list of all Items of this Kind
        /// </summary>
        /// <returns>A list of items.  Allowed to return null if no Items found.</returns>
        public override List<VideoOS.Platform.Item> GetItems() =>
            Configuration.Instance.GetItemConfigurations(pluginId, null, kind);

        /// <summary>
        /// Returns a list of all Items from a specific server.
        /// </summary>
        /// <param name="parentItem">The parent Items</param>
        /// <returns>A list of items.  Allowed to return null if no Items found.</returns>
        public override List<VideoOS.Platform.Item> GetItems(VideoOS.Platform.Item parentItem) =>
            Configuration.Instance.GetItemConfigurations(pluginId, parentItem, kind);


        /// <summary>
        /// Always returns the same item. 
        /// Returns the Item defined by the FQID. Will return null if not found.
        /// </summary>
        /// <param name="fqid">Fully Qualified ID of an Item</param>
        /// <returns>An Item</returns>
        public override VideoOS.Platform.Item GetItem(FQID fqid)
        {
            var item= Configuration.Instance.GetItemConfiguration(pluginId, kind, new Guid(ItemGuid));
            return item;
        }

        #endregion

        #region Messages and Status

        /// <summary>
        /// Return all the Event groups this Kind of Item can deliver to the Event Server and Alarm PluginDefinition.
        /// This list is used for configuring alarms.
        /// </summary>
        /// <returns></returns>		
        public override Collection<EventGroup> GetKnownEventGroups(CultureInfo culture) =>
            new Collection<EventGroup>
            {
                Constants.AnyVisionEventGroup
            };

        /// <summary>
		/// Return all the Event Types, within the group this Kind of Item can deliver to the Event Server.
		/// This list is used for configuring alarms.
		/// </summary>
		/// <returns></returns>		
		public override Collection<EventType> GetKnownEventTypes(CultureInfo culture) =>
            new Collection<EventType>
            {
                Constants.Disconnected,
                Constants.Connected,
                Constants.Connecting
            };

        /// <summary>
        /// Return the operational state of a specific Item.
        /// This is used by the Event Server.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override OperationalState GetOperationalState(VideoOS.Platform.Item item) => OperationalState.Ok;

        /// <summary>
        /// Just before a context menu is displayed, each line on the context menu is checked for it should be enabled or disabled.
        /// This method is called with the following command (If allowed by the ItemNode definition)<br/>
        ///   "ADD" - for the "Add new ..." <br/>
        ///   "DELETE" - for the "Delete ..."<br/>
        ///   "RENAME" - for rename<br/>
        /// If your plugin has the configuration stored on another server, and management is not possible
        /// via the ItemManager, then this method can be used to disable all contextmenu actions.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public override bool IsContextMenuValid(string command) => true;
        #endregion

    }
}

