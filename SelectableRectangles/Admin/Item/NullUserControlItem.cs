﻿using System;
using AdminUi.Interfaces;
using Item = VideoOS.Platform.Item;

namespace SelectableRectangles.Admin.Item
{
    internal class NullUserControlItem : IUserControlItem
    {
        public event EventHandler ConfigurationChangedByUser;

        public void ClearContent()
        {
        }

        public void FillContent(VideoOS.Platform.Item item)
        {
        }

        public void UpdateItem(VideoOS.Platform.Item item)
        {
        }

        public string DisplayName { get; set; }
        public object DataContext { get; set; }
        public IFunctionality Functionality { get; set; }
        public bool SuspendConfigurationChanged { get; set; } = true;
        public bool HasUnsavedChanges { get; set; } = false;
    }
}
