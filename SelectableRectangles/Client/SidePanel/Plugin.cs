using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Extensions.Messaging;
using VideoOS.Platform;
using VideoOS.Platform.Client;

namespace SelectableRectangles.Client.SidePanel
{
    /// <summary>
    /// The Plugin defines a plugin that resides in the live or playback side panel in the Smart Client.
    /// Is it only created once during startup/login and never disposed.
    /// This class can be instantiated directly without making your own inherited class
    /// </summary>
    public class Plugin : VideoOS.Platform.Client.SidePanelPlugin
    {
        public override void Init()
        {
           
        }

        

        /// <summary>
        /// Flush any configuration or dynamic resources.
        /// </summary>
        public override void Close()
        {
            
        }

        /// <summary>
        /// Creates a new UserControl to be placed on the specified panel place.
        /// Size of this panel is limited and can not be wider than 188 pixels.
        /// </summary>
        /// <returns></returns>
        public override VideoOS.Platform.Client.SidePanelWpfUserControl GenerateWpfUserControl() => new UserControl();

        /// <summary>
        /// Identification of this SidePanelId
        /// </summary>
        public override Guid Id => PluginDefinition.SidePanelId;

        /// <summary>
        /// Name of panel - displayed on top of user control
        /// </summary>
        public override string Name => "Subject Search";
        /// <summary>
        /// Where to place this panel.
        /// </summary>
        public override List<SidePanelPlaceDefinition> SidePanelPlaceDefinitions =>
            new List<SidePanelPlaceDefinition> {
                new SidePanelPlaceDefinition {
                    WorkSpaceId = ClientControl.PlaybackBuildInWorkSpaceId,
                    WorkSpaceStates = new List<WorkSpaceState> { WorkSpaceState.Normal }
                }
            };
    }
}
