﻿using System;

namespace SelectableRectangles.Client.SidePanel
{
    public class SidePanelItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public byte[] Face { get; set; }
      
    }
}
