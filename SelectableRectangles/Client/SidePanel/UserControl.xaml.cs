using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Extensions.Messaging;
using VideoOS.Platform;
using VideoOS.Platform.Messaging;

namespace SelectableRectangles.Client.SidePanel
{
    public partial class UserControl : VideoOS.Platform.Client.SidePanelWpfUserControl
    {
        private readonly SimpleMessaging localMessenger;
        
        public UserControl()
        {
            InitializeComponent();
            Items = new ObservableCollection<SidePanelItem>();
            DataContext = Items;
            localMessenger = new SimpleMessaging(MessagingScope.Internal);
        }

        public override void Init()
        {
            localMessenger.Register<string>(AddSelected, PluginDefinition.MessageIds.AddToSelectedCommand);
        }
        public void AddSelected(string json)
        {
            var item = Newtonsoft.Json.JsonConvert.DeserializeObject<SidePanelItem>(json);
            if (Items.Any(t => t.Id == item.Id))
            {
                MessageBox.Show("Item is already added", "Already Added", MessageBoxButton.OK);
                return;
            }
            Items.Add(item);
        }

        public ObservableCollection<SidePanelItem> Items { get; set; }

        public override void Close()
        {
            localMessenger.DeRegister();
        }

        /// <summary>
        /// Sample code to show how to maximize the current view item to fill the entire layout area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMax_Click(object sender, RoutedEventArgs e)
        {
            EnvironmentManager.Instance.SendMessage(
                new Message(MessageId.SmartClient.ViewItemControlCommand)
                { Data = ViewItemControlCommandData.MaximizeSelectedViewItem });
        }

        /// <summary>
        /// Sample code to show how to restore the view to normal mode, e.g. show all view items.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMin_Click(object sender, RoutedEventArgs e)
        {
            EnvironmentManager.Instance.SendMessage(
                new Message(MessageId.SmartClient.ViewItemControlCommand)
                { Data = ViewItemControlCommandData.RestoreSelectedViewItem });
        }

        private void RemoveItem(object sender, RoutedEventArgs e)
        {
            Items.RemoveAt(0);
        }

        private void ClearList(object sender, RoutedEventArgs e)
        {
            var message = "Click \"Yes\" to clear the list";
            if (MessageBoxResult.Yes == MessageBox.Show(message, "Confirm", MessageBoxButton.YesNo))
                Items.Clear();
            
        }
    }
}
