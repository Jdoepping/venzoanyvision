using System;
using System.Collections.Generic;
using VideoOS.Platform;
using VideoOS.Platform.Client;

namespace SelectableRectangles.Client
{
    class WorkSpaceToolbarPlugin : VideoOS.Platform.Client.WorkSpaceToolbarPlugin
    {
        public WorkSpaceToolbarPlugin()
        {
        }

        public override Guid Id => PluginDefinition.WorkSpaceToolbarPluginId;
        public override string Name => "SelectableRectangles";

        public override void Init()
        {
            WorkSpaceToolbarPlaceDefinition.WorkSpaceIds = new List<Guid>() { ClientControl.LiveBuildInWorkSpaceId, ClientControl.PlaybackBuildInWorkSpaceId, PluginDefinition.WorkSpacePluginId };
            WorkSpaceToolbarPlaceDefinition.WorkSpaceStates = new List<WorkSpaceState>() { WorkSpaceState.Normal };
        }

        public override void Close()
        {
        }

        public override VideoOS.Platform.Client.WorkSpaceToolbarPluginInstance GenerateWorkSpaceToolbarPluginInstance()
        {
            return new WorkSpaceToolbarPluginInstance();
        }
    }
}
