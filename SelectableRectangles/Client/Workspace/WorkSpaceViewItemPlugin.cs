using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using VideoOS.Platform.Client;

namespace SelectableRectangles.Client
{
    public class WorkSpaceViewItemPlugin : VideoOS.Platform.Client.ViewItemPlugin
    {
        private static System.Drawing.Image _treeNodeImage;

        public WorkSpaceViewItemPlugin()
        {
            _treeNodeImage = Properties.Resources.WorkSpaceIcon;
        }

        public override Guid Id => PluginDefinition.WorkSpaceViewItemPluginId;

        public override System.Drawing.Image Icon => _treeNodeImage;

        public override string Name => "WorkSpace Plugin View Item";

        public override bool HideSetupItem => false;

        public override VideoOS.Platform.Client.ViewItemManager GenerateViewItemManager()
        {
            return new WorkSpaceViewItemManager();
        }

        public override void Init()
        {
        }

        public override void Close()
        {
        }


    }
}
