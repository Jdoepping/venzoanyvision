using System;
using System.Collections.Generic;
using System.Drawing;
using Extensions.Messaging;
using VideoOS.Platform;
using VideoOS.Platform.Messaging;

namespace SelectableRectangles.Client
{
    /// <summary>
    /// Relies on https://github.com/cascadia-technology/ModernBrowser, the "ModernBrowser" must be installed. 
    /// </summary>
    public class WorkSpacePlugin : VideoOS.Platform.Client.WorkSpacePlugin
    {
        private SimpleMessaging localMessenger;

        private bool workSpaceSelected;
        private bool workSpaceViewSelected;
        public override Guid Id => PluginDefinition.WorkSpacePluginId;
        /// <summary>
        /// The name displayed on top
        /// </summary>
        public override string Name => "AnyVision";
        /// <summary>
        /// We support setup mode
        /// </summary>
        public override bool IsSetupStateSupported => true;
        public override void Init()
        {
            LoadProperties(true);
            //add message listeners
            localMessenger = new SimpleMessaging(MessagingScope.Internal);
            localMessenger.Register(ShownWorkSpaceChangedReceiver, MessageId.SmartClient.ShownWorkSpaceChangedIndication);
            localMessenger.Register(WorkSpaceStateChangedReceiver,MessageId.SmartClient.WorkSpaceStateChangedIndication);
            localMessenger.Register(SelectedViewChangedReceiver, MessageId.SmartClient.SelectedViewChangedIndication);

            //build view layout - modify to your needs. Here we use a matrix of 1000x1000 to define the layout 
            var rectangles = new []{new Rectangle(0, 0, 1000, 1000)};
            // Index 0 = Used by a camera below
            ViewAndLayoutItem.Layout = rectangles;
            ViewAndLayoutItem.Name = Name;
            var item = SelectableRectangles.Background.Constants.GetAnyVisionSettings();

            var properties = new Dictionary<string, string>
            {
                {"Address", item.BaseUrl},
                {"EnableSmartClientScripting", "true"},
                {"HideNavigationBar", "false"},
                { "ShowNavigationBar", "true"}
                //{"Scaling", "1" }  //properties2.Add("Scaling", "4"); // fit in 640x480
            };
            ViewAndLayoutItem.InsertViewItemPlugin(0, new ModernBrowser.BrowserViewItemPlugin(), properties);
        }
        public override void Close() => localMessenger.DeRegister();

        /// <summary>
        /// User modified something in setup mode
        /// </summary>
        /// <param name="index"></param>
        public override void ViewItemConfigurationModified(int index)
        {
            base.ViewItemConfigurationModified(index);
        }

        /// <summary>
        /// Keep track of what workspace is selected, if this is selected the _workSpaceViewSelected is true.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sender"></param>
        /// <param name="related"></param>
        /// <returns></returns>
        private object ShownWorkSpaceChangedReceiver(Message message, FQID sender, FQID related)
        {
            workSpaceSelected = IsForThisWorkSpace(message);
            Notification = workSpaceSelected ? null : Notification;
            return null;
        }

        /// <summary>
        /// Keep track of current state: in setup or normal
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sender"></param>
        /// <param name="related"></param>
        /// <returns></returns>
        private object WorkSpaceStateChangedReceiver(Message message, FQID sender, FQID related)
        {
            if (workSpaceSelected && ((WorkSpaceState)message.Data) == WorkSpaceState.Normal)
            {
                // Went in or out of Setup state
            }
            return null;
        }
        /// <summary>
        /// Keep track of what workspace is selected, if this is selected the _workSpaceViewSelected is true.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="sender"></param>
        /// <param name="related"></param>
        /// <returns></returns>
        private object SelectedViewChangedReceiver(Message message, FQID sender, FQID related)
        {
            workSpaceViewSelected = IsForThisWorkSpace(message);
            return null;
        }
        private bool IsForThisWorkSpace(Message message) => message.Data is Item item && item.FQID.ObjectId == Id;
    }
}
