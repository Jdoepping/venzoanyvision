using VideoOS.Platform.Client;

namespace SelectableRectangles.Client
{
    public class WorkSpaceViewItemManager : VideoOS.Platform.Client.ViewItemManager
    {
        public WorkSpaceViewItemManager() : base("WorkSpaceViewItemManager")
        {
        }

        public override VideoOS.Platform.Client.ViewItemWpfUserControl GenerateViewItemWpfUserControl() =>
            new WorkSpaceViewItemWpfUserControl();
    }
}
