using VideoOS.Platform;

namespace SelectableRectangles.Client
{
    class WorkSpaceToolbarPluginInstance : VideoOS.Platform.Client.WorkSpaceToolbarPluginInstance
    {
        private Item _window;

        public WorkSpaceToolbarPluginInstance()
        {
        }

        public override void Init(Item window)
        {
            _window = window;

            Title = "SelectableRectangles";
        }

        public override void Activate()
        {
            // Here you should put whatever action that should be executed when the toolbar button is pressed
        }

        public override void Close()
        {
        }

    }
}