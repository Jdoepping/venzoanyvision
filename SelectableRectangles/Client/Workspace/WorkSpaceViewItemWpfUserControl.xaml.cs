using System;
using VideoOS.Platform.Client;

namespace SelectableRectangles.Client
{
    /// <summary>
    /// Interaction logic for SelectableRectanglesWorkSpaceViewItemWpfUserControl.xaml
    /// </summary>
    public partial class WorkSpaceViewItemWpfUserControl : VideoOS.Platform.Client.ViewItemWpfUserControl
    {
        public WorkSpaceViewItemWpfUserControl()
        {
            InitializeComponent();
        }

        public override void Init()
        {
        }

        public override void Close()
        {
        }

        /// <summary>
        /// Do not show the sliding toolbar!
        /// </summary>
        public override bool ShowToolbar => false;

        private void ViewItemWpfUserControl_ClickEvent(object sender, EventArgs e)
        {
            FireClickEvent();
        }

        private void ViewItemWpfUserControl_DoubleClickEvent(object sender, EventArgs e)
        {
            FireDoubleClickEvent();
        }
    }
}
