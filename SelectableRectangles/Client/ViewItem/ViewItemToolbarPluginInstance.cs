using Newtonsoft.Json;
using VideoOS.CustomDevelopment.Implementations;
using VideoOS.Platform;

namespace SelectableRectangles.Client
{
    class ViewItemToolbarPluginInstance : VideoOS.Platform.Client.ViewItemToolbarPluginInstance
    {
        private Item viewItemInstance;
        private Item window;
        private MessagingWrapper messenger;

        public override void Init(Item viewItemInstance, Item window)
        {
            this.viewItemInstance = viewItemInstance;
            this.window = window;

            Title = "Find faces";
            Tooltip = "BroadcastListener tooltip";
            Icon = Properties.Resources.eye_3_32.ToBitmap();
        }

        /// <summary>
        /// Initiate a process that will find faces in the current frame.
        /// </summary>
        public override void Activate()
        {
            messenger = messenger ?? new MessagingWrapper(MessagingWrapper.MessagingProvider.External);
            var message = new VideoOS.Platform.Messaging.Message(PluginDefinition.MessageIds.InitiateFaceSearch, JsonConvert.SerializeObject(viewItemInstance.FQID));
            messenger.MessageReceiver.TransmitMessage(message, null, null, viewItemInstance.FQID);
        }

        public override void Close() => messenger?.DeRegister();
    }
}