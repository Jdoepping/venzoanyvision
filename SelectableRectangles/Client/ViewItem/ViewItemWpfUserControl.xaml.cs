using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Extensions.Messaging;
using SelectableRectangles.Background;
using SelectableRectangles.Background.ApiClasses;
using SelectableRectangles.Custom;
using SelectableRectangles.Shared;
using VideoOS.Platform;
using VideoOS.Platform.Client;
using VideoOS.Platform.Messaging;
using Color = System.Windows.Media.Color;

namespace SelectableRectangles.Client
{
    /// <summary>
    /// The ViewItemWpfUserControl is the WPF version of the ViewItemUserControl. It is instantiated for every position it is created on the current visible view. When a user select another View or ViewLayout, this class will be disposed.  No permanent settings can be saved in this class.
    /// The Init() method is called when the class is initiated and handle has been created for the UserControl. Please perform resource initialization in this method.
    /// <br>
    /// If Message communication is performed, register the MessageReceivers during the Init() method and UnRegister the receivers during the Close() method.
    /// <br>
    /// The Close() method can be used to Dispose resources in a controlled manor.
    /// <br>
    /// Mouse events not used by this control, should be passed on to the Smart Client by issuing the following methods:<br>
    /// FireClickEvent() for single click<br>
    ///	FireDoubleClickEvent() for double click<br>
    /// The single click will be interpreted by the Smart Client as a selection of the item, and the double click will be interpreted to expand the current viewitem to fill the entire View.
    /// </summary>
    public partial class ViewItemWpfUserControl : VideoOS.Platform.Client.ViewItemWpfUserControl, INotifyPropertyChanged
    {
        #region Component private class variables
        private ImageViewerWpfControl _imageViewerControl;
        private SelectableRectangles.Client.ViewItemManager _viewItemManager;
        private object _themeChangedReceiver;
        private readonly SimpleMessaging externalMessenger;
        private readonly SimpleMessaging localMessenger;
        private SidebarData sidebarData;
        #endregion

        #region Component constructors + dispose

        /// <summary>
		/// Constructs a SelectableRectanglesViewItemUserControl instance
        /// </summary>
		public ViewItemWpfUserControl(SelectableRectangles.Client.ViewItemManager viewItemManager)
        {
            _viewItemManager = viewItemManager;
            InitializeComponent();
            SetHeaderColors();
            DataContext = this;
            localMessenger = new SimpleMessaging(MessagingScope.Internal);
            externalMessenger = new SimpleMessaging(MessagingScope.External);
            externalMessenger.Register<byte[]>(HandleImageReceived, PluginDefinition.MessageIds.ImageSelected);
            localMessenger.Register(PlaybackModeChanged, MessageId.SmartClient.PlaybackIndication);
        }

        private object PlaybackModeChanged(VideoOS.Platform.Messaging.Message message, FQID destination, FQID sender)
        {
            var playbackCommand = (PlaybackCommandData) message.Data;
            if (playbackCommand.Command != "PlayStop")
            {
                Sidebar = null;
            }
            return null;
        }
        private async void HandleImageReceived(byte[] image, FQID destination)
        {
            if (IsForThisViewItem(destination)) return; // think this is not needed
            // Show the image before the return from AnyVision (which probably takes a looong time)
            ClientControl.Instance.CallOnUiThread(new Action(() =>
                        Sidebar = new SidebarData(Guid.Empty)
                        {
                            PictureCutOut = image,
                        }
                    )
            );
            var api = new AnyVisionApi();
            var sideBar = await api.GetSidebarData();
            ClientControl.Instance.CallOnUiThread(new Action(() =>
                { 
                    sideBar.PictureCutOut = image;
                   Sidebar = sideBar;
               })
            );
        }
        public SidebarData Sidebar
        {
            get => sidebarData; 
            set
            {
                sidebarData = value;
                AddToSearchButton.IsEnabled = value != null;
                //SideBarFragment.MainImage.Visibility = value!=null ? Visibility.Visible: Visibility.Collapsed;
                OnPropertyChanged(nameof(Sidebar));
            }
        }

       

        /// <summary>
        /// _viewItemManager.FQID and destination are not the same.
        /// I thought they should be... 
        /// </summary>
        /// <param name="destination"></param>
        /// <returns></returns>
        private bool IsForThisViewItem(FQID destination) => _viewItemManager.FQID.Equals(destination);//_viewItemManager.FQID.ObjectIdString == destination.ObjectIdString;

        private static System.Windows.Media.Color GetWindowsMediaColor(System.Drawing.Color inColor)
        {
            return Color.FromArgb(inColor.A, inColor.R, inColor.G, inColor.B);
        }

        private void SetHeaderColors()
        {
            var theme = ClientControl.Instance.Theme;
            var headerColor = Selected ? theme.ViewItemSelectedHeaderColor : theme.ViewItemHeaderColor;
            var headerTextColor = Selected ? theme.ViewItemSelectedHeaderTextColor : theme.ViewItemHeaderTextColor;
            var headerBrush = new SolidColorBrush(GetWindowsMediaColor(headerColor));
            var headerTextBrush = new SolidColorBrush(GetWindowsMediaColor(headerTextColor));
            
            //_headerGrid.Background = headerBrush;
            //_nameTextBlock.Foreground = headerTextBrush;
            //_headerTextBlock.Foreground = headerTextBrush;
        }

        private void SetUpApplicationEventListeners()
        {
            //set up ViewItem event listeners
            _viewItemManager.PropertyChangedEvent += ViewItemManagerPropertyChangedEvent;
            _themeChangedReceiver = EnvironmentManager.Instance.RegisterReceiver(
                new MessageReceiver(ThemeChangedIndicationHandler),
                new MessageIdFilter(MessageId.SmartClient.ThemeChangedIndication));
        }

        private void RemoveApplicationEventListeners()
        {
            //remove ViewItem event listeners
            _viewItemManager.PropertyChangedEvent -= ViewItemManagerPropertyChangedEvent;

            EnvironmentManager.Instance.UnRegisterReceiver(_themeChangedReceiver);
            _themeChangedReceiver = null;
        }

        /// <summary>
        /// Method that is called immediately after the view item is displayed.
        /// </summary>
        public override void Init()
        {
            _imageViewerControl = new ImageViewerWpfControl(WindowInformation);
            _imageViewerControl.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            _imageViewerControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            _imageViewerControl.EnableMouseControlledPtz = true;
            _imageViewerControl.Selected = true;
            //_imageViewerControl.CameraFQID = null;
            CanvasVideo.Children.Add(_imageViewerControl);
            SetUpApplicationEventListeners();
            _imageViewerControl.Initialize();               // Make sure Click events have been configured
            if (_viewItemManager.SelectedCamera != null)
            {
                ViewItemManagerPropertyChangedEvent(this, null);
            }
        }

        #region Component events

        private void ImageViewerControlClickEvent(object sender, EventArgs e)
        {
            FireClickEvent();
            _imageViewerControl.Selected = true;
        }

        void _imageViewerControl_DoubleClickEvent(object sender, EventArgs e)
        {
            //Forward the double click event to the Smart Client, to let it handle maximize / restore the view layout
            FireDoubleClickEvent();
        }

        void _imageViewerControl_RightClickEvent(object sender, EventArgs e)
        {
           
        }

      

        private void _imageViewerControl_Drop(object sender, System.Windows.DragEventArgs e)
        {
            // Obtaining the Guid of the dropped camera like this
            var cameraGuidList = e.Data.GetData("VideoOS.RemoteClient.Application.DragDrop.DraggedDeviceIdList") as List<Guid>;
            if (cameraGuidList != null && cameraGuidList.Any())
            {
                _viewItemManager.SelectedCamera = Configuration.Instance.GetItem(cameraGuidList[0], Kind.Camera);
                ViewItemManagerPropertyChangedEvent(null, null);
                e.Handled = true;
            }
            else
            {
                MessageBox.Show("DragDrop: the only supported dragged content for this plugin is a camera");

                // Keeping this false lets the SmartClient to continue handling this event (in the case if you dropped another plugin, it will change to this plugin in the view)
                e.Handled = false;
            }
        }
        #endregion

        /// <summary>
        /// Method that is called when the view item is closed. The view item should free all resources when the method is called.
        /// Is called when userControl is not displayed anymore. Either because of 
        /// user clicking on another View or Item has been removed from View.
        /// </summary>
        public override void Close()
        {
            RemoveApplicationEventListeners();
        }

        #endregion

        #region Print method

        /// <summary>
        /// Method that is called when print is activated while the content holder is selected.
        /// </summary>
        public override void Print()
        {
            Print("Name of this item", "Some extra information");
        }

        #endregion

        #region Component events

        private void ViewItemWpfUserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    FireClickEvent();
                    break;
                case MouseButton.Right:
                    FireRightClickEvent(e);
                    break;
            }
        }

        private void ViewItemWpfUserControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                FireDoubleClickEvent();
            }
        }

        /// <summary>
        /// Signals that the form is right clicked
        /// </summary>
        public event EventHandler RightClickEvent;

        /// <summary>
        /// Activates the RightClickEvent
        /// </summary>
        /// <param name="e">Event args</param>
        protected virtual void FireRightClickEvent(EventArgs e)
        {
            RightClickEvent?.Invoke(this, e);
        }

        void ViewItemManagerPropertyChangedEvent(object sender, EventArgs e)
        {
            if (_imageViewerControl.CameraFQID != null)
            {
                _imageViewerControl.Disconnect();
            }
            _imageViewerControl.CameraFQID = _viewItemManager.SelectedCamera.FQID;
            _imageViewerControl.Initialize();
            _imageViewerControl.Connect();
        }

        private object ThemeChangedIndicationHandler(VideoOS.Platform.Messaging.Message message, FQID destination, FQID source)
        {
            SetHeaderColors();
            return null;
        }

        #endregion

        #region Component properties

        /// <summary>
        /// Gets boolean indicating whether the view item can be maximized or not. <br/>
        /// The content holder should implement the click and double click events even if it is not maximizable. 
        /// </summary>
        public override bool Maximizable => true;

        /// <summary>
        /// Tell if ViewItem is selectable
        /// </summary>
        public override bool Selectable => true;

        /// <summary>
        /// Make support for Theme colors to show if this ViewItem is selected or not.
        /// </summary>
        public override bool Selected
        {
            get => base.Selected;
            set
            {
                base.Selected = value;
                SetHeaderColors();
            }
        }

        #endregion

        private async void AddSubject(object sender, RoutedEventArgs e)
        {
            var item = new SidePanel.SidePanelItem
            {
                Id = Sidebar.Id,
                Name = Sidebar.SubjectName,
                Face = Sidebar.PictureCutOut,

            };
            var json =Newtonsoft.Json.JsonConvert.SerializeObject(item);
            localMessenger.Send(PluginDefinition.MessageIds.AddToSelectedCommand,json);
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
