using System.Collections.Generic;
using VideoOS.Platform;

namespace SelectableRectangles.Client
{
    /// <summary>
    /// The ViewItemManager contains the configuration for the ViewItem. <br/>
    /// When the class is initiated it will automatically recreate relevant ViewItem configuration saved in the properties collection from earlier.
    /// Also, when the viewlayout is saved the ViewItemManager will supply current configuration to the SmartClient to be saved on the server.<br/>
    /// This class is only relevant when executing in the Smart Client.
    /// </summary>
    public class ViewItemManager : VideoOS.Platform.Client.ViewItemManager
    {
        private Item selectedCamera;

        public ViewItemManager() : base("SelectableRectanglesViewItemManager")
        {
        }

        #region Methods overridden 
        /// <summary>
        /// The properties for this ViewItem is now loaded into the base class and can be accessed via 
        /// GetProperty(key) and SetProperty(key,value) methods
        /// </summary>
        public override void PropertiesLoaded()
        {
            var fqidString = GetProperty("SelectedFQID");
            if (string.IsNullOrEmpty(fqidString)) return;
            var cameraFQID = new FQID(fqidString);
            selectedCamera = Configuration.Instance.GetItem(cameraFQID);
        }

        ///// <summary>
        ///// Generate the UserControl containing the actual ViewItem Content.
        ///// 
        ///// For new plugins it is recommended to use GenerateViewItemWpfUserControl() instead. Only implement this one if support for Smart Clients older than 2017 R3 is needed.
        ///// </summary>
        ///// <returns></returns>
        //public override ViewItemUserControl GenerateViewItemUserControl()
        //{
        //	return new SelectableRectanglesViewItemUserControl(this);
        //}

        /// <summary>
        /// Generate the UserControl containing the actual ViewItem Content.
        /// </summary>
        /// <returns></returns>
        public override VideoOS.Platform.Client.ViewItemWpfUserControl GenerateViewItemWpfUserControl()
        {
            var ctrl =  new ViewItemWpfUserControl(this);
            ClientControl.Instance.RegisterUIControlForAutoTheming(ctrl.LayoutRoot);
            return ctrl;
        }

        ///// <summary>
        ///// Generate the UserControl containing the property configuration.
        ///// 
        ///// For new plugins it is recommended to use GeneratePropertiesWpfUserControl() instead. Only implement this one if support for Smart Clients older than 2017 R3 is needed.
        ///// </summary>
        ///// <returns></returns>
        //public override PropertiesUserControl GeneratePropertiesUserControl()
        //{
        //	return new SelectableRectanglesPropertiesUserControl(this);
        //}

        /// <summary>
        /// Generate the UserControl containing the property configuration.
        /// </summary>
        /// <returns></returns>
        public override VideoOS.Platform.Client.PropertiesWpfUserControl GeneratePropertiesWpfUserControl() =>
         new PropertiesWpfUserControl(this);


        #endregion

        public IEnumerable<Item> ConfigItems { get; private set; } = new List<Item>();

        public Item SelectedCamera
        {
            get => selectedCamera;
            set
            {
                selectedCamera = value;
                SetProperty("SelectedFQID", selectedCamera.FQID.ToXmlNode().OuterXml);
                SaveProperties();
            }
        }
    }
}
