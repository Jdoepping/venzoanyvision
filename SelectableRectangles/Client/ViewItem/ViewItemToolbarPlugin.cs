using System;
using System.Collections.Generic;
using VideoOS.Platform;
using VideoOS.Platform.Client;

namespace SelectableRectangles.Client
{
    class ViewItemToolbarPlugin : VideoOS.Platform.Client.ViewItemToolbarPlugin
    {
        public override Guid Id => PluginDefinition.ViewItemToolbarPluginId;

        public override string Name => "SelectableRectangles";

        public override ToolbarPluginOverflowMode ToolbarPluginOverflowMode => ToolbarPluginOverflowMode.AsNeeded;

        public override void Init()
        {
            ViewItemToolbarPlaceDefinition.ViewItemIds = new List<Guid> { ViewAndLayoutItem.CameraBuiltinId, PluginDefinition.ViewItemPluginId };
            ViewItemToolbarPlaceDefinition.WorkSpaceIds = new List<Guid>{ ClientControl.LiveBuildInWorkSpaceId, ClientControl.PlaybackBuildInWorkSpaceId, PluginDefinition.WorkSpacePluginId };
            ViewItemToolbarPlaceDefinition.WorkSpaceStates = new List<WorkSpaceState>{ WorkSpaceState.Normal };
        }

        public override void Close()
        {
        }

        public override VideoOS.Platform.Client.ViewItemToolbarPluginInstance GenerateViewItemToolbarPluginInstance() =>
            new ViewItemToolbarPluginInstance();
    }
}
