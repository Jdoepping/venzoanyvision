using VideoOS.Platform;

namespace SelectableRectangles.Client
{
    internal class ComboBoxNode
    {
        internal Item Item { get; private set; }
        internal ComboBoxNode(Item item)
        {
            Item = item;
        }

        public override string ToString()
        {
            return Item.Name;
        }
    }
}