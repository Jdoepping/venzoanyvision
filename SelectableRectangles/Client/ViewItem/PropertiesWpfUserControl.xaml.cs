using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Forms;
using VideoOS.Platform.Client;
using VideoOS.Platform;
using VideoOS.Platform.UI;

namespace SelectableRectangles.Client
{
    /// <summary>
    /// This UserControl contains the visible part of the Property panel during Setup mode. <br/>
    /// If no properties is required by this ViewItemPluginId, the GeneratePropertiesUserControl() method on the ViewItemManager can return a value of null.
    /// <br/>
    /// When changing properties the ViewItemManager should continuously be updated with the changes to ensure correct saving of the changes.
    /// <br/>
    /// As the user click on different ViewItem, the displayed property UserControl will be disposed, and a new one created for the newly selected ViewItem.
    /// </summary>
    public partial class PropertiesWpfUserControl : VideoOS.Platform.Client.PropertiesWpfUserControl
    {
        #region private fields

        private SelectableRectangles.Client.ViewItemManager _viewItemManager;

        #endregion

        #region Initialization & Dispose

        /// <summary>
        /// This class is created by the ViewItemManager.  
        /// </summary>
        /// <param name="viewItemManager"></param>
        public PropertiesWpfUserControl(SelectableRectangles.Client.ViewItemManager viewItemManager)
        {
            _viewItemManager = viewItemManager;
            InitializeComponent();
        }

        /// <summary>
        /// Setup events and message receivers and load stored configuration.
        /// </summary>
        public override void Init()
        {
            
        }
        public override void Close()
        {
        }

        ///// <summary>
        ///// We have some configuration from the server, that the user can choose from.
        ///// </summary>
        ///// <param name="config"></param>
        ///// <param name="selectedId"></param>
        //internal void FillContent(IEnumerable<Item> config, Guid selectedId)
        //{
        //    comboBoxID.Items.Clear();
        //    ComboBoxNode selectedComboBoxNode = null;
        //    foreach (var item in config)
        //    {
        //        var comboBoxNode = new ComboBoxNode(item);
        //        comboBoxID.Items.Add(comboBoxNode);
        //        if (item.FQID.ObjectId == selectedId)
        //            selectedComboBoxNode = comboBoxNode;
        //    }
        //    if (selectedComboBoxNode != null)
        //        comboBoxID.SelectedItem = selectedComboBoxNode;
        //}

        #endregion

        

        private void OnSourceSelected(object sender, System.Windows.RoutedEventArgs e)
        {
            var form = new ItemPickerForm();
            form.KindFilter = Kind.Camera;
            form.AutoAccept = true;
            form.Init(Configuration.Instance.GetItems());
            if (form.ShowDialog() != DialogResult.OK) return;

            _viewItemManager.SelectedCamera = form.SelectedItem;
            buttonSelect.Content = _viewItemManager.SelectedCamera.Name;
        }
    }
}
