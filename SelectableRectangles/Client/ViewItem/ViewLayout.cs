using System;
using System.Drawing;
using VideoOS.Platform.Client;

namespace SelectableRectangles.Client
{
    public class ViewLayout : VideoOS.Platform.Client.ViewLayout
    {
        public override Image Icon
        {
            get => PluginDefinition.TreeNodeImage;
            set { }
        }


        public override Rectangle[] Rectangles
        {
            get => new Rectangle[] { new Rectangle(000, 000, 999, 499), new Rectangle(000, 499, 499, 499), new Rectangle(499, 499, 499, 499) };
            set { }
        }

        public override Guid Id
        {
            get => PluginDefinition.ViewLayoutId;
            set { }
        }

        public override string DisplayName
        {
            get => "SelectableRectangles";
            set { }
        }
    }
}
