﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;

namespace SelectableRectangles.Shared
{
    public class Utilities
    {
        public static string GetBitmapAsBase64String(Bitmap bitmap)
        {
            using (var ms = new MemoryStream())
            {
                bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return Convert.ToBase64String(ms.GetBuffer());
            }
        }
        public static byte[] GetBitmapAsByteArray(Bitmap bitmap)
        {
            using (var stream = new MemoryStream())
            {
                bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
                return stream.ToArray();
            }
        }
        public static byte[] GetBitmapImageAsByteArray(BitmapImage bitmap)
        {
            BitmapEncoder encoder = new BmpBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmap));
            using (var ms = new MemoryStream())
            {
                encoder.Save(ms);
                return ms.ToArray();
            }
        }
        /// <summary>
        /// Turns out CacheOption is extremely important. The picture will not show without it.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static BitmapImage ToImage(byte[] array)
        {
            using (var ms = new System.IO.MemoryStream(array))
            {
                var image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad; // here
                image.StreamSource = ms;
                image.EndInit();
                return image;
            }
        }

        public static Guid SavePhoto(BitmapImage objImage)
        {

            Guid photoID = System.Guid.NewGuid();
            //var photolocation = photoID.ToString() + ".jpg";  //file name

            var encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(objImage));

            using (var filestream = new FileStream(@"c:\temp\klipklyp.jpg", FileMode.Create))
            {
                encoder.Save(filestream);
            }

            return photoID;
        }

        public static string ToBase64(BitmapImage image, string format)
        {
            return Convert.ToBase64String(Encode(image, format));
        }
        private static byte[] Encode(BitmapImage bitmapImage, string format)
        {
            byte[] data = null;
            BitmapEncoder encoder = null;
            switch (format.ToUpper())
            {
                case "PNG":
                    encoder = new PngBitmapEncoder();
                    break;
                case "GIF":
                    encoder = new GifBitmapEncoder();
                    break;
                case "BMP":
                    encoder = new BmpBitmapEncoder();
                    break;
                case "JPG":
                    encoder = new JpegBitmapEncoder();
                    break;
            }
            if (encoder != null)
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                using (var ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    data = ms.ToArray();
                }
            }
            return data;
        }
        
    }
}
