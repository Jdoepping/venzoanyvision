﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using SelectableRectangles.Background;
using SelectableRectangles.Background.ApiClasses;

namespace SelectableRectangles.Shared
{
    public class AnyVisionApi
    {

        public async Task<List<MyRectangle>> MakeFaceSearch(Bitmap image)
        {
            List<MyRectangle> rectangles;
            try
            {
                rectangles = await MakeFaceSearchDummy();
                //rectangles = await MakeFaceSearch02(image);
            }
            catch (Exception ex)
            {
                rectangles = await MakeFaceSearchDummy();
            }
            return rectangles;
        }

        public async Task<List<MyRectangle>> MakeFaceSearch02(Bitmap bitmap)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri("https://localhost:44349/")
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var bitmapAsString = Utilities.GetBitmapAsBase64String(bitmap);
            var boundary = "---011000010111000001101";
            var multiForm = new MultipartFormDataContent(boundary);
            var content = new StringContent(bitmapAsString, Encoding.UTF8, "multipart/form-data");
            multiForm.Add(content, "bitmap");
            var response = await client.PostAsync("api/image/extract-faces", multiForm);
            response.EnsureSuccessStatusCode();
            var s = await response.Content.ReadAsStringAsync();
            var r = Newtonsoft.Json.JsonConvert.DeserializeObject<ImageExtractResponse>(s);
            return r.ToMyRectangles().ToList();
        }


        public async Task<SidebarData> GetSidebarData()
        {
            return await Task.Run(() => new SidebarData(Guid.NewGuid())
            {
                Actions = new[] {"Action01", "Action02", "Action03", "Action04"},
                Attributes = new[] {"Backpack", "Male"},
                BodyImages = GetBodyImages(),
                Description = "A long description",
                CreatedDate = DateTime.Now,
                FaceImages = GetFaceImages(),
                Rankings = new[] {"Ranking01", "Ranking02", "Ranking03"},
                SubjectName = "Donald Duck",
                SubjectGroup = "The Duck Family"
            });
        }

        private async Task<List<MyRectangle>> MakeFaceSearchDummy()
        {
            return new List<MyRectangle>
            {
                new MyRectangle {Id=1, X = 10, Y = 30, Width = 160, Height = 22},
                new MyRectangle {Id=2, X = 220, Y = 100, Width = 60, Height = 60},
            };
        }

        private IEnumerable<Uri> GetBodyImages()
        {
            var path = @"C:\Users\jdoep\source\repos\Milestone\Venzo\SelectableRectangles\SelectableRectangles\TestImages\Donald\body\";
            return Directory.GetFiles(path).Select(t => new Uri(t));
        }

        private IEnumerable<Uri> GetFaceImages()
        {
            var path = @"C:\Users\jdoep\source\repos\Milestone\Venzo\SelectableRectangles\SelectableRectangles\TestImages\Donald\faces\";
            return Directory.GetFiles(path).Select(t => new Uri(t));
        }
    }
}
