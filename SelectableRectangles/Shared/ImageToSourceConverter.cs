﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace SelectableRectangles.Shared
{
    public class ImageToSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(ImageSource)) return value;
            switch (value)
            {
                case byte[] s:
                    var y = (ImageSource)Utilities.ToImage(s);
                    return y;
                case Uri uri:
                    var x = uri;
                    return x;
                default:
                    return null;

            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}