﻿using System.Collections.Generic;
using VideoOS.Platform;

namespace MilestoneSdkExtensions
{
    public static class CameraExtensions
    {
        public static IEnumerable<Item> GetFlatCameraList() => Cameras(Configuration.Instance.GetItemsByKind(Kind.Camera));
        public static IEnumerable<Item> Cameras(this IReadOnlyCollection<Item> parent)
        {
            var cams = new List<Item>();
            if (parent == null) return cams;
            foreach (var item in parent)
            {
                if (item.IsACamera()) cams.Add(item);
                else
                    cams.AddRange(Cameras(item.GetChildren()));
            }
            return cams;
        }
        public static bool IsACamera(this Item item) => item.FQID.Kind == Kind.Camera && item.FQID.FolderType == FolderType.No && item.FQID.Kind == Kind.Camera;

    }
}
