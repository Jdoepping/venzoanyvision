﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VideoOS.Platform;
using VideoOS.Platform.ConfigurationItems;
using VideoOS.Platform.Data;

namespace MilestoneSdkExtensions
{
    public static class AnalyticsEventFolderExtensions
    {
        /// <summary>
        /// Creates the events specified by the parameter eventIds, if they do not already exists.
        /// An analytical event is created with what ends up being the display name or name property. Silently
        /// an identity property is created with a guid id but as a string.  
        /// </summary>
        /// <param name="folder">The Analytical events folder</param>
        /// <param name="eventIds">A list of names</param>
        /// <returns>Each ServerTask element contains the success/failure status for the creation of the analytical event</returns>
        public static async Task<ServerTask[]> Create(this AnalyticsEventFolder folder, IEnumerable<string> eventIds) =>
            await Helper(eventIds.Where(folder.NotExists), folder.AddAnalyticsEvent);
        public static async Task<ServerTask[]> Remove(this AnalyticsEventFolder folder, IEnumerable<string> eventIds) =>
                await Helper(eventIds.Where(folder.Exists), name =>
                {
                    var path = folder.AnalyticsEvents.First(a => a.Name.Equals(name)).Path;
                    return folder.RemoveAnalyticsEvent(path);
                }
                 );
        private static async Task<ServerTask[]> Helper(IEnumerable<string> eventIds, Func<string, ServerTask> func)
        {
            var tasks = new List<ServerTask>();
            foreach (var arg in eventIds)
                tasks.Add(await CheckServerState(() => func(arg)));
            return tasks.ToArray();
        }
        public static bool Exists(this AnalyticsEventFolder folder, string id) =>
            folder.AnalyticsEvents.Any(t => t.Name.Equals(id));
        public static bool NotExists(this AnalyticsEventFolder folder, string id) => !folder.Exists(id);
        /// <summary>
        /// How the f... is this serverTask thingy to be handled?
        /// there should be some sort of timeout. 
        /// </summary>
        private static async Task<ServerTask> CheckServerState(Func<ServerTask> func)
        {
            var st = func();
            while (st.State != StateEnum.Success && st.State != StateEnum.Completed)
            {
                if (st.State == StateEnum.Error) return st;
                await Task.Delay(50);
            }
            return st;
        }

        /// <summary>
        /// Shows how to build the event ()
        /// </summary>
        /// <param name="source"></param>
        /// <param name="eventId"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public static VideoOS.Platform.Data.AnalyticsEvent CreateAnalyticalEvent(Item source, string eventId, string json)
        {
            //var cam = GetFlatCameraList().First();
            var eventSource = new EventSource()
            {
                // Send empty - it is possible to send without an eventsource, but the intended design is that there should be a source
                // the FQID is primarily used to match up the ObjectId with a camera.
                FQID = source.FQID,
                // If FQID is null, then the Name can be an IP address, and the event server will make a lookup to find the camera
                Name = source.Name
            };

            var eventHeader = new EventHeader
            {
                ID = Guid.NewGuid(),
                Type = "AnyVision",
                Timestamp = DateTime.Now,
                Message = eventId, // Analytical Event Id!
                Source = eventSource,
                CustomTag = "AnyVision"
            };

            var analyticEvent = new VideoOS.Platform.Data.AnalyticsEvent
            {
                EventHeader = eventHeader,
                Location = "Event location 1",
                Description = "Analytics event description.",
                Vendor = new Vendor { Name = "AnyVision" },
            };
            return analyticEvent;
            //internalMessenger.Send(MessageId.Server.NewEventCommand, analyticEvent);
        }

        
    }
}
