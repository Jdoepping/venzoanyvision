﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilestoneSdkExtensions.Miscellaneous
{
    public struct Entry
    {
        public DateTime TimeStamp { get; set; }
        public string Reason { get; set; }
        public string ErrorMessage { get; set; }
    }
}
