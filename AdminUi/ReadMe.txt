﻿
This project is of type Windows Application.
It is used because it enables using a WPF control in the Admin, instead of an
winforms control. Yeah, I just don't like winforms I guess.

See the HostingUserControl(s) in the integration project.

The projects sole purpose is to enable WPF support. It should not contain any code 
not dedicated to that purpose.