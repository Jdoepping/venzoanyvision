﻿using System;
using System.Windows;
using AdminUi.Interfaces;

namespace AdminUi
{
    /// <summary>
    /// Interaction logic for PropertiesAnyVision.xaml
    /// Used in Admin/Item/HostingUserControl 
    /// </summary>
    public partial class PropertiesAnyVision //: UserControl
    {
        public event Action TextChangedEvent;
        public PropertiesAnyVision() => InitializeComponent();

        /// <summary>
        /// NB NB Doesn't work don't use it.
        /// What ever functionality that should be associated is injected.
        /// </summary>
        /// <param name="functionality"></param>
        public PropertiesAnyVision(IFunctionality functionality):base()
        {
            Functionality = functionality;
        }

        // Adding these Dependency Properties is of no use. 
        #region Dependency properties
        public static readonly DependencyProperty UserNameProperty =
            DependencyProperty.Register("UserName", typeof(string),
                typeof(PropertiesAnyVision), new PropertyMetadata(string.Empty));
        public string UserName
        {
            set => SetValue(UserNameProperty, value);
            get => (string)GetValue(UserNameProperty);
        }
        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string),
                typeof(PropertiesAnyVision), new PropertyMetadata(string.Empty));
        public string Password
        {
            set => SetValue(PasswordProperty, value);
            get => (string)GetValue(PasswordProperty);
        }
        public static readonly DependencyProperty DomainProperty =
            DependencyProperty.Register("Domain", typeof(string),
                typeof(PropertiesAnyVision), new PropertyMetadata(string.Empty));
        public string Domain
        {
            set => SetValue(DomainProperty, value);
            get => (string)GetValue(DomainProperty);
        }
        public static readonly DependencyProperty BaseUrlProperty =
            DependencyProperty.Register("BaseUrl", typeof(string),
                typeof(PropertiesAnyVision), new PropertyMetadata(string.Empty));

        public string BaseUrl
        {
            set => SetValue(BaseUrlProperty, value);
            get => (string)GetValue(BaseUrlProperty);
        }
        #endregion Dependecy properties

        private async void TestConnection(object sender, RoutedEventArgs e)
        {
            var success = await Functionality.TestConnection(

                s=>MessageBox.Show(s,"Info",MessageBoxButton.OK));
        }

        public IFunctionality Functionality { get; set; }

        private void OnTextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e) =>
            TextChangedEvent?.Invoke();

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e) =>
            Functionality.ConnectToAnyVisionSocketServer(
                s => MessageBox.Show(s, "Info", MessageBoxButton.OK));

        private void ChkTrackCreated_OnClick(object sender, RoutedEventArgs e) => TextChangedEvent?.Invoke();
    }
}
