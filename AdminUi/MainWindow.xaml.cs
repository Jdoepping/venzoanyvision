﻿using System.Windows;
using AdminUi.Interfaces;

namespace AdminUi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            PropertiesAnyVision.TextChangedEvent += PropertiesAnyVision_TextChangedEvent;
        }

        private void PropertiesAnyVision_TextChangedEvent() => 
            PropertiesAnyVision.Functionality.HasUnsavedChanges = true;

        public IFunctionality Functionality
        {
            get => PropertiesAnyVision.Functionality;
            set=>PropertiesAnyVision.Functionality = value;
        }

        public void RemoveHasUnsavedChangesFlag() => PropertiesAnyVision.Functionality.HasUnsavedChanges = false;

        private void SaveSettings(object sender, RoutedEventArgs e)
        {
            PropertiesAnyVision.Functionality.Save();
            PropertiesAnyVision.Functionality.HasUnsavedChanges = false;
        }
    }
}
