﻿using System;
using System.Windows;
using System.Windows.Controls;
using AdminUi.Interfaces;

namespace AdminUi
{
    /// <summary>
    /// Interaction logic for TabUserControl.xaml
    /// </summary>
    public partial class TabUserControl : UserControl
    {
        
        public TabUserControl()
        {
            InitializeComponent();
        }

        public ITabFunctionality TabFunctionality { get; set; }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems != null && e.AddedItems.Count>0)
            {
                var context = (ITabSettings)DataContext;
                var cam = (ITabItem) e.AddedItems[0];
                context.Camera.Name = cam.Name;
                context.Camera.Description = cam.Description;
                context.Camera.Guid = cam.Guid;
            }
        }

        private async void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            ((ITabSettings)DataContext).Cameras = await TabFunctionality.GetAnyVisionCameras();
        }

        private void ClearAssociation_OnClick(object sender, RoutedEventArgs e)
        {
            var context = (ITabSettings)DataContext;
            context.Camera.Name = null;
            context.Camera.Description = null;
            context.Camera.Guid = null;
        }
    }
}
