﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminUi.Interfaces
{
    public interface IWebSockets : IEnumerable<bool>
    {
        event Action<IWebSockets> OnStatusChange;
        bool TrackCreated { get; set; }
        bool RecognitionCreated { get; set; }
        bool CameraChanged { get; set; }
        bool CameraDeleted { get; set; }
    }
}
