﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminUi.Interfaces
{
    public interface ITabSettings
    {
        ITabItem Camera { get; set; }
        IEnumerable<ITabItem> Cameras { get; set; }
    }
}
