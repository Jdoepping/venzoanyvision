﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AdminUi.Interfaces
{
    public interface ITabFunctionality
    {
        Task<IEnumerable<ITabItem>> GetAnyVisionCameras();
    }
}
