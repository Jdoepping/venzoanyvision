﻿namespace AdminUi.Interfaces
{
    public interface ITabItem
    {
        string Name { get; set; }
        string Description { get; set; }
        string Guid { get; set; }
    }
}
