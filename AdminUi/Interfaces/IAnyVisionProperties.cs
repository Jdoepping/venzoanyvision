﻿using System;
using System.Collections.Generic;

namespace AdminUi.Interfaces
{
    public interface IAnyVisionProperties
    {
        event Action ConfigurationChangedByUser;
        string UserName { get; set; }
        string Password { get; set; }
        string Domain { get; set; }
        string BaseUrl { get; set; }
        string LastErrorMessage { get; set; }
        DateTime LastConnectTime { get; set; }
        string Status { get; set; }
        IWebSockets WebSocketSettings { get; set; }
        string Reporting { get; set; }
        string NoneTransientError { get; set; }
        int EventCount { get; set; }
        string TokenRenewalTime { get; set; }
        bool IsTokenRenewalTimeChanged { get; set; }
    }
}
