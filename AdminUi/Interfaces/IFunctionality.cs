﻿using System;
using System.Threading.Tasks;

namespace AdminUi.Interfaces
{
    /// <summary>
    /// Not sure what will go into this interface.
    /// This will be the functionality needed by the UI I Guess
    /// </summary>
    public interface IFunctionality
    {
        /// <summary>
        /// Returns true when a connection can be made to the AnyVision server
        /// </summary>
        /// <returns></returns>
        Task<bool> TestConnection(Action<string> showMessage);

        Task<bool> ConnectToAnyVisionSocketServer(Action<string> showMessage);
        IAnyVisionProperties Properties { get; }
        bool HasUnsavedChanges { get; set; }
        void Save();
    }
}
